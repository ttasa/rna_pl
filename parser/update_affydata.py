from basic import get_files
import os
import MySQLdb as mdb

def query(query_string):
    con = connect()
    cur = con.cursor()
    cur.execute(query_string)
    rows = cur.fetchall()
    con.close()
    return rows

def connect():
    """
    Returns the connection to the database. Must be closed after use with close().
    """
    try:
        con = mdb.connect(
                            'localhost',    # address
                            'affy',         # user
                            'parool!',      # password
                            'affyseq_db')   # database
        return con

    except mdb.Error, e:
        return "Error %d: %s" % (e.args[0], e.args[1])

def update_string(unique_name):
    
    query_string="update affy_data set processed=1, processed_date=curdate() where experiment_name='"+unique_name+"';"
    return query(query_string)
    
def main():
    files=get_files('/group/software/public/affyseq_dev/final/affy','.nc',None)
    basenames=[os.path.basename(x) for x in files]
    experiment_names=[x.replace('.nc','') for x in basenames]
    uniquenames=list(set(experiment_names))
    map(update_string,uniquenames)

if __name__ == "__main__":
    main()