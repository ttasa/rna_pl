import re
from basic import rows_to_array
import urllib2
import logging

"""
//// IDF

"""

def aaexperiment_type(file_object):
	rows_array = rows_to_array(file_object)
	clean_idf_rows = clean_idf(rows_array)
	row_indexes = get_idf_row_indexes(clean_idf_rows,get_idf_aaexperimenttype_dict())
	AEvalues=[clean_idf_rows[ae_index][1].lower()  for ae_index in row_indexes.values()[0] ]
	logging.info('%s ArrayExpress experiment types distinguished ',AEvalues)
	return filter(lambda x: x in ['rna-seq of coding rna', 'transcription profiling by high throughput sequencing'],AEvalues)


def parse_idf(file_object):
	"""
	Parses the IDF file object.
	"""
	rows_array = rows_to_array(file_object)
	clean_idf_rows = clean_idf(rows_array)
	
	row_indexes = get_idf_row_indexes(clean_idf_rows,get_idf_row_filters_dict())
	
	if row_indexes == False:
		logging.error("Could not get all '" + file_object.name + "' rows - a header is missing.")
		return False
	exp_desc_index=row_indexes['experimentdescription'][0]
	inv_desc_index=row_indexes['investigationtitle'][0]
	if (len(clean_idf_rows[exp_desc_index][1]) > 1 or 
		len(clean_idf_rows[inv_desc_index][1]) > 1):
		
		useful_idf_rows = get_useful_idf_rows(clean_idf_rows, row_indexes)
		
		idf_rows_array = convert_to_rows_array(useful_idf_rows)
		idf_rows_array=remove_characters_idf(idf_rows_array)
		logging.debug("Idf rows_array created")
		return idf_rows_array
	else:
		logging.error('ERROR: Experiment idf file is missing both the experiment description and the investigation title')
		return False
		

def remove_characters_idf(idf_rows_array):
	new_idf_rows_array=[]
	for row in idf_rows_array:
		new_row=[element.replace('\n','') for element in row]
		new_idf_rows_array.append(new_row)
	return new_idf_rows_array
		
def get_idf_row_indexes(rows_array,idf_row_filters_dict):
	"""
	Returns a dictionary with indexes for specified rows in IDF data.
	"""
	
	i = 0

	for row in rows_array:
		if(len(row)==0):
			i+=1
			continue
		element = row[0]
	
		element = element.replace(' ', '')
		element = element.replace('\n', '')
		element = element.lower()
		for row_filter in idf_row_filters_dict:
			
			re_filter = re.compile(row_filter)
			if re_filter.search(element) != None:
				idf_row_filters_dict[row_filter].append(i)
		i += 1
		
	if ('aeexperimenttype' not in idf_row_filters_dict):
		
		return test_idf_row_indexes(idf_row_filters_dict,rows_array)
		
	else:
		return idf_row_filters_dict
	

def test_idf_row_indexes(idf_row_filters_dict,rows_array):
	
	if (len(idf_row_filters_dict['experimentdescription']) == 0 or
		len(idf_row_filters_dict['investigationtitle']) == 0):
		
		return False
	
	elif (len(idf_row_filters_dict['secondaryaccession'])>=2):
	
		idf_row_filters_dict=select_secaccession_index(idf_row_filters_dict,rows_array)
		print idf_row_filters_dict
		return idf_row_filters_dict
	
	else:
		return idf_row_filters_dict
		
	
	

def select_secaccession_index(idf_row_filters_dict,rows_array):
	logging.info("Selecting secondary accession")
	sec_accession_list=idf_row_filters_dict['secondaryaccession']
	print idf_row_filters_dict
	GSE=[sec_accession for sec_accession in sec_accession_list if 'RP' in rows_array[sec_accession][1]]
	print GSE
	if (len(GSE)==0):
		GSE=[sec_accession_list][0]
	elif(len(GSE)>1):
		print('Multiple Gen accessions')
		GSE=[GSE[0]]
	idf_row_filters_dict['secondaryaccession']=GSE
	return idf_row_filters_dict


def get_idf_aaexperimenttype_dict():
	
	idf_row_filters_dict = dict()
	idf_row_filters_dict['aeexperimenttype']=[]
	
	return idf_row_filters_dict

def get_idf_row_filters_dict():
	"""
	Returns a dictionary with specified filters for IDF data.
	"""
	idf_row_filters_dict = dict()
	idf_row_filters_dict['experimentdescription'] = []
	idf_row_filters_dict['investigationtitle'] = []
	idf_row_filters_dict['pubmedid'] = []
	idf_row_filters_dict['secondaryaccession'] = []

	return idf_row_filters_dict

def clean_idf(rows_array):
	"""
	Removes unnecessary strings from the rows array data.
	"""
	for index in range(0, len(rows_array)):
		for times in range(0, rows_array[index].count('\n')):
			rows_array[index].remove('\n')
		for times in range(0, rows_array[index].count('\r\n')):
			rows_array[index].remove('\r\n')
		for times in range(0, rows_array[index].count('')):
			rows_array[index].remove('')
		if (len(rows_array[index]) == 1): rows_array[index].append('NA')
	return rows_array

def get_useful_idf_rows(rows_array, row_indexes):
	"""
	Returns only useful data from the IDF file for the parser.
	"""
	
	useful_array=[rows_array[row_indexes[element][0]] for element in row_indexes if row_indexes[element] != []]


	return useful_array

import sys

def convert_to_rows_array(idf_data):
	"""
	Returns rows array data format from IDF data format.
	"""
	headers = []
	body = []
	
	for element in idf_data:
	
		header = modify_header(element[0])
		headers.append(header)
		
		if(header=='__SecondaryAccession'):
			SRP_list=filter(lambda x: 'RP' in x ,element)
			
			if len(SRP_list)==1:
				body.append(SRP_list[0])
			
			elif(len(SRP_list)>1):
				
				Correct_file=map(find_correct_SRP,SRP_list)
			
				SRP=filter(lambda x: x, Correct_file)
				
				if(len(SRP)==1):
					body+=SRP
				elif(len(SRP)==0):
					body.append(element[1])
				else:
					logging.warn("Multiple eligble SRP %s",SRP)
					
					body+=SRP[0]
			
			else:
				body.append(element[1])
		
		else:
			body.append(element[1])
	logging.info("IDF row array created")
	return [headers, body]


def find_correct_SRP(SRP):
	
	target_file='http://www.ebi.ac.uk/ena/data/warehouse/filereport?accession=' + \
        SRP+"&result=read_run&fields=experiment_accession,scientific_name,fastq_ftp,run_accession"    
	f=urllib2.urlopen(target_file)
	f.readline()
	line=f.readline()
	print line
	if len(line)>0:
		return SRP
	else:
		return False

def modify_header(header):
	"""
	Returns a modified string for the given specified string.
	"""
	PUBMEDID = re.compile('pubmedid')
	SEC_AC = re.compile('secondaryaccession')

	header = header.replace(" ", "")
	header = header.lower()
	if PUBMEDID.search(header) != None:
		return "__PubMedID"

	elif SEC_AC.search(header) != None:
		return "__SecondaryAccession"

	return header