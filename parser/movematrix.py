
from basic import *
from rnaseq import get_species
import shutil


def move_all_matrices(thread_info_dict,exp='affy'):
    """
    Executes the ncks program for all platforms.
    """

    
    experiment_dir = get_experiment_type_dir(thread_info_dict,exp)
    
    tool_dirs = get_tool_dirs(experiment_dir)

    for tool_dir in tool_dirs:
        logging.debug('%s Move matrices', thread_info_dict['dataset'])
        move_matrix_nc(thread_info_dict, tool_dir,exp)
        #Move more files
        if(exp=='rnaseq'):
            logging.debug('%s Moving additional files', thread_info_dict['dataset'])
            move_additional_files(thread_info_dict, tool_dir,exp)
    return True
        

def move_matrix_nc(thread_info_dict, cel_file_dir,exp):
    """
    Moves the matrix.nc file to the final directory.
    """ 
    root_dir = thread_info_dict['root_dir']
    dataset = thread_info_dict['dataset']
    
    MATRIX_NC = 'matrix.nc'
    print os.path.basename(cel_file_dir),'cel_file_basename'
    fin_file_name = dataset +'.nc'
    platform=os.path.basename(cel_file_dir)
    if(platform in thread_info_dict):
        logging.debug('%s No moving of platform %s', thread_info_dict['dataset'], platform)
    else:
        logging.debug('%s Moving matrix.nc to final directory ', thread_info_dict['dataset'])
        cur_dir_file_path = os.path.join(cel_file_dir, MATRIX_NC)
        fin_dir_file_path = get_ncks_file_fin_path(thread_info_dict, cel_file_dir, fin_file_name,exp,dataset)
        logging.debug('%s Copy files to %s', thread_info_dict['dataset'], fin_dir_file_path)
        shutil.copyfile(cur_dir_file_path, fin_dir_file_path)
    return True


def move_additional_files(thread_info_dict, cel_file_dir,exp):
    """
    Moves the matrix.nc file to the final directory.
    """ 
    root_dir = thread_info_dict['root_dir']
    dataset = thread_info_dict['dataset']
    
    fin_file_name = dataset + '.nc'
    platform=os.path.basename(cel_file_dir)
    #star='star'
    mapper=thread_info_dict['mapper']
    mapper_dir=os.path.join(cel_file_dir,mapper)
    
    try:
    
        if(platform in thread_info_dict):
            logging.debug('%s No moving of platform %s', thread_info_dict['dataset'], platform)
        else:
         
            logging.debug('%s Moving matrix.nc to final directory ', thread_info_dict['dataset'])
            cur_dir_file_path = os.path.join(cel_file_dir, 'fastqc')
            fin_dir_file_path = get_rnaseq_additonal_file_dir(thread_info_dict,cel_file_dir,exp,dataset)
            logging.debug('%s Final destination directory %s', thread_info_dict['dataset'],fin_dir_file_path)
            
            #Move quality reports
            if (not os.path.exists(fin_dir_file_path)):
                shutil.copytree(cur_dir_file_path, fin_dir_file_path)
            ##Move alignment data
            
            if (mapper=='star'):
                file_='Log.final.out'
            elif(mapper=='tophat'):
                file_='align_summary.txt'
            mapper_dirs=os.listdir(mapper_dir)
            logging.debug('%s Star directories are the following %s', thread_info_dict['dataset'], mapper_dirs)
                
            map(lambda x: shutil.copy(os.path.join(mapper_dir,x,file_),
                    os.path.join(fin_dir_file_path,x)),mapper_dirs)
               
            
            #Move quantification files
                
            if(thread_info_dict['quantifier']=='htseq'):
             
                summary_file=os.path.join(cel_file_dir,'summary')
                logging.debug('%s Summary file is located at %s', thread_info_dict['dataset'], summary_file)  
                shutil.copy(summary_file, fin_dir_file_path)
            
            elif(thread_info_dict['quantifier']=='cufflinks'):
                
                cuffnorm_dir=os.path.join(cel_file_dir,'cuffnorm')
                logging.debug('%s Moving cuffnorm files %s', thread_info_dict['dataset'], os.listdir(cuffnorm_dir))
                map(lambda x: shutil.copy(os.path.join(cuffnorm_dir,x), fin_dir_file_path),os.listdir(cuffnorm_dir))
                if(thread_info_dict['assembly']):
                    merged_gtf=os.path.join(cel_file_dir,'cuffmerge','merged.gtf')
                    shutil.copy(merged_gtf, fin_dir_file_path)
                    
            else:
                pass
            
    except IOError as e:
        logging.error("%s I/O error(%s): %s",thread_info_dict['dataset'], e.errno, e.strerror)
        logging.error("%s Moving files from final directories failed. Likely reason: Some files are missing",thread_info_dict['dataset'])
        logging.debug("%s Error code attribute in thread_info_dict variable: %s",thread_info_dict['dataset'],os.path.basename(cel_file_dir))
        thread_info_dict[os.path.basename(cel_file_dir)]='MVM'
        return False
            
    return True


def get_rnaseq_additonal_file_dir(thread_info_dict,cel_file_dir,exp,dataset):
      
    platform=dataset
    organism=os.path.basename(cel_file_dir)
    transcriptome_path=get_address(organism,thread_info_dict,fileend='.gtf',nat='transcriptome')
    transcriptome_file=os.path.basename(transcriptome_path)
    platform=transcriptome_file.replace('.gtf','')    
    root_dir = thread_info_dict['root_dir']
    fin_dir_list = ['final', exp+'_add', platform,dataset]
    fin_dir_path = get_path_from_root(root_dir, fin_dir_list)
    return fin_dir_path


def get_ncks_file_fin_path(thread_info_dict, cel_file_dir, file_name,exp,dataset):
    """
    Returns the full path to a final directory.
    """
    if(exp=='affy'):
        platform = os.path.split(cel_file_dir)[-1]
    else:
        platform=dataset
        organism=os.path.basename(cel_file_dir)
        transcriptome_path=get_address(organism,thread_info_dict,fileend='.gtf',nat='transcriptome')
        transcriptome_file=os.path.basename(transcriptome_path)
        platform=transcriptome_file.replace('.gtf','')
        logging.debug('%s Platform name %s',thread_info_dict['dataset'],platform)
        
    root_dir = thread_info_dict['root_dir']
    fin_dir_list = ['final', exp, platform]
    fin_dir_path = get_path_from_root(root_dir, fin_dir_list)
    
    fin_file_list = fin_dir_list + [file_name]
    fin_file_path = get_path_from_root(root_dir, fin_file_list)
   
    logging.debug('%s Final directory path %s',thread_info_dict['dataset'],fin_dir_path)
    create_directory(fin_dir_path)
    return fin_file_path
