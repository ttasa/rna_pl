# rnaseq.py
import shutil
import math
import subsampler
import random
from basic import *
from itertools import islice


def run_rnaseq(thread_info_dict, match_dict):
	
	"""
	Executes quality control software and creates a script to use with
	Torque/Maui scheduling system.
	"""
	kwargs=thread_info_dict['General']
	
	raw_files,paired_files=setup_raw_files(kwargs)
	if error_check(kwargs,'File setup') : return False
		
	logging.info('%s Raw files set up ', kwargs['dataset'])
	logging.info('%s Quality control start ', kwargs['dataset'])
	
	files = run_quality_control(thread_info_dict, paired_files, raw_files)
	
	if error_check(kwargs,'Quality control') : return False
	
	logging.info('%s Mapper usable files %s', kwargs['dataset'], files)
	
	mapper=kwargs['mapper']
	logging.debug('%s Mapper specified as %s ', kwargs['dataset'], mapper)
		
	if(mapper == 'tophat'):
		
		prepare_tophat(files, thread_info_dict)
		if error_check(kwargs,'TopHat Prerun') : return False
		logging.debug('%s Prerun of TopHat completed', kwargs['dataset'])
		mapper_output = tophat_intermediate(files, thread_info_dict, match_dict)
			
		if error_check(kwargs,'TopHat') : return False

		
	elif(mapper == 'star'):
		
		mapper_output = star_intermediate(files, thread_info_dict, match_dict)
		
		if error_check(kwargs,'Star') : return False
	logging.info('%s Mapping phase completed with %s %s', kwargs['dataset'], mapper, mapper_output)
	if(kwargs['meta']):
		merged_alignment_paths = merge_alignment(mapper_output, kwargs, match_dict)
	else: 
		merged_alignment_paths=mapper_output
		
	logging.info('%s Merged alignment paths %s', kwargs['dataset'], merged_alignment_paths)
		
		
	libraries = RSeQC_intermediate(merged_alignment_paths, kwargs)
	# Kommenteeri Return TRUE v2lja, muutused cutadapt
		
	if error_check(kwargs,'RSeQC') : return False
	
	logging.info('%s Library types inferred ', kwargs['dataset'])
	
	quantifier=kwargs['quantifier']
	logging.debug('%s Quantifier specified as %s ', kwargs['dataset'], quantifier)
	
	if(quantifier == 'htseq'):
	
		htseq_output = htseq_intermediate(merged_alignment_paths, libraries, thread_info_dict)
		
		logging.info('%s HTSeq run completed ', kwargs['dataset'])
		
		prepare_tnc(kwargs)
		logging.info('%s TNC files prepared ', kwargs['dataset'])
		create_matrix(htseq_output, kwargs)
		logging.info('%s matrix created ', kwargs['dataset'])
	
		
		
	elif(quantifier == 'cufflinks'):
		
		if 'assembly' not in kwargs: kwargs['assembly'] = False
		
		if(kwargs['assembly']):
		
			cufflinks_output = cufflinks(merged_alignment_paths, libraries, thread_info_dict)
			
			if error_check(kwargs,'Cufflinks') : return False
			
			logging.info('%s Cufflinks output %s', kwargs['dataset'], cufflinks_output)
		
			cuffmerge_output = cuffmerge(cufflinks_output, kwargs)
			
			if error_check(kwargs,'Cuffmerge') : return False
			
			logging.info('%s Cuffmerge output %s', kwargs['dataset'], cuffmerge_output)
		
		
			
		cuffquant_output = cuffquant(merged_alignment_paths, thread_info_dict, libraries)
			
		logging.info('%s Cuffquant output %s', kwargs['dataset'], cuffquant_output)
			
		if error_check(kwargs,'Cuffquant') : return False
			
		cuffnorm_output = cuffnorm(thread_info_dict, libraries)
			
		logging.info('%s Cuffnorm output %s', kwargs['dataset'], cuffnorm_output)
			
		if error_check(kwargs,'Cuffnorm') : return False
			
		prepare_nc(cuffnorm_output, kwargs)
			
		if error_check(kwargs,'Cuffnorm parse') : return False
			
		prepare_tnc(kwargs)
			
		return bool
	
			
	return True

def setup_raw_files(thread_info_dict):
	
	job_path = get_experiment_type_dir(thread_info_dict, type='rnaseq')
	
	if(thread_info_dict['meta']):
	
		raw_files = get_files(job_path, ".fastq", "raw_data") + get_files(job_path, ".fq", "raw_data")
		paired_files = separate_files_pairing(raw_files, thread_info_dict, next_=False)		
		logging.debug('%s Paired files %s ', thread_info_dict['dataset'], paired_files)
		if (not metadata_raw_union(thread_info_dict, paired_files)): return (False,False)
		
	else:
		raw_addr=thread_info_dict['raw_data']
		old_raw_files=[os.path.join(raw_addr,file_) for file_ in os.listdir(raw_addr) if file_.split('.')[-1] in ['fq','fastq']]
		logging.debug('%s FastQ files in specified folder: %s ', thread_info_dict['dataset'], old_raw_files)
		species = get_species(thread_info_dict)
		spec_directory = os.path.join(get_experiment_type_dir(thread_info_dict, type='rnaseq'), species)
		true_raw_dir=os.path.join(spec_directory,'raw_data')
		
		if(not(os.path.abspath(raw_addr)==true_raw_dir)):
			logging.debug('%s Location of raw files is not raw_data folder %s ', thread_info_dict['dataset'], os.path.abspath(raw_addr))
			base_names=map(os.path.basename,old_raw_files)
			if(not os.path.exists(true_raw_dir)):
				os.makedirs(true_raw_dir)
				
			raw_files=map(lambda x: os.path.join(true_raw_dir,x),base_names)
			
			def sym_copy(old_raw_file,raw_file):
				
				if not(os.path.exists(raw_file) or os.path.islink(raw_file)):
					os.symlink(old_raw_file,raw_file)
				else:
					pass 
				return True
					
			
			map(sym_copy,old_raw_files,raw_files)
			logging.debug('%s Symlinks created for %s ', thread_info_dict['dataset'], raw_files)

		paired_files = separate_files_pairing(raw_files, thread_info_dict, next_=False)		
		logging.debug('%s Paired files %s ', thread_info_dict['dataset'], paired_files)
	return (raw_files,paired_files)


def merge_alignment(mapper_output, thread_info_dict, match_dict):
	
	output = []
	new_dict, SRX_dict = {}, {}
	
	try:
		for k, v in match_dict.iteritems():
			for addr in mapper_output:
				if (k == os.path.basename(addr)):
					new_dict[addr] = v
		logging.debug('%s match_dict with addresses %s', thread_info_dict['dataset'], new_dict)
		
		for k, v in new_dict.iteritems():
			if (v in SRX_dict):
				SRX_dict[v] = SRX_dict[v] + [k]
			else:
				SRX_dict[v] = [k]
		
		logging.debug('%s SRX_dict %s', thread_info_dict['dataset'], SRX_dict)
		
		if (max(map(len, SRX_dict.itervalues()))) > 1:
			
			sam_merge_command_list = []
			for key, val in SRX_dict.iteritems():
				dir_ = os.path.join(os.path.dirname(val[0]), key)
				
				output_folder = data_folder(dir_, thread_info_dict, 'alignment')
				
				output.append(output_folder)
				output_path = os.path.join(output_folder, 'alignment.bam')
				logging.debug('%s Output path %s', thread_info_dict['dataset'], output_path)
				if (os.path.exists(output_path)):
					continue
				else:
					logging.debug('%s SRX decision, output %s val %s' , thread_info_dict['dataset'], output_path, val)
					if (len(val) > 1):
						command = sam_merge_command(output_path, val, thread_info_dict)
						sam_merge_command_list.append(command)
						logging.debug('%s Command list for sam_merge %s', thread_info_dict['dataset'], sam_merge_command_list)
					
					elif (len(val) == 1):
						logging.debug('%s Start copying files %s %s', thread_info_dict['dataset'], val[0], output_folder)
						bam_end = [fl for fl in os.listdir(output_folder) if fl.endswith(".bam")]
						if(len(bam_end) == 0):
							map(lambda x: shutil.copy(os.path.join(val[0], x), output_folder), os.listdir(val[0]))
							logging.debug('%s Successfully copied files ', thread_info_dict['dataset'])
						else:
							continue
					
			if(len(sam_merge_command_list) > 0):
				Queue_handler(10, sam_merge_command_list, thread_create, merge_lane, thread_info_dict)
		
			return output
		
		else:
			
			output = []
			
			for k, v in new_dict.iteritems():
				
				path = os.path.join(os.path.dirname(k), v)
				logging.debug('%s Path %s to be rename to %s', thread_info_dict['dataset'], k, path)
				output.append(path)
				if (not os.path.exists(path)):
					os.rename(k, path)
			
			return output
	
	except:
		
		logging.error('%s Unexpected error during merge alignment %s', thread_info_dict['dataset'], str(sys.exc_info()))
		return False

		

def sam_merge_command(output_path, val, thread_info_dict):
	
	logging.debug('%s Output path form sam_merge %s', output_path, thread_info_dict['dataset'])
	bam_ends = [i for dir_ in val for i in os.listdir(dir_) if i.endswith('.bam') and not i == 'unmapped.bam']	
	logging.debug('%s Bam_ends to merge %s', output_path, thread_info_dict['dataset'])
	bam_files = map(lambda x, y : os.path.join(x, y), val, bam_ends)
	command = ['samtools', 'merge', output_path] + bam_files
	logging.debug('%s Samtools merge command %s', "".join(command), thread_info_dict['dataset'])
	return command
	
	
def merge_lane(thread_info_dict, thr, queue):
	
	command = queue.get()
	logging.debug('%s Starting command %s', thread_info_dict['dataset'], command)
	result = run_process(command, 'Merging lanes')
	logging.debug('%s Results from merging lanes %s', thread_info_dict['dataset'], result)
	queue.task_done()
	return True

def metadata_raw_union(thread_info_dict, paired_files):
	
	experiment_dir = get_experiment_type_dir(thread_info_dict, 'rnaseq')
	tool_dirs = get_tool_dirs(experiment_dir)
	for tool_dir in tool_dirs:
		organism = os.path.basename(tool_dir)
		path = os.path.join(tool_dir, 'metadata')
		logging.debug('%s Metadata path %s', thread_info_dict['dataset'], path)
		with open(path, 'r+') as met_file:
			lines = [line for line in met_file.readlines()]
		array = map(lambda x: x.strip().split('\t'), lines)
		
		if(max(map(len, array)) == len(paired_files)):
			logging.debug('%s Metadata and file lenghts are equal', thread_info_dict['dataset'])
			return True
		else:
			thread_info_dict[organism] = 'MTD'
			logging.error('%s Metadata and file lenghts are not equal', thread_info_dict['dataset'])
			
			return False
			

def RSeQC_intermediate(files, thread_info_dict):
	
	libraries = {}
	lock = threading.Lock()
	logging.debug('%s Starting RSeQC', thread_info_dict['dataset'])
	
	species = get_species(thread_info_dict)
	logging.debug('%s species for the dataset is %s', thread_info_dict['dataset'], species)
	output_path = os.path.join(thread_info_dict['root_dir'], 'lib', 'bedGene', species)
	
	if (not os.path.exists(output_path)):
		os.mkdir(output_path)
	
	if (not os.listdir(output_path)):
		logging.debug('%s bedgene file does not exist for %s', thread_info_dict['dataset'], species)
		check_bedgene(output_path, species, thread_info_dict)
		logging.debug('%s bed formatted gtf created for %s', thread_info_dict['dataset'], species)
	
	Queue_handler(15, files, thread_create, RSeQC_process, thread_info_dict, libraries, lock)
	return libraries

def bam_from_file(folder):
	
	bam_suffix = [el for el in os.listdir(folder) if el.endswith('.bam') and not el == 'unmapped.bam']
	assert len(bam_suffix) == 1
	file_ = os.path.join(folder, "".join(bam_suffix))
	return file_

def htseq_count_process(thread_info_dict, output, libraries, thr, queue):
	
	kwargs=thread_info_dict['General']
	folder = queue.get()
	organism = get_species(kwargs)
	bam_file = bam_from_file(folder)
	basename = os.path.basename(folder)
	
	method = libraries[basename] 
	
	output_folder = data_folder(folder, kwargs, 'htseq')
	
	output.append(output_folder)
	
	file_path = os.path.join(output_folder, 'Count.txt')
		
	if(os.path.exists(file_path)):
		logging.info('%s HT-Seq files exist %s', kwargs['dataset'], file_path)
		queue.task_done()
		return True
		
	
	write_file = open(file_path, "w+")	
	transcriptome_path = get_address(organism, kwargs, fileend='.gtf', nat='transcriptome')
	identifier = str(random.uniform(0, 1))[2:]
	
	
	str_components,auto_param=software_command_parse(thread_info_dict,'htseq')
	logging.debug('%s HTSeq run components %s',kwargs['dataset'],str_components)
	add_param=auto_param_filtering('htseq',auto_param,*[method])
	logging.debug('%s Added parameters to HTSeq %s',kwargs['dataset'],add_param)

	sort_bam = subprocess.Popen(['samtools', 'sort', '-n','-o', bam_file, 'Tmp' + identifier], stdout=subprocess.PIPE)	
	ps = subprocess.Popen(['samtools', 'view', '-'], stdout=subprocess.PIPE, stdin=sort_bam.stdout)
	#subprocess.call(['htseq-count', '-r', 'name', '-i', 'gene_id', '-m', 'union', '-s', method, '-', transcriptome_path], stdin=ps.stdout, stdout=write_file)
	
	joined_str=" ".join(str_components+add_param).split(' ')
	subproc=['htseq-count'] + joined_str + [ '-', transcriptome_path]
	print subproc
	logging.debug('%s htseq command subprocess %s',kwargs['dataset'],subproc)
	subprocess.call(subproc, stdin=ps.stdout, stdout=write_file)

	write_file.close()
	ps.stdout.close()
	logging.info('%s HT-Seq completed run %s', kwargs['dataset'], file_path)
	queue.task_done()
	return True


def RSeQC_process(thread_info_dict, libraries, lock, thr, queue):
	
	folder = queue.get()
	organism = get_species(thread_info_dict)
	
	bam_file = bam_from_file(folder)
	
	output_folder = data_folder(folder, thread_info_dict, 'RseQC')
	sample_name = os.path.basename(folder)
	library_file = os.path.join(output_folder, 'library_type.txt')
	if(os.path.exists(os.path.join(library_file))):
		with open(library_file, 'r') as f:
			info = dict([tuple(line.strip().split(':')) for line in f])
		libraries.update(info)
		logging.debug('%s RSEQC had already been run and libraries are updated', thread_info_dict['dataset'])

	else:
		
		write_file = open(os.path.join(output_folder, 'RSeQCoutput.txt'), "w+")
	
		bed_reference_path = get_address(organism, thread_info_dict, fileend='.bed', nat='bedGene')
		logging.debug('%s Running infer_experiment %s', thread_info_dict['dataset'], sample_name)
		subproc = ['infer_experiment.py', '-i', bam_file, '-r', bed_reference_path, '-q', '10']
		logging.debug('%s Infer_experiment parameters %s', thread_info_dict['dataset'], subproc)
		subprocess.call(subproc, stdout=write_file)
		write_file.seek(0)
		try:
			frac_reads = map(lambda x: x.strip().split(': ')[1], write_file.readlines()[-2::])
			frac_reads = map(lambda x: round(float(x), 2), frac_reads)
			write_file.close()
		except IndexError:
			thread_info_dict[organism] = 'UND'
			logging.error('%s Unknown datatype in alignment ', thread_info_dict['dataset'])
			write_file.close()
			queue.task_done()
			return False
		try:
			if(frac_reads[0] > 2 * frac_reads[1]):
				lib_type = 'yes'
			elif(frac_reads[1] > 2 * frac_reads[0]):
				lib_type = 'reverse'
			else:
				lib_type = 'no'
		except IndexError:
			thread_info_dict[organism] = 'LQR'
			logging.error('%s Running Infer experiment failed ', thread_info_dict['dataset'])
			queue.task_done()
			return False
		
		lock.acquire()
		logging.debug('%s %s %s %s', thread_info_dict['dataset'], frac_reads, sample_name, lib_type)
		
		write_file_to_path(sample_name + ':' + lib_type, library_file)
		libraries[sample_name] = lib_type
		lock.release()
	queue.task_done()
	return True
	

def create_matrix(htseq_output, thread_info_dict):
	
	thread_dir = get_thread_dir_path(thread_info_dict)
	rna_folder = os.path.join(thread_dir, 'rnaseq')
	spec_directories = [os.path.join(rna_folder, x) for x in os.listdir(rna_folder) if os.path.isdir(os.path.join(rna_folder, x))]
	
	def is_correct_species(path, folder):
		
		if (os.path.dirname(os.path.dirname(path)) == folder):
			return True
		else:
			False
	
	for folder in spec_directories:
		if(not os.path.exists(os.path.join(folder, 'matrix'))):
			species_paths = filter(lambda x: is_correct_species(x, folder), htseq_output)
			SRRnames = map(os.path.basename, species_paths)
			logging.debug('%s SRRnames that are available %s', thread_info_dict['dataset'], SRRnames)
			# Rscript 
			metatrue_R=str(thread_info_dict['meta']).upper()
			subprocess = ['Rscript', 'lib/Create_matrix1.R', folder,metatrue_R] + SRRnames 
			error_dict = {2:'MEM', 3:'MTD', 4:'NRM'}
			result = run_process(subprocess, 'Create_matrix')
			logging.debug('%s Results from create_matrix script %s', thread_info_dict['dataset'], result)
			evaluate_error(result, error_dict, thread_info_dict, os.path.basename(folder))
				
		else:
			
			logging.info('%s Matrix already exists', thread_info_dict['dataset'])
			
	
	return True
	

def htseq_intermediate(files, libraries, thread_info_dict):
	
	output = []
	logging.debug('%s Starting HTSeq count', thread_info_dict['General']['dataset'])
	Queue_handler(10, files, thread_create, htseq_count_process, thread_info_dict, output, libraries)
	
	return output


def star_intermediate(files, thread_info_dict, match_dict):
	output = []
	kwargs=thread_info_dict['General']
	logging.debug('%s Starting STAR threads', kwargs['dataset'])
	species = get_species(kwargs)
	logging.debug('%s species for the dataset is %s', kwargs['dataset'], species)
	output_path = os.path.join(kwargs['root_dir'], 'lib', 'stargenome', species)
	
	if (not os.path.exists(output_path)):
		os.mkdir(output_path)
		
	if (not os.listdir(output_path)):
		logging.debug('%s stargenome does not exist for %s', kwargs['dataset'], species)
		check_stargenome(output_path, species, kwargs)
		logging.debug('%s stargenome created for %s', kwargs['dataset'], species)
		
	Queue_handler(4, files, thread_create, star_process, thread_info_dict, output, match_dict)
	
	return output


def alternative_path(output_folder, thread_info_dict, match_dict):
	
	
	RX_outputfolder, head = os.path.split(output_folder)
	
	alt_path = os.path.join(RX_outputfolder, match_dict[head])
	
	logging.debug('%s Alternative path  %s', thread_info_dict['dataset'], alt_path)
	
	if os.path.exists(alt_path) and alt_path!=output_folder:
		
		check_fold = alt_path
		os.rmdir(output_folder)
	
	else:
		check_fold = output_folder
		
	return check_fold


def star_process(thread_info_dict, output, match_dict, thr, queue):
	
		# for i in xrange(len(paired_files[0])):
	kwargs=thread_info_dict['General']
	paired_file = queue.get()
	organism = get_species(kwargs)
	
	if (type(paired_file) is tuple):
		output_folder = data_folder(paired_file[0], kwargs, 'star')
	else:
		output_folder = data_folder(paired_file, kwargs, 'star')
	
	check_fold = alternative_path(output_folder, kwargs, match_dict)
	
	filenames = [j for j in os.listdir(check_fold) if j in ["Log.out", "SJ.out.tab",
		"Log.final.out", "Log.progress.out"] or j.endswith('.bam')] 
	# Muuda 5
	logging.debug('%s Filename contents in star folder %s', kwargs['dataset'], filenames)
	if(len(filenames) == 5):
		output.append(output_folder)
		queue.task_done()
		return True
	
	subproc = get_star_command(paired_file, thread_info_dict, output_folder)
	logging.debug('%s Star command %s', kwargs['dataset'], subproc)
	
	if(run_process(subproc, 'STAR')):
		output.append(output_folder)
		''' Paired end reads'''
		queue.task_done()
		if(not(check_completion(check_fold))):
			kwargs[organism] = 'STR'
	else:
		kwargs[organism] = 'STR'
		queue.task_done()
	return True

def check_completion(output_folder):
	bool_tmp = filter(lambda x: x == '_STARtmp', [os.listdir(output_folder)]) 
	if(len(bool_tmp) > 0):
		return False
	else:
		return True

def get_star_command(file_, thread_info_dict, output_folder):
	
	kwargs=thread_info_dict['General']
	
	organism = get_species(kwargs)
	
	genome_path = get_address(organism, kwargs, fileend='.tab', nat='stargenome')
	
	if(not(genome_path)): return False
	
	genome_index_path = os.path.dirname(genome_path)
	
	str_components,auto_param=software_command_parse(thread_info_dict,'star')
	
	add_param=auto_param_filtering('star',auto_param,*[])
	
	subproc = ["STAR",'--genomeDir', genome_index_path, '--outFileNamePrefix', output_folder + '/',
				'--outSAMstrandField', 'intronMotif',
				'--outSAMtype', 'BAM', 'Unsorted'] + str_components + add_param + ['--readFilesIn']
	print subproc
	if(type(file_) is tuple):
	
		subproc = subproc + [file_[0]] + [file_[1]]
		return subproc
	
	elif(type(file_) is str):

		subproc = subproc + [file_]
		return subproc

	else:
		return False
		
	return True
	

def prepare_tophat(paired_files, thread_info_dict):
	
	kwargs=thread_info_dict['General']
	logging.debug('%s Start prerun of tophat on subsampled data', kwargs['dataset'])
	
	species = get_species(kwargs)
	logging.debug('%s species for the dataset is %s', kwargs['dataset'], species)
	bowtie_genomeindex(species, kwargs)
	bowtie_transcriptomeindex(species, kwargs)
	Queue_handler(4,paired_files,thread_create,library_thread,thread_info_dict)

	return True

def bowtie_transcriptomeindex(species, thread_info_dict):
	
	genome_path = get_address(species, thread_info_dict, fileend='.fa', nat='genome')
	transcriptome_path = get_address(species, thread_info_dict, fileend='.gtf', nat='transcriptome')
	
	trindex = [filename for filename in os.listdir(os.path.dirname(transcriptome_path)) if filename.endswith('.gff')]
	
	if(trindex):
		logging.debug('%s TopHat transcriptome index already exists for %s', thread_info_dict['dataset'], species)
	
	else:
		logging.debug('%s Starting creating TopHat transcriptome index for %s', thread_info_dict['dataset'], species)
		create_transcriptomeindex(genome_path, transcriptome_path)
		logging.debug('%s TopHat transcriptome index for %s created', thread_info_dict['dataset'], species)

def create_transcriptomeindex(genome_path, transcriptome_path):
	index_prefix = genome_path.replace('.fa', '')
	transcriptome_index_path = transcriptome_path.replace('.gtf', '')
	subprocess = ['tophat2', '-G', transcriptome_path, '--transcriptome-index='+transcriptome_index_path, '-p', '5', index_prefix]
	result = run_process(subprocess, 'Tophat transcriptome index')
	
	if(not(result)):
		logging.error(' Bowtie index creation failed. Command used was %s. Please construct it manually', subprocess)
		logging.error('%s error ', str(sys.exc_info()))
		sys.exit(1)
	else:
		return True

def bowtie_genomeindex(species, thread_info_dict,):
	
	genome_path = get_address(species, thread_info_dict, fileend='.fa', nat='genome')
	
	btindex = [filename for filename in os.listdir(os.path.dirname(genome_path)) if filename.endswith('.bt2')]
	
	if (not(len(btindex) == 6)):	
		logging.debug('%s Starting creating Bowtie index for %s', thread_info_dict['dataset'], species)
		create_bowtieindex(genome_path)
		logging.debug('%s stargenome created for %s', thread_info_dict['dataset'], species)
		
	else:
		logging.debug('%s Bowtie index already exists for %s', thread_info_dict['dataset'], species)
		
	return True
	
def create_bowtieindex(genome_path):
	
	index_prefix = genome_path.replace('.fa', '')
	subprocess = ['bowtie2-build', '-f', genome_path, index_prefix]
	result = run_process(subprocess, 'Bowtie index')
	
	if(not(result)):
		logging.error(' Bowtie index creation failed. Command used was %s. Please construct it manually', subprocess)
		logging.error('%s error ', str(sys.exc_info()))
		sys.exit(1)
	else:
		return True


def library_thread(thread_info_dict,thr,queue):
	
	kwargs=thread_info_dict['General']
	selected_file=queue.get()

	if(type(selected_file) is not tuple):
		selected=selected_file
	else:
		selected=selected_file[0]
	

	output_folder=data_folder(selected,kwargs,'lib_type')
	
	sample_name=(os.path.basename(selected).split('_'))[0].replace('.fastq','')
	
	lib_file_path=os.path.join(output_folder,sample_name)
	logging.debug('%s Libraries will be directed to %s',kwargs['dataset'],lib_file_path)

	if(not(os.path.exists(lib_file_path)) and type(selected_file) is tuple):
		
		path_to_samples=subsampler(selected_file,output_folder)
		
		subproc=get_tophat2_command(path_to_samples,thread_info_dict,output_folder)
		
		logging.debug('%s Start tophat on subsampled data with subprocesses %s',kwargs['dataset'],subproc)
		library(subproc,sample_name,path_to_samples,kwargs)		
		queue.task_done()
		
		return True
	
	else:
		#Single end files
		queue.task_done()
		
		return True


	
def tophat_intermediate(paired_files, thread_info_dict, match_dict):
	
	output = []
	logging.info('%s Star Tophat threads', thread_info_dict['General']['dataset'])
	Queue_handler(6, paired_files, thread_create, tophat_process, thread_info_dict, output, match_dict)
	
	return output
	



def tophat_process(thread_info_dict, output, match_dict, thr, queue):
	# for i in xrange(len(paired_files[0])):
	kwargs=thread_info_dict['General']
	paired_file = queue.get()
	
	if (type(paired_file) is tuple):
		output_folder = data_folder(paired_file[0], kwargs, 'tophat')
	else:
		output_folder = data_folder(paired_file, kwargs, 'tophat')
	
	check_fold = alternative_path(output_folder, kwargs, match_dict)
	
	file_exist = map(lambda x :x in os.listdir(check_fold), ["accepted_hits.bam", "deletions.bed", "junctions.bed", "prep_reads.info", "unmapped.bam"])
	logging.debug('%s Number of existing files in %s is %s', kwargs['dataset'], os.listdir(check_fold), file_exist)
	if(all(file_exist)):
		output.append(output_folder)
		queue.task_done()
		return True
	else:	
		subproc = get_tophat2_command(paired_file, thread_info_dict, output_folder)
		print subproc
		logging.debug('%s Tophat command %s', kwargs['dataset'], subproc)
	
		if(run_process(subproc, 'tophat')):
			output.append(output_folder)
			''' Paired end reads'''
			queue.task_done()
		
		else:
			organism = get_species(kwargs)
			thread_info_dict[organism] = 'TPH'
			queue.task_done()
		
		return True


def library(subproc,sample_name,path_to_samples,thread_info_dict):
	#sizes=[]
	logging.debug('%s Results from %s will be directed to %s',sample_name,subproc,path_to_samples)

	if(type(path_to_samples) is tuple):
		header=os.path.dirname(path_to_samples[0])
	else:
		header=os.path.dirname(path_to_samples)
	
	outputvar=os.path.join(header,sample_name+'_'+str(0))
	create_directory(outputvar)
	subproc[4]=outputvar
	
	if(not (type(path_to_samples) is tuple)):
		subproc[len(subproc)-1]=path_to_samples
	else:
		subproc[len(subproc)-2]=path_to_samples[0]
		subproc[len(subproc)-1]=path_to_samples[1]
	
	logging.debug('%s Process command for prerun tophat %s',sample_name,subproc)
	result=run_process(subproc,'tophat')
	if(not(result)):
		organism = get_species(thread_info_dict)
		thread_info_dict[organism] = 'TPH'
		logging.debug('%s Tophat prerun failed %s',sample_name,subproc)
		
	return True		


def prepare_tnc(thread_info_dict):
	
	organism = get_species(thread_info_dict)

	transcriptome_path = get_address(organism, thread_info_dict, fileend='.gtf', nat='transcriptome')
	transcriptome_file = os.path.basename(transcriptome_path)
		
	genome_path = get_address(organism, thread_info_dict, fileend='.fa', nat='genome')
		
	genome_file = os.path.basename(genome_path)
			
	output_dir = os.path.join(get_experiment_type_dir(thread_info_dict, type='rnaseq'), organism)
		
	tmp = organism.split('_')
	organism = tmp[0][0] + tmp[1]
	write_file_to_path(organism, os.path.join(output_dir, 'organism'))
	logging.debug('%s organism file created to %s', thread_info_dict['dataset'], os.path.join(output_dir, 'organism'))
	write_file_to_path(genome_file, os.path.join(output_dir, 'genome'))
	logging.debug('%s genome file created to %s', thread_info_dict['dataset'], os.path.join(output_dir, 'genome'))
	write_file_to_path(transcriptome_file, os.path.join(output_dir, 'transcriptome'))
		
	logging.debug('%s transcriptome file created to %s', thread_info_dict['dataset'], os.path.join(output_dir, 'transcriptome'))
	
	return True

def prepare_nc(cuffnorm_output, thread_info_dict):
	
	for cuffnorm_path in cuffnorm_output:
		
		filelist = ['samples.table', 'genes.attr_table', 'genes.fpkm_table']
		argument_list = join_files(cuffnorm_path, filelist)
		root_dir = thread_info_dict['root_dir']
		parser_script_path = get_lib_file_path(root_dir, 'cuffnorm_parse.R')
		output_path = os.path.dirname(cuffnorm_path)
		
		command = ["Rscript", parser_script_path] + argument_list + [output_path] + [str(thread_info_dict['assembly']).upper()]
		
		if (os.path.exists(os.path.join(output_path, 'matrix'))):
			logging.debug('%s Parsed matrix from Cuffnorm output already exists', thread_info_dict['dataset'])
			
		else:
			logging.debug('%s Parsing expression matrix from Cuffnorm %s', thread_info_dict['dataset'], command)
			result = run_process(command, 'Rscript')
			
			if(not(result)):
				logging.error('%s Error in cuffnorm_parse.R subprocess %s', thread_info_dict['dataset'], command)
				thread_info_dict[get_species(thread_info_dict)] = 'CFL'
				return False
			
	return True 

def join_files(cuffnorm_output, filelist):
	joined = [ os.path.join(cuffnorm_output, x) for x in filelist]
	return joined

def cuffmerge(transcripts, thread_info_dict):
	logging.debug('%s Cufflinks transcript files inserted into Cuffmerge %s ', thread_info_dict['dataset'], transcripts)
	output_folders = []
	thread_dir = get_thread_dir_path(thread_info_dict)
	rna_folder = os.path.join(thread_dir, 'rnaseq')
	spec_directories = [os.path.join(rna_folder, x) for x in os.listdir(rna_folder) if os.path.isdir(os.path.join(rna_folder, x))]
	logging.debug('%s Starting Cuffmerge ', thread_info_dict['dataset'])
	Queue_handler(1, spec_directories, thread_create, cuffmerge_species, thread_info_dict, output_folders)
	
	return output_folders

def cuffmerge_species(thread_info_dict, output_folders, thr, queue):
	
	directory = queue.get()
	transcript_files = get_files(directory, "transcripts.gtf", None)
	output_folder = os.path.join(directory, 'cuffmerge')
	create_directory(output_folder)
	organism = get_species(thread_info_dict)
	transcriptome_path = get_address(organism, thread_info_dict, fileend='.gtf', nat='transcriptome')
	genome_path = get_address(organism, thread_info_dict, fileend='.fa', nat='genome')
	
	assembly_file = transcripts_to_file(transcript_files, output_folder)	
	
	command = ["cuffmerge", '-g', transcriptome_path, '-p', '20', '-s', genome_path, '-o', output_folder, assembly_file]
	logging.debug('%s Cuffmerge command %s ', thread_info_dict['dataset'], command)
	merged_file = os.path.join(output_folder, 'merged.gtf')
	
	if (os.path.exists(merged_file)):
		logging.debug('%s Cuffmerge merged file already exists ', thread_info_dict['dataset'])
		
	else:
		logging.debug('%s Running Cuffmerge', thread_info_dict['dataset'])
		if(not(run_process(command, 'cuffmerge'))):
			thread_info_dict[get_species(thread_info_dict)]=='CFL'
			logging.error('%s Cuffmerge failed %s', thread_info_dict['dataset'], command)
	output_folders.append(output_folder)
	queue.task_done()
	return True


def transcripts_to_file(transcripts, output_folder):
	assembly_file = os.path.join(output_folder, 'assembly_list.txt')
	with open(str(assembly_file), 'w') as file_:
		for item in transcripts:
			file_.write("%s\n" % item)
	logging.info('Assembly list file created')
	return assembly_file
		

def cuffnorm(thread_info_dict, lib):
	kwargs=thread_info_dict['General']
	species = get_species(kwargs)
	spec_directories = [os.path.join(get_experiment_type_dir(kwargs, type='rnaseq'), species)]
	output_folders = []
	logging.debug('%s Starting Cuffnorm', kwargs['dataset'])
	Queue_handler(1, spec_directories, thread_create, cuffnorm_procedure, thread_info_dict, output_folders, lib)
	return output_folders
	



def cuffquant(mapper_output, thread_info_dict, lib):
	output = []
	logging.debug('%s Start cuffquant with input data %s', thread_info_dict['General']['dataset'], mapper_output)
	Queue_handler(8, mapper_output, thread_create, cuffquant_procedure, thread_info_dict, lib, output)
	return output



def data_folder(file_, thread_info_dict, software_name):
	
	if(software_name == 'trimmed_data'):
		prefix = os.path.basename(file_).split('.')[0].replace('.fastq', '')
	else:
		prefix = os.path.basename(file_).split('_')[0].replace('.fastq', '')
		
	organism = get_species(thread_info_dict)
	job_path = get_experiment_type_dir(thread_info_dict, type='rnaseq')
	
	if(software_name in ['trimmed_data', 'cuffnorm', 'cuffmerge', 'lib_type']):
		output_folder = os.path.join(job_path, organism, software_name)
	elif(software_name == 'fastqc'):
		prefix = os.path.basename(file_).split('.')[0]
		dir_folder = os.path.join(job_path, organism, software_name, prefix + '_fastqc')
		create_directory(dir_folder)
		output_folder = os.path.join(job_path, organism, software_name)
	
		return dir_folder, output_folder
	else:
		output_folder = os.path.join(job_path, organism, software_name, prefix,)
	
	create_directory(output_folder)
	logging.debug('%s Created folder %s', thread_info_dict['dataset'], output_folder)
	return output_folder


def cufflinks(merged_alignment_paths, lib, thread_info_dict):
	output = []
	logging.debug('%s Starting Cufflinks',thread_info_dict['General']['dataset'])
	Queue_handler(4, merged_alignment_paths, thread_create, cufflinks_procedure, thread_info_dict, lib, output)
	return output




def cufflinks_procedure(thread_info_dict, lib, output, thr, queue):
	
	kwargs=thread_info_dict['General']
	file_ = queue.get()
	output_folder = data_folder(file_, kwargs, 'cufflinks')
	
	file_exist = map(lambda x: x in os.listdir(output_folder), ["genes.fpkm_tracking", "isoforms.fpkm_tracking", "skipped.gtf", "transcripts.gtf"])
	logging.debug('%s Cufflinks files exist %s', kwargs['dataset'], file_exist)
	thread_wide = {'thread_info_dict':thread_info_dict, 'output_folder':output_folder, 'file_':file_, 'libtype':lib}
	cuffsuccess = cuff_coreprocess(get_cufflinks_command, 'Cufflinks', file_exist, **thread_wide)
	
	output.append(output_folder)
	queue.task_done()
	
	if(cuffsuccess):
		return True
	else:
		return False
	
	
	

def get_cuffquant_command(file_, lib, thread_info_dict, output_folder):
	kwargs=thread_info_dict['General']
	sample_name = os.path.basename(file_)
	# #
	libtype = convert_libtype(lib[sample_name])
	organism = get_species(kwargs)
	thread_dir = get_thread_dir_path(kwargs)
	cuffmerge_folder = os.path.join(thread_dir, 'rnaseq', organism, 'cuffmerge')
	assembly = kwargs['assembly']
	bam_file = bam_from_file(file_)
	
	if(kwargs['mapper']=='star'):
		bam_file=sort_bam(kwargs,bam_file)
	
	if(assembly):
		gtf_file = os.path.join(cuffmerge_folder, 'merged.gtf')
	else:
		gtf_file = get_address(organism, kwargs, fileend='.gtf', nat='transcriptome')
	
	genome_path = get_address(organism, kwargs, fileend='.fa', nat='genome')
	
	
	str_components,auto_param=software_command_parse(thread_info_dict,'cuffquant')
	
	add_param=auto_param_filtering('cuffquant',auto_param,*[libtype])
	
	if(os.path.exists(gtf_file) and os.path.exists(bam_file)):
		
		subproc = ["cuffquant", '-b', genome_path, '-o', output_folder]+str_components+add_param+[gtf_file, bam_file]
		
		logging.debug('%s Cuffquant command %s', kwargs['dataset'], subproc)
		return subproc
	else:
		logging.error('%s Bam file in %s or GTF file in %s are missing', kwargs['dataset'], bam_file, gtf_file)
		return False
		

def cuffnorm_procedure(thread_info_dict, output, lib, thr, queue):
	kwargs=thread_info_dict['General']
	directory = queue.get()
	cxb_files = get_files(directory, "abundances.cxb", None)

	output_folder = os.path.join(directory, 'cuffnorm')
	create_directory(output_folder)
	
	file_exist = map(lambda x: x in os.listdir(output_folder), ["genes.fpkm_table", "genes.attr_table", "samples.table"])	
	
	thread_wide = {'thread_info_dict':thread_info_dict, 'output_folder':output_folder,
		'cxb_folder':cxb_files}
	logging.debug('%s Cuffnorm core process arguments %s', kwargs['dataset'], thread_wide)
	cuffsuccess = cuff_coreprocess(get_cuffnorm_command, 'Cuffnorm', file_exist, **thread_wide)
	
	output.append(output_folder)
	queue.task_done()
	
	if(cuffsuccess):
	
		return True
	else:

		return False
	
	

def cuffquant_procedure(thread_info_dict, lib, output, thr, queue):
	
	kwargs=thread_info_dict['General']
	file_ = queue.get()
	output_folder = data_folder(file_, kwargs, 'cuffquant')
	
	file_exist = map(lambda x: x in os.listdir(output_folder), ["abundances.cxb"])
	thread_wide = {'thread_info_dict':thread_info_dict, 'output_folder':output_folder, 'file_':file_, 'lib':lib}
	cuffsuccess = cuff_coreprocess(get_cuffquant_command, 'Cuffquant', file_exist, **thread_wide)
	
	output.append(output_folder)
	queue.task_done()
	
	if(cuffsuccess):
		
		return True
	else:

		return False
	
def get_cuffnorm_command(cxb_folder, thread_info_dict, output_folder):
	kwargs=thread_info_dict['General']
	thread_dir = get_thread_dir_path(kwargs)
	organism = get_species(kwargs)
	cuffmerge_output = os.path.join(thread_dir, 'rnaseq', organism, 'cuffmerge')
	
	if(kwargs['assembly']):
		gtf_file = os.path.join(cuffmerge_output, 'merged.gtf')
	else:
		gtf_file = get_address(organism, kwargs, fileend='.gtf', nat='transcriptome')
	
	if(os.path.exists(gtf_file)):
		subproc = ["cuffnorm", '-o', output_folder, '-p', '20',
				gtf_file] + cxb_folder
		return subproc
	else:
		logging.error('%s Merged gtf file is missing', kwargs['dataset'])
		kwargs[organism] = 'CFL'
		return False


def sort_bam(thread_info_dict,bam_file):
	bam_tmp=os.path.join(os.path.dirname(bam_file),'tmp.bam')
	tmp_sort=['mv',bam_file,bam_tmp]
	run_process(tmp_sort, 'mv bam')
	logging.debug('%s Bam moved to %s', thread_info_dict['dataset'],bam_tmp)
	bam_name=bam_file.replace('.bam','')
	
	sort_process=['samtools','sort',bam_tmp,bam_name]
	run_process(sort_process, 'sort bam')
	logging.debug('%s Sorted bam outputted %s', thread_info_dict['dataset'],bam_file)
	
	tmp_sort=['rm',bam_tmp]
	run_process(tmp_sort, 'rm bam_tmp')
	logging.debug('%s Temporary bam removed', thread_info_dict['dataset'])
	
	return bam_file

def get_cufflinks_command(file_, libtype, thread_info_dict, output_folder):
	
	kwargs=thread_info_dict['General']
	sample_name = os.path.basename(file_)
	lib = convert_libtype(libtype[sample_name])	
	organism = get_species(kwargs)
	transcriptome_path = get_address(organism, kwargs, fileend='.gtf', nat='transcriptome')
	bam_file = bam_from_file(file_)
	
	if(kwargs['mapper']=='star'):
		bam_file=sort_bam(kwargs,bam_file)

	else:
		pass
	
	str_components,auto_param=software_command_parse(thread_info_dict,'cufflinks')
	
	add_param=auto_param_filtering('cufflinks',auto_param,*[lib])
	
	
	if(os.path.exists(bam_file)):
		
		subproc = ["cufflinks", "-G", transcriptome_path, '-o', output_folder] +add_param + str_components + [bam_file]
		return subproc
	
	else:
		logging.error('%s Bam file is missing %s', kwargs['dataset'], bam_file)
		return False


def convert_libtype(libtype):
	'''
	Conversion of HTSeq libtypes to Cufflinks
	'''
	librarytypes = {'no':'fr-unstranded',
			'yes':'fr-secondstrand',
			'reverse':'fr-firststrand'
	}
	
	return librarytypes[libtype]
	
	

def cuff_coreprocess(get_cufffunction_command, str_cuff, file_exist, **thread_wide):
	print thread_wide
	dataset=thread_wide['thread_info_dict']['General']['dataset']
	species=thread_wide['thread_info_dict']['General']['species'].replace(' ', '_').lower()
	
	if(all(file_exist)):
		logging.debug('%s All necessary %s files already exist in %s', dataset, str_cuff, thread_wide['output_folder'])
		
		return True
	
	else:
		command = get_cufffunction_command(**thread_wide)
		logging.debug('%s %s command %s', dataset, str_cuff, command)
		if(command):
			result = run_process(command, str_cuff)
			if(not(result)):
				logging.error('%s %s command failed %s', dataset, str_cuff, command)
				thread_wide['thread_info_dict']['General'][species] = 'CFL'
				return False
			else:
				return True
		else:
			logging.error('%s %s command generation failed %s', dataset, str_cuff, command)
			thread_wide['thread_info_dict']['General'][species] = 'CFL'
			return False

def get_command_parse_parameters(thread_info_dict,software):
		
	QC={'split_param':['-a','-b','-g','-A','-B','-G'],'ignore_param':['-o','-p'],'override_param':['-q','-m','M','--quality-base','-u']}
	TopHat={'split_param':[],'ignore_param':['-G','--GTF','--transcriptome_index','-o'],'override_param':['-p','--mate-std-dev','-r','--no-coverage-search']}
	Star={'split_param':[],'ignore_param':['--genomeDir','--outFileNamePrefix','--outSAMtype','--readFilesIn','--outSAMstrandField'],'override_param':['runThreadN','--outFilterType','--outFilterIntronMotifs']}
	Cufflinks={'split_param':[],'ignore_param':['-G','--GTF','-o','--output-dir'],'override_param':['--library-type','-p']}
	Cuffquant={'split_param':[],'ignore_param':['-b','--frag-bias-correct','-o','--output-dir'],'override_param':['--library-type','-p']}
	#Cuffnorm={'split_param':[],'ignore_param':['-o','--output-dir'],'override_param':['-p']}
	HTSeq={'split_param':[],'ignore_param':['-r','--order','-i','--idattr'],'override_param':['-m','--mode','-s','--stranded']}

	dict_={'QC':QC,'tophat':TopHat,'star':Star,'cufflinks':Cufflinks,'cuffquant':Cuffquant,'htseq':HTSeq}
	
	return dict_[software]

	
def software_command_parse(thread_info_dict,software):
		
	if  not software in thread_info_dict:
			
		return [],[]
		
	else:
			
		soft_input=thread_info_dict[software]
		print soft_input
		str_components,auto_param=[],[]
			
		software_dict=get_command_parse_parameters(thread_info_dict,software)
	
		for key,val in soft_input.iteritems():
	
			if key in software_dict['split_param']:
		
				str_components+=map(lambda x: "{0} {1}".format(key,x),  val.split(' '))
		
			elif(key in software_dict['ignore_param']):
		
				pass
				
			elif(key.startswith('--') and val!='' and software!='star'):
				
				str_components+=["{0}={1}".format(key,val).strip()]
			else:
				str_components+=["{0} {1}".format(key,val).strip()]
				
			if key in software_dict['override_param']:
				
				auto_param.append(key)
			
		return str_components,auto_param


def auto_param_filtering(software,auto_param,*args):
	
	padding=[0] * (3 - len(args))
	args=list(args)+padding
	
	QC={'-q':str(25), '-u': str(args[0]),'-m':str(args[1]),'-M': '240','--quality-base': str(args[2])}
	
	
	TopHat={'-p':5,'--segment-length':str(args[0]),'--no-coverage-search':''}
	

	STAR={'runThreadN':5,'--outFilterType':'BySJout','--outFilterIntronMotifs': 'RemoveNoncanonicalUnannotated'}
	Cufflinks={"--library-type": args[0], "-p":"5"}
		
	Cuffquant={"--library-type": args[0], "-p":"5"}
		
	htseq={'-m':'union','-s':args[0],'-r':'name'}
		
	estimated_parameters={'QC':QC,
							'tophat':TopHat,
							'star':STAR,
							'cufflinks':Cufflinks,
							'cuffquant':Cuffquant,
							'htseq':htseq
						}
	
	diff_list=list(set(estimated_parameters[software].keys())-set(auto_param))
	
		
	add_param=[]
	
	for i in diff_list:
	
		if i.startswith('--') and estimated_parameters[software][i]!='' and software!='star':
			add_param.append("{0}={1}".format(i,estimated_parameters[software][i]).strip())

		else: add_param.append("{0} {1}".format(i,estimated_parameters[software][i]).strip()) 
	
	
	return add_param
	
	

def get_tophat2_command(file_, thread_info_dict, output_folder):
	"""
	Returns a list of strings for TopHat2 software to execute.
	"""
	kwargs=thread_info_dict['General']
	organism = get_species(kwargs)
	transcriptome_path = get_address(organism, kwargs, fileend='.gtf', nat='transcriptome')
	
	genome_path = get_address(organism, kwargs, fileend='.fa', nat='genome')
	
	if(not(genome_path and transcriptome_path)):
		return False
	genome_index_path = genome_path.replace('.fa', '')
	transcriptome_index_path = transcriptome_path.replace('.gtf', '')
	segment_size = segment_length(file_)
	
	str_components,auto_param=software_command_parse(thread_info_dict,'tophat')
	
	add_param=auto_param_filtering('tophat',auto_param,*[segment_size])
	
	if(type(file_) is tuple):
	
	# Siin loe sisse lib_path ja sellest s6ltuvalt hinda mate inner std dev ja mean inner dist

		lib_folder = data_folder(file_[0], kwargs, 'lib_type')

		sample_name = (os.path.basename(file_[0]).split('_')[0]).replace('.fastq', '')
		lib_file_path = os.path.join(lib_folder, sample_name)
		if (os.path.exists(lib_file_path)):
		
			BAM_addr = os.path.join(lib_file_path, 'accepted_hits.bam')
			
			mate_std = mate_inner_std(BAM_addr, kwargs)
			logging.debug('%s Mate_std %s', kwargs['dataset'], mate_std)
		# mate_std v22rtused siia sisse panna
			subproc = ["tophat2", "-G", transcriptome_path, '-o', output_folder,
					"--transcriptome-index=" + transcriptome_index_path , '-r', str(mate_std[0]),
					 '--mate-std-dev', str(mate_std[1])]+str_components+add_param+[genome_index_path, file_[0], file_[1]]
		else:
			subproc = ["tophat2", "-G", transcriptome_path, '-o', output_folder,
					"--transcriptome-index=" + transcriptome_index_path]+str_components+ add_param+[genome_index_path,
											 file_[0], file_[1]]
		return subproc
	
	elif(type(file_) is str):
	
		subproc = ["tophat2", "-G", transcriptome_path, '-o', output_folder,
				"--transcriptome-index=" + transcriptome_index_path] + str_components+add_param+ [genome_index_path, file_]
		print subproc
		return subproc
	
	else:
		return 'ERROR'

def mate_inner_std(BAM_addr, thread_info_dict):
	
	
	ps = subprocess.Popen(['samtools', 'view', BAM_addr], stdout=subprocess.PIPE)
	second_ps = subprocess.Popen(['python', '-u', 'lib/getinsertsize.py', '-'], stdin=ps.stdout, stdout=subprocess.PIPE)
	ps.stdout.close()
	ret_val = str(second_ps.communicate()[0])
	logging.debug('%s Get insert size script successfully ran ', thread_info_dict['dataset'])
	second_ps.stdout.close()
	elements = ret_val.strip().split(' ')
	elements = map(lambda x: int(float(x)), elements)
	return (elements)


def segment_length(file_):

	if(type(file) is tuple):
		file_ = file[0]
	else:
		pass
	segment_size = 25
	with open(file_) as fin:
		for line in islice(fin, 1, 2):
			
			segment_size = len(line)
			logging.debug(' Segment size %s', segment_size)
		if (segment_size > 40 and segment_size < 100):
			return segment_size / 2
		else:
			'''Default value'''
			return 25
		
def run_quality_control(thread_info_dict, fastq_files, separated_files):
	"""
	Executes FastX and FastQC software for fastq files.
	"""
	kwargs=thread_info_dict['General']
	logging.debug('%s Mate_std %s', kwargs['dataset'], fastq_files)

	address_value = []
	logging.debug('%s Running FastQC ', kwargs['dataset'])
	Queue_handler(10, separated_files, thread_create, fastq_subprocess, kwargs)
	
	if ('FQC' in thread_info_dict.itervalues()):
		logging.error('%s Running FastQC failed', kwargs['dataset'])
		return False
	
	else:
		logging.debug('%s Running Cutadapt ', kwargs['dataset'])
		Queue_handler(10, fastq_files, thread_create, cutadapt_handler, thread_info_dict, address_value)
		return address_value


def cutadapt_parameters(file_, thread_info_dict):
	
	dir_folder,output_folder = data_folder(file_, thread_info_dict, 'fastqc')
	qual_stat_path = os.path.join(dir_folder, 'fastqc_data.txt')
	
	stat_length = read_qual_file(qual_stat_path, ">>Basic Statistics")
	
	parameters = {}
	
	if (stat_length[3][0] == 'Encoding'):
		phred = encoding(stat_length[3][1])
		if (phred):
			parameters.update({stat_length[3][0]:phred})
		else:
			return False
		
	if (stat_length[5][0] == 'Sequence length'):
		seq_length = sequence_length(stat_length[5][1])
		parameters.update({stat_length[5][0]:seq_length})
	
	if (stat_length[2][0] == 'File type'):
		col_bool = colorspace(stat_length[2][1])
		parameters.update({'Colorspace':col_bool})
	logging.debug('%s parameters %s ', thread_info_dict['dataset'], parameters)	
	
	return parameters

def encoding(Encoding_value):
	
	# Add more encodings here
	
	encodings = {"Sanger / Illumina 1.9":33, "Illumina 1.5":64}
	try:
		phred = encodings[Encoding_value]
	except KeyError:
		logging.error('%s Enconding value error ', Encoding_value)
		return False
	return phred

def colorspace(file_type):

	return True if ('Colorspace' in file_type) else False


def sequence_length(read_lengths):
	
	size_variation = read_lengths.split('-')
	
	return size_variation[0]


def get_QC_args(parameters):
	
	length = parameters['Sequence length']
	quality = parameters['Encoding']
	param_u = int(math.floor(int(length) * 0.95))
	param_m = int(math.floor(int(length) * 0.7))
	arg1=-int(length) + param_u

	return[arg1,param_m,quality]
	
def cutadapt_string(parameters, file_names, output_files,thread_info_dict):	
		
	str_components,auto_param=software_command_parse(thread_info_dict,'QC')

	QC_args=get_QC_args(parameters)

	add_param=auto_param_filtering('QC',auto_param,*QC_args)
	
	if(len(output_files) > 1):
		
		cutadapt_command = ['cutadapt']+add_param+str_components+[ '-o', output_files[0],
				'-p', output_files[1], file_names[0], file_names[1]]
	
	else:
		cutadapt_command = ['cutadapt']+add_param+str_components+['-o'] + output_files + file_names
	
	logging.debug('%s Cutadapt string', cutadapt_command)
	
	return cutadapt_command


def cutadapt_handler(thread_info_dict, address_value, thr, queue):
	kwargs=thread_info_dict['General']
	file_ = queue.get()
	
	if(type(file_) is tuple):
		file_el = file_[0]
	
	elif(type(file_) is str):		
		file_el = file_
		file_ = [file_]
	
	else:
		pass
		
	organism = get_species(kwargs)
	
	output_folder = data_folder(file_el, kwargs, 'trimmed_data')
	
	file_names = map(lambda x: os.path.basename(x), file_)
	
	output_files = map(lambda file_name:os.path.join(output_folder, file_name), file_names)
	
	address_value += output_files
	files_exist = map(lambda x: os.path.exists(x), output_files)

	if(all(files_exist)):
		logging.debug('%s All cutadapt files already exist', kwargs['dataset'])
		queue.task_done()
		return True
		
	else:
			
			parameters = cutadapt_parameters(file_el, kwargs)
			
			if not parameters:
				kwargs[organism] = 'ENC'
				logging.error('%s Encoding wrong', kwargs['dataset'])
				queue.task_done()
				return False
			
			elif(parameters['Colorspace']):
				kwargs[organism] = 'CLR'
				logging.error('%s Reads are colorspace', kwargs['dataset'])
				queue.task_done()
				return False
					
			else:
		
				cutadapt_str = cutadapt_string(parameters, file_, output_files,thread_info_dict)
				logging.debug('%s Running cutadapt', kwargs['dataset'])
				if(not(run_process(cutadapt_str, 'cutadapt'))):
					logging.error('%s Running cutadapt failed ', kwargs['dataset'])
					kwargs[organism] = 'CTA'
				queue.task_done()

	return True

def error_check(kwargs,software):
	
	species=get_species(kwargs)
	
	if species in kwargs:
		logging.error('%s %s generated an error ', kwargs['dataset'],software)
		return True
	else:
		return False

def fastq_subprocess(thread_info_dict, thr, queue):
	
	file_ = queue.get()
	logging.debug('%s FastQ on file %s', thread_info_dict['dataset'], file_)
	organism = get_species(thread_info_dict)
	FastQC_path = 'fastqc'
	dir_folder, output_folder = data_folder(file_, thread_info_dict, 'fastqc')
	qual_stat_path = os.path.join(dir_folder, 'fastqc_data.txt')
	fastqc_string = [FastQC_path, str(file_), '--outdir=' + output_folder]
	
	logging.debug( "%s Processing fastq data with FastQC: ",thread_info_dict['dataset'])
	
	if(os.path.exists(qual_stat_path)):
		logging.debug('%s FastQ already exists %s', thread_info_dict['dataset'], qual_stat_path)
		queue.task_done()
		return True
	
	else:
		logging.debug('%s Running FastQ ', thread_info_dict['dataset'])
		run_process(fastqc_string, 'FastQC')
	
		if(os.path.exists(qual_stat_path)):
			logging.info('%s Running FastQ successful ', thread_info_dict['dataset'])
			queue.task_done()
			return True
		
		else:
			thread_info_dict[organism] = 'FQC'
			logging.error('%s Running FastQ failed ', thread_info_dict['dataset'])
			queue.task_done()
			return False


def read_qual_file(qual_stat_path, sector='>>Overrepresented sequences'):
	pr = False
	list_ = []
	with open(qual_stat_path, 'r') as f:
		for line in f.readlines():
			if (pr and line.find('>>END_MODULE') != -1):
				pr = False
			
			if (pr):
				
				seq_line = line.strip().split('\t')
				list_.append(seq_line)
			if(line.find(sector) != -1):
				pr = True
	return list_
			
			
def extract_adapter(adapter_list, file_):
	adapters = []
	if (len(adapter_list) > 0):
		adapter_list.pop(0)
		for adapter in adapter_list:
			adapter_name = adapter[3].split(',')
			if (adapter_name[0] == 'TruSeq Adapter'):				
				adapters.append('AGATCGGAAGAGC')
			else:
				adapters.append(adapter[0])
	return adapters


'''
def get_base_string(thread_info_dict):
	"""
	Returns the base string for a job script.
	"""	
	dataset = thread_info_dict['dataset']

	JOB_WALLTIME = 'PBS -l walltime=200:00:00\n'
	JOB_CPU = 'PBS -l nodes=1:ppn:60\n'
	JOB_MEM = 'PBS -l vmem=80gb\n'
	JOB_EMAIL = 'PBS -m be\n'
	job_path = get_job_path(thread_info_dict)
	job_name = 'PBS -N ' + dataset + '\n'
	job_work_dir = 'PBS -d ' + job_path + '\n'

	base_string = job_name + JOB_WALLTIME + JOB_CPU + \
					JOB_MEM + JOB_EMAIL + job_work_dir

	return base_string

'''

def Barcode_test(fastq_files):
	for file_ in fastq_files:
		with open(file_, 'r') as f:
			first_line = f.readline()
			first_line = first_line.split("#")
			if (len(first_line) > 1):
				
				return False
			else:
				pass
	return True


def check_stargenome(output_path, organism, thread_info_dict):
	
	genome_address = get_address(organism, thread_info_dict, fileend='.fa', nat='genome')
	
	transcriptome_path = get_address(organism, thread_info_dict, fileend='.gtf', nat='transcriptome')
	
	subprocess = ['STAR', '--runThreadN', '10', '--limitGenomeGenerateRAM', '101000000000', '--runMode', 'genomeGenerate', '--genomeDir', output_path, '--genomeFastaFiles',
	genome_address, '--sjdbGTFfile', 	transcriptome_path, '--sjdbOverhang', '100']
	
	result = run_process(subprocess, 'stargenome')
	
	if(not(result)):
		logging.error('%s StarGenome creation failed. Command used was %s. Please construct it manually', thread_info_dict['dataset'], subprocess)
		logging.error('%s error ', str(sys.exc_info()))
		sys.exit(1)
	
	logging.debug('%s stargenome created with result %s', thread_info_dict['dataset'], result)
	
	return True

def check_bedgene(output_path, species, thread_info_dict):
	
	transcriptome_path = get_address(species, thread_info_dict, fileend='.gtf', nat='transcriptome')
	
	gtf2bedstr = 'gtf2bed < ' + transcriptome_path + ' > ' + os.path.join(output_path, os.path.basename(transcriptome_path).split('.')[0]) + '.bed'
	logging.debug('%s gtf formatted bed gene command %s', thread_info_dict['dataset'], gtf2bedstr)
	if(os.system(gtf2bedstr)):
		logging.error('%s bedgene file was not able to be constructed. Command used was %s. Please construct it manually', thread_info_dict['dataset'], gtf2bedstr)
		logging.error('%s error ', str(sys.exc_info()))
		sys.exit(1)
	
	return True



def separate_files_pairing(files, thread_info_dict, next_=False):
	
	if(files[0].endswith('.fastq')):
		suffix='fastq'
	else:
		suffix='fq'
	paired_end = [[], [], []]
	files.sort()
	
	for file_ in reversed(files):
		fn = os.path.basename(file_)
		if (fn.endswith('_2.'+suffix)):
			paired_end[2].append(file_)
			next_ = True
		elif(next_ == True):
			paired_end[1].append(file_)
			next_ = False
		else:
			paired_end[0].append(file_)
			
	paired_fastq = paired_end[0] + zip(paired_end[1], paired_end[2])
	
	return paired_fastq