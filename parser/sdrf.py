import re
from basic import rows_to_array
import logging

"""
//// SDRF

Functions hierarchy:

parse_sdrf(file_object)
	get_filter_type_and_indexed_dict(rows_array)
		get_rnaseq_column_filter_dict()
		get_affy_column_filter_dict()
		get_sdrf_indexed_columns_dict(rows_array, colum_names_dictionary)
	get_row_filters_dict(filter_type)

"""
def remove_characters_from_rowsarray(rows_array,chr):
	new_rows_array=[]
	for row in rows_array:
		new_row=[element.replace(chr, '') for element in row]
		new_rows_array.append(new_row)
	return new_rows_array

def remove_rows_without_cel(rows_array,thread_info_dict):
	domain=thread_info_dict['domain']
	if(domain=='rnaseq'):
		
		index=rows_array[0].index('comment[ena_experiment]')
		index_list=filter(lambda x:  'RX' in x[index].upper(),rows_array)
	elif(domain=='affy'):
		index=rows_array[0].index('arraydatafile')
		index_list=filter(lambda x:  x[index].upper().endswith('.CEL'),rows_array)
	else:
		return False
	index_list.insert(0,rows_array[0])
	return index_list

def parse_sdrf(file_object,thread_info_dict):
	"""
	Parses through all the rows in a file with the specified filter_type
	filter_type: "affy" or "rnaseq"
	"""
	rows_array = rows_to_array(file_object)
	
	rows_array=remove_characters_from_rowsarray(rows_array,'"')
	rows_array=remove_characters_from_rowsarray(rows_array,'\n')
	#Remove rows that are not completely empty or have only empty elements
	
	def empty_rows(row):
		return not all([element=='' for element in row]) and len(row)>0
	
	rows_array=filter(empty_rows,rows_array)
	
	if type(rows_array) == False or not rows_array:
		logging.error("%s Error with the file",thread_info_dict['dataset'])
		return False
	
	rows_array = get_useful_rows_array(rows_array)
	rows_array=remove_rows_without_cel(rows_array,thread_info_dict)
	#Species filter
	
	def species_rows(rows_array,thread_info_dict):
		domain=thread_info_dict['domain']
		if(domain=='rnaseq'):
			
			species=thread_info_dict['species']
			index=rows_array[0].index('characteristics[organism]')
			rows_array= [row[:index]+[row[index].capitalize()]+row[index+1:] for row in rows_array]
			new_rows_array=[row for row in rows_array if row[index].capitalize()==species.capitalize()]
			new_rows_array.insert(0,rows_array[0])
			return new_rows_array
		
		else:
			return rows_array
	#Siin m22ratakse, mis liikide andmeid t66deldakse.
	
	rows_array=species_rows(rows_array,thread_info_dict)
	logging.debug("%s Filtered species rows",thread_info_dict['dataset'])

	#rows_array=species_rows(rows_array,thread_info_dict,'Homo sapiens')

	filter_type_and_indexed_dictionary = get_filter_type_and_indexed_dict(rows_array)
	if filter_type_and_indexed_dictionary == False:
		logging.error("%s Error with the columns",thread_info_dict['dataset'])
		return False
	filter_type = filter_type_and_indexed_dictionary[0]
	indexed_columns_dict = filter_type_and_indexed_dictionary[1]
	empty_found_columns_dict = filter_type_and_indexed_dictionary[2]
	row_filters_dict = get_row_filters_dict(filter_type)
	if row_filters_dict == False:
		logging.error("%s Error with get_row_filters_dict(filter_type)",thread_info_dict['dataset'])
		return False
	
	correct_rows_array = []
	correct_rows_array.append(rows_array[0])
	for row_index in range(1, len(rows_array)):
		found_columns_dict = empty_found_columns_dict
		found_all = 1
		
		for column in indexed_columns_dict:
			
			column_index = indexed_columns_dict[column]
			row_filter = row_filters_dict[column]
			
			if (not(len(rows_array[row_index])-1 < column_index) and 
				row_filter.search(rows_array[row_index][column_index]) != None):
				
					found_columns_dict[column] = 1
		#for column in found_columns_dict:
		#	if found_columns_dict[column] == -1:
		#		found_all = 0
		
		if found_all == 1:
	
			correct_rows_array.append(rows_array[row_index])
	
	if len(correct_rows_array) == 1:
		logging.error("%s Error with the rows (none are correct)",thread_info_dict['dataset'])
		return False
	return correct_rows_array

def get_filter_type_and_indexed_dict(rows_array):
	"""
	Checks if the columns match with either AFFY or RNA-SEQ columns,
	then returns [filter_type, indexed_columns_dictionary]
	"""
	affy_column_filter_dict = get_affy_column_filter_dict()
	affy_column_names_dictionary = get_sdrf_indexed_columns_dict(rows_array, affy_column_filter_dict)
	if affy_column_names_dictionary != False:
		modify_useful_columns(rows_array)
		logging.info("Modified useful columns for affy")
		return ['affy', affy_column_names_dictionary, get_affy_column_filter_dict()]
	else:
		rnaseq_column_filter_dict = get_rnaseq_column_filter_dict()
		rnaseq_column_names_dictionary = get_sdrf_indexed_columns_dict(rows_array, rnaseq_column_filter_dict)
		if rnaseq_column_names_dictionary != False:
			modify_useful_columns(rows_array)
			logging.info("Modified useful columns for rnaseq")
			return ['rnaseq', rnaseq_column_names_dictionary, get_rnaseq_column_filter_dict()]
		else:
			logging.error(" File is missing some columns")
			return False


def get_rnaseq_column_filter_dict():
	"""
	Returns dictionary of column_name:-1
	"""
	rnaseq_column_filter_dict = dict()
	rnaseq_column_filter_dict['comment[library_strategy]'] = -1
	rnaseq_column_filter_dict['technologytype'] = -1
	rnaseq_column_filter_dict['comment[ena_experiment]'] = -1

	return rnaseq_column_filter_dict

def get_affy_column_filter_dict():
	"""
	Returns dictionary of column_name:-1
	"""
	affy_column_filter_dict = dict()
	affy_column_filter_dict['arraydesignref'] = -1
	affy_column_filter_dict['arraydatafile'] = -1
	affy_column_filter_dict['comment[arrayexpressftpfile]'] = -1

	return affy_column_filter_dict

def get_sdrf_indexed_columns_dict(rows_array, colum_names_dictionary):
	"""
	Returns a dictionary of column_name:index
	"""
	i = 0
	for element in rows_array[0]:
		element = element.replace(' ', '')
		element = element.replace('\n', '')
		element = element.replace('"', '')
		element = element.lower()
		for column_name in colum_names_dictionary:
			if element == column_name:
				colum_names_dictionary[column_name] = i
		#rows_array[0][i] = modify_useful_column(element)
		i += 1

	for column_name in colum_names_dictionary:
		if colum_names_dictionary[column_name] == -1:
			return False
	return colum_names_dictionary 

def get_row_filters_dict(filter_type):
	""" 
	Returns an array of regular expressions to filter AFFY or RNA-SEQ
	"""
	if filter_type == 'affy':
		affy_row_filter_dict = dict()
		affy_row_filter_dict['arraydesignref'] = re.compile('affy|geod',re.I)
		affy_row_filter_dict['arraydatafile'] = re.compile('\.cel$',re.I)
		affy_row_filter_dict['comment[arrayexpressftpfile]'] = re.compile('^.+$')
		return affy_row_filter_dict
	elif filter_type == 'rnaseq':
		rnaseq_row_filter_dict = dict()
		rnaseq_row_filter_dict['comment[library_strategy]'] = re.compile('RNA\-?Seq',re.I)
		rnaseq_row_filter_dict['comment[ena_experiment]'] = re.compile('^.+$')
		rnaseq_row_filter_dict['technologytype'] = re.compile('^.*ssay$')
		return rnaseq_row_filter_dict
	else:
		return False

def modify_useful_column(column_name):
	"""
	Modifies a given column_name.
	"""
	first_part = 0
	
	patterns=['comment\[ena_experiment\]','comment\[ena_run\]','comment\[library_strategy\]','comment\[library_source\]',
			'comment\[instrument_model\]','comment\[arrayexpressftpfile\]','comment\[sample_source_name\]']
	
	if re.search('characteristics\[.*?\]', column_name,re.IGNORECASE) != None:
		first_part = len('characteristics[')

	elif re.search('factorvalue\[.*?\]', column_name) != None:
		first_part = len('factorvalue[')

	# Only for RNA-seq
	
	elif (any(map(lambda x: re.search(x,column_name) !=None,patterns))):
		first_part = len('comment[')

	elif re.match('arraydatafile', column_name) != None:
		column_name = 'MetadataOrder'
		return column_name
	
	if not first_part: last_part = len(column_name)
	else: last_part = len(column_name)-1

	return column_name[first_part:last_part].capitalize()

def modify_useful_columns(rows_array):
	"""
	Returns a rows array with modified columns (see modify_useful_column(column_name)).
	"""
	i = 0
	for element in rows_array[0]:
		rows_array[0][i] = modify_useful_column(element)
		i += 1

	return rows_array

def get_useful_columns_filters_tuple():
	"""
	Returns a tuple of strings to use for filtering out unnecessary data.
	"""
	COLUMN_FILTERS = (
		'sourcename',
		'characteristics\[.*?\]',
		'factorvalue\[.*?\]',
		'comment\[sample_source_name\]',
		'arraydatafile',
		# AFFY
		'arraydesignref',
		'comment\[arrayexpressftpfile\]',
		# RNA-seq
		'comment\[ena_experiment\]',
		'comment\[ena_run\]',
		'comment\[library_strategy\]',
		'comment\[library_source\]',
		'technologytype',
		'comment\[instrument_model\]'
		)

	return COLUMN_FILTERS

def get_useful_rows_array(rows_array):
	"""
	Returns a rows array with data that the parser can use.
	"""
	# sourcename, characteristics[.*?], factorvalue[.*?], comment[sample_source_name], arraydatafile
	# affy - arraydesignref, comment[arrayexpressftpfile]
	# rnaseq - comment[fastq_uri], comment[ena_run]
	
	columns_filters = get_useful_columns_filters_tuple()
	useful_rows_array = []
	i = 0

	for element in rows_array:
		if (len(element) > 1):
			useful_rows_array.append([])

	for column in rows_array[0]:
		column = column.replace(' ', '')
		column = column.replace('\n', '')
		column = column.replace('"', '')
		column = column.lower()
		for column_filter in columns_filters:
			if re.search(column_filter, column) != None:
				useful_rows_array[0].append(column)
				for index in range(1, len(useful_rows_array)):
					if (len(rows_array[index]) > 1):
						useful_rows_array[index].append(rows_array[index][i])
		i+=1
	return useful_rows_array