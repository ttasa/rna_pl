# affy_metadata.py
import re
import os
import sys

from update import *

from basic import *
import numpy
#2.7
#from collections import Counter 
import logging



def create_metadata(thread_info_dict, idf_data, sdrf_data,match_dict=None):
    """
    Creates the metadata files for RNA-seq data.
    """
    type_=thread_info_dict['domain']
    if(type_=='rnaseq'):
        sdrf_data_by_platform=get_sdrf_data_by_platform(sdrf_data,'Organism')
      
    elif(type_=='affy'):
        sdrf_data_by_platform = get_sdrf_data_by_platform(sdrf_data,'Arraydesignref')
        
        #sdrf_data_by_platform = get_sdrf_data_by_platform(sdrf_data,'Compound')
        
    special_metadata_tnc_string = get_special_metadata_tnc_string()
    special_metadata_string = get_special_metadata_string()
    idf_metadata_headers = idf_data[0]
    idf_metadata_body = idf_data[1]
    idf_metadata_tnc_string = get_idf_metadata_tnc_string(idf_metadata_headers)
    idf_metadata_string = get_idf_metadata_string(idf_metadata_body)
    
    if not sdrf_data_by_platform:
        logging.error('%s SDRF data can not be divided into platforms',thread_info_dict['dataset'])
        return False
    for organism in sdrf_data_by_platform:
        logging.debug('%s: Platform',thread_info_dict['dataset'])
        
        sdrf_data = sdrf_data_by_platform[organism]
        
        sdrf_metadata = get_metadata_from_sdrf(sdrf_data,thread_info_dict)
        sdrf_metadata_headers = sdrf_metadata[0]
        sdrf_metadata_body = sdrf_metadata[1:]
        sdrf_metadata_tnc_string = get_sdrf_metadata_tnc_string(sdrf_metadata_headers)
        sdrf_metadata_string = get_sdrf_metadata_string(sdrf_metadata_body)
        
        metadata = (idf_metadata_string + 
                    sdrf_metadata_string)

        metadata_tnc = (idf_metadata_tnc_string + 
                        sdrf_metadata_tnc_string)
        
        additional_metadata_tnc_string = get_additional_metadata_tnc()
        additional_metadata_string = get_additional_metadata(thread_info_dict)
        metadata = metadata + special_metadata_string + additional_metadata_string
        metadata_tnc = metadata_tnc + special_metadata_tnc_string + additional_metadata_tnc_string
        logging.info('%s: Metadata string constructed',thread_info_dict['dataset'])
        if(type_=='rnaseq'):
            organism=organism.replace(' ','_').lower()
            
        metadata_tnc_file_path = get_metadata_file_path(thread_info_dict, organism, 'metadata.tnc',type_)
        metadata_file_path = get_metadata_file_path(thread_info_dict, organism, 'metadata',type_)
       
        
        write_file_to_path(metadata_tnc, metadata_tnc_file_path)
     
        write_file_to_path(metadata, metadata_file_path)
        logging.debug('%s Metadata created for %s',thread_info_dict['dataset'],organism)
        
        if(type_=='rnaseq'):
  
            
            with open(metadata_tnc_file_path) as f:
                rows_array=[]
                tnc_index=[]
                elements_to_skip=[]
                for row in f:
                    split_row = re.search('Ena_run', row)
                    exp_row = re.search('Ena_experiment', row)
                 
                    rows_array.append(split_row)
                    tnc_index.append(exp_row)
                   
            
            if (not(any(tnc_index))):
                logging.error('%s Ena_experiment is not available %s',thread_info_dict['dataset'],organism)
                thread_info_dict[organism]='SDRF'
                return False
                            
            elif(not(any(rows_array))):
               
                match_dict=SRX_SRR_dict(thread_info_dict)
            
                if not(SRX_count_check(match_dict,thread_info_dict)): return False
              
                
                
                ind=numpy.nonzero(tnc_index)
                logging.debug('%s Ena_run data needs to be inserted %s',thread_info_dict['dataset'],organism)
                old_working_dir=thread_info_dict['root_dir']
                fileShort=thread_info_dict['dataset']
                  
                with open(metadata_file_path,'r+') as met_file:
                    lines = [line for line in met_file.readlines()]
                
                array=map(lambda x: x.strip().split('\t'), lines)
                logging.debug('%s Metadata read in and parsed for ENA_RUN insertion %s',thread_info_dict['dataset'],organism)
                SRXlist=array[ind[0]]
                SRR_order=[]
                
                ct=dict((i,match_dict.values().count(i)) for i in match_dict.values())
                
                for x in SRXlist:
                    if ct[x]>1:
                        SRR_order.append('Ambiguous')
                    elif(ct[x]==1):
                        SRR=match_dict.keys()[match_dict.values().index(x)]
                        SRR_order.append(SRR)
                    else:
                        logging.error('%s Metadata suitable files do not have downloadable files %s',thread_info_dict['dataset'],organism)
                        thread_info_dict[organism]='SDRF'
                        return False
                    
                strSRR="\t".join(SRR_order)
                with open(metadata_file_path, "a+") as myfile:
                    myfile.write(strSRR)
                strtnc='{string Ena_run[n]}'
                with open(metadata_tnc_file_path, "a+") as myfile:
                    myfile.write(strtnc)        
                logging.info('%s ENA_RUN field added to metadata %s',thread_info_dict['dataset'],organism)
            else:
                
                logging.info('%s SRR field exists in metadata %s',thread_info_dict['dataset'],organism)
                continue
            
        
    return True

def get_sdrf_data_by_platform(sdrf_data,searched_columnname):
    """
    Returns a dictionary of SDRF data by platform.
    """
    platform_dict = {}
    adr_index = get_arraydesignref_index(sdrf_data,searched_columnname)
    
    sdrf_data_headers = sdrf_data[0]
    
    sdrf_data_body = sdrf_data[1:]

    if (adr_index != False):
        for row in sdrf_data_body:
            platform = row[adr_index]
            element = []
            element.append(row)
            if platform in platform_dict:
                    #Currently only homo_sapiens data
                    
                    # Appends to the existing dictionary entry
                platform_dict[platform] += element
            else:
                    # Creates a new dictionary entry
                header_element = []
                header_element.append(sdrf_data_headers)
                platform_dict[platform] = header_element
                platform_dict[platform] += element
        else: 
            pass
            
        return platform_dict
    return False

def get_arraydesignref_index(sdrf_data,search_string):
    """
    Returns the index for Arraydesignref header.
    """
    
    headers = sdrf_data[0]
    
    i = 0
    
    for header in headers:
        if re.search(search_string,header,re.IGNORECASE) != None:
            return i
        i += 1
    
    logging.error('No header field matches the search string  %s',search_string)

    return False


def unique_cel_rows(cel_files,not_unique):
    
    logging.info('Making cel files unique')
    not_unique=[cel_files.index(cel_file) for cel_file in not_unique]
    return not_unique

def not_unique_celfiles(cel_files):
    not_unique=list(set([x for x in cel_files if cel_files.count(x) > 1])) 
    return not_unique

def unique_celfiles(cel_files):
    unique=list(set([x for x in cel_files if cel_files.count(x) == 1])) 
    return unique


def get_metadata_from_sdrf(sdrf_data,thread_info_dict):
    """
    Returns the metadata from SDRF data.
    """

    domain=thread_info_dict['domain']
    metadata = []
    sdrf_data_headers = sdrf_data[0]
    sdrf_data_body = sdrf_data[1:]
    metadata_indexes = get_metadata_indexes(sdrf_data_headers)
    # Headers
    new_row = []
    for metadata_index in metadata_indexes:
        metadata_header = sdrf_data_headers[metadata_index]
        new_row.append(metadata_header)
    metadata.append(new_row)
    
    unique_column_index = 'METADATAORDER' if domain=='affy' else 'Comment[ENA_EXPERIMENT]'
    
    cel_file_index=int(filter(lambda x: sdrf_data_headers[x].upper() in unique_column_index,xrange(len(sdrf_data_headers)))[0])
    cel_files=[row[cel_file_index] for row in sdrf_data_body]
   
    unique_cel=unique_celfiles(cel_files)
   
    not_unique_cel=not_unique_celfiles(cel_files)
    
    logging.info('%s Celfiles are being tested for uniqueness',thread_info_dict['dataset'])
    rows_to_include_un=unique_cel_rows(cel_files,unique_cel)  
   
   
    ##Kui mingi muu tunnus peale source_name p6hjustab duplikaati siis lendab errorisse
    if(len(not_unique_cel)>0):
        logging.warning('%s There were multiple sources for a single cel file!!!',thread_info_dict['dataset'])
        source_index=int(filter(lambda x: sdrf_data_headers[x].upper() in 'SOURCENAME',xrange(len(sdrf_data_headers)))[0])
        rows_to_include_dup=unique_cel_rows(cel_files,not_unique_cel)
        for row_index in xrange(len(sdrf_data_body)):
            if(row_index in rows_to_include_un):
                new_row=[sdrf_data_body[row_index][metadata_index] for metadata_index in metadata_indexes]
                metadata.append(new_row)
            elif(row_index in rows_to_include_dup):
                new_row = []
                for metadata_index in metadata_indexes:
                    if(metadata_index==source_index):
                        new_row.append('Ambiguous')
                    else:
                        new_row.append(sdrf_data_body[row_index][metadata_index])
                metadata.append(new_row)
    else:
        for row_index in xrange(len(sdrf_data_body)):
            new_row=[sdrf_data_body[row_index][metadata_index] for metadata_index in metadata_indexes]
            metadata.append(new_row)
    logging.info('%s Metadata successfully created from parsed sdrf',thread_info_dict['dataset'])
    return metadata

def is_header_metadata(element, list_of_keywords):
    """
    Returns True if no keywords are found in the header and False if a keyword is found in the header.
    """ 
    for keyword in list_of_keywords:
        
        keyword = keyword.lower()
        element = element.lower()
        re_keyword = re.compile(keyword)
        element=re.sub('[()]', '_', element)
        re_element = re.compile(element)
        if re_keyword.search(element) != None:
            return False
        elif re_element.search(keyword) != None:
            return False
    return True

def get_metadata_indexes(sdrf_data_headers):
    """
    Returns a list of indexes for metadata headers in SDRF data.
    """
    metadata_indexes = []
    sdrf_data_headers_len = len(sdrf_data_headers)
    NOT_METADATA_KEYWORDS = ["Arraydesignref",
                            "Arrayexpressftpfile",
                            "Arraydatafile",
                            "Technologytype",
                            "Library_strategy"]
    for i in range(sdrf_data_headers_len):
        header = sdrf_data_headers[i]
        if is_header_metadata(header, NOT_METADATA_KEYWORDS):
            metadata_indexes.append(i)

    return metadata_indexes

def get_sdrf_metadata_tnc_string(sdrf_data_headers):
    """
    Returns the SDRF content for metadata.tnc file.
    """
    sdrf_metadata_tnc_string = ''
    TYPE = 'array'

    for element in sdrf_data_headers:
        sdrf_metadata_tnc_string += generate_metadata_tnc_row(element, TYPE)
        sdrf_metadata_tnc_string += '\n'

    return sdrf_metadata_tnc_string

def get_sdrf_metadata_string(sdrf_data_body):
    """
    Returns the SDRF content for metadata file.
    """
    ###Unique rows with ambiguous source
    
    sdrf_metadata_string = ''
    converted_sdrf_data_body = convert_rows_array(sdrf_data_body)
    for element in converted_sdrf_data_body:
        sdrf_metadata_string += convert_array_to_string(element)

    return sdrf_metadata_string

def get_idf_metadata_tnc_string(idf_data_headers):
    """
    Returns the IDF content for metadata.tnc file.
    """
    idf_metadata_tnc_string = ''
    TYPE = 'string'

    for element in idf_data_headers:

        idf_metadata_tnc_string += generate_metadata_tnc_row(element, TYPE)
        idf_metadata_tnc_string += '\n'

    return idf_metadata_tnc_string

def get_idf_metadata_string(idf_data_body):
    """
    Returns the IDF content for metadata file.
    """
    idf_metadata_string = ''

    for element in idf_data_body:
        idf_metadata_string += generate_metadata_row(element)
        idf_metadata_string += '\n'

    return idf_metadata_string

def get_special_metadata_tnc_string():
    """
    Returns the special content for metadata.tnc file.
    """
    special_metadata_tnc_string = ''
    special_variables = get_special_variables()

    for element in special_variables:
        special_metadata_tnc_string += generate_metadata_tnc_row(element, 'special')
        special_metadata_tnc_string += '\n'

    return special_metadata_tnc_string

def get_special_metadata_string():
    """
    Returns the special content for metadata file.
    """
    special_metadata_string = ''
    special_variables = get_special_variables()

    for element in special_variables:
        special_metadata_string += generate_metadata_row(element)
        special_metadata_string += '\n'

    return special_metadata_string


def generate_metadata_tnc_row(variable, type_):
    """
    Returns a formatted string for metadata.tnc file.
    """
    PREFIX = '__'
    STRING = 'string'
    ARRAY = '[n]'
    variable = (variable.capitalize() if variable not in ['__SecondaryAccession','__PubMedID'] else variable)
    variable=re.sub('[^A-Za-z0-9_]+', '_', variable)
    #If column_name starts with number then add a subscript to the front 
    variable = ('_'+variable if(re.match('^[0-9].*',variable)!=None) else variable) 
    
    special_variables = get_special_variables()

    for special_variable in special_variables:
        if special_variable == variable:
            return '{' + STRING + " " + PREFIX + variable + '}'

    if type_ == 'array':
        return '{' + STRING + " " + str(variable) + ARRAY + '}'

    elif type_ == 'string':
        return '{' + STRING + " " + variable + '}'

    elif type_ == 'special':
        return '{' + STRING + " " + PREFIX + variable + '}'

def generate_metadata_row(variable):
    """
    Returns a formatted string for metadata file.
    """
    special_variables = get_special_variables()
    file_format = special_variables[0]
    dataset_type = special_variables[1]
    
    if variable == file_format:
        return 'ExpressView 1.1'

    elif variable == dataset_type:
        return 'expression'

    else: return variable

def convert_array_to_string(array):
    """
    Returns a string converted from a list.
    """
    string = ''
    for index in range(len(array)):
        string += array[index]
        if index != len(array)-1:
            string += '\t'
        else:
            string += '\n'
    return string

def get_special_variables():
    """
    Returns special variables.
    """
    FILE_FORMAT = 'FileFormat'
    DATASET_TYPE = 'DatasetType'

    return (FILE_FORMAT, 
            DATASET_TYPE)

def convert_rows_array(rows_array):
    """
    Returns a propery formatted rows array data.
    """
    new_rows_array = []
    row_len = len(rows_array[0])

    for i in range(row_len):
        new_rows_array.append([])

    for row in rows_array:
        for i in range(row_len):
            new_rows_array[i].append(row[i])

    return new_rows_array

def get_metadata_file_path(thread_info_dict, platform, file_name,type):
    """
    Returns a path to a directory in the current directory.
    """

    metadata_dir_path = get_metadata_dir_path(thread_info_dict, platform,type)
    
    create_directory(metadata_dir_path)
    metadata_file_path = os.path.join(metadata_dir_path, file_name)

    return metadata_file_path

def get_metadata_dir_path(thread_info_dict, platform,type):
    """
    Returns a path to a directory in the current directory.
    """
    thread_dir = get_thread_dir_path(thread_info_dict)

    if type == "rnaseq":
        metadata_dir_path = os.path.join(thread_dir,'rnaseq',platform)
    else:
        metadata_dir_path = os.path.join(thread_dir,'affy',platform)

    return metadata_dir_path

def get_additional_metadata_tnc():
    """
    Returns additional content for metadata.tnc file.
    """
    additional_metadata_tnc_string = ''
    DATASET_LINK = 'DatasetLink'
    DATASET_ID = 'DatasetID'

    additional_metadata_tnc_string += generate_metadata_tnc_row(DATASET_ID, 'string')+ '\n'
    additional_metadata_tnc_string += generate_metadata_tnc_row(DATASET_LINK, 'string') + '\n'

    return additional_metadata_tnc_string

def get_additional_metadata(thread_info_dict):
    """
    Returns additional content for metadata file.
    """
    dataset = thread_info_dict['dataset']
    additional_metadata_string = ''

    additional_metadata_string += dataset + '\n'
    additional_metadata_string += generate_dataset_link(dataset)  + '\n'

    return additional_metadata_string

def generate_dataset_link(platform):
    """
    Returns a string represeting a html link for the given platform.
    """
    LINK = 'http://www.ebi.ac.uk/arrayexpress/experiments/'
    dataset_link = LINK + platform

    return dataset_link