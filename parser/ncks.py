# ncks.py
import os
import shutil
from basic import *
import logging


def run_ncks(thread_info_dict, cel_file_dir,exp):
    """
    Executes the ncks program in the specified directory.
    """
    
    command_list = get_ncks_command_list(cel_file_dir,exp,thread_info_dict)
    logging.debug(" %s List of commands for NCKS. %s",thread_info_dict['dataset'],command_list)
    
    platform=os.path.basename(cel_file_dir)
    if all(command_list):
        for command in command_list:    
    
            logging.debug(" %s NCKS command: %s.",thread_info_dict['dataset'],command)    
            if(not run_process(command,"ncks")):
                logging.error(" %s Cannot append nc files: %s.",thread_info_dict['dataset'],platform)
                thread_info_dict[platform]='NCKS'
                return True
            
        return True
    else:
        return False

def get_ncks_command_list(cel_file_dir,exp,thread_info_dict):
    """
    Returns a list for strings for the ncks software to execute.
    """
    type_list=get_type_list(exp,thread_info_dict)
    nc_paths=[os.path.join(cel_file_dir,i+'.nc') for i in type_list]
    
    commands=[get_ncks_command(nc_paths[i], 
        nc_paths[i+1]) for i in xrange(len(nc_paths)-1)]
    
    return commands


   

def get_ncks_command(file_1_path, file_2_path):
    """
    Returns a string for the ncks software to execute.
    """
    if not (os.path.exists(file_1_path) and
            os.path.exists(file_2_path)):
        return False
    command =["ncks","-A",file_1_path,file_2_path]
    return command

