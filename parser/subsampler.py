import random
import sys
import os
import basic
import logging

def subsampler(file,output_folder):
    #name of the input file (fasta or fastq)
    #assumes input file is standard fasta/fastq format
    #number of sequences to subsample
    numSeq = 100000
    logging.debug(" Number of reads to sample FastQ file down to: %s.",numSeq)
    increment = 0
    if(type(file) is tuple):
        file_tmp=file[0]
    else:
        file_tmp=file
    #if it's a fasta file
    if (file_tmp.find(".fasta") != -1):
        increment = 2
    #else if it's a fastq file
    elif (file_tmp.find(".fastq") != -1):
        increment = 4
    #quit if neither
    else:
        logging.error(" %s not a fasta/fastq file: %s.")
    
        sys.exit(0)
 
    FILE = open(file_tmp, 'r')
    totalReads = list()
    index = 0
    for line in FILE:
        if(index % increment == 0):
            totalReads.append(index/increment)
        index += 1
    FILE.close()
    if(len(totalReads) < numSeq):
        sys.stdout.write("You only have "+str(len(totalReads))+" reads!\n")
        numSeq=len(totalReads)
    random.shuffle(totalReads)
    totalReads = totalReads[0: numSeq]
    totalReads.sort()
    if (type(file) is tuple):
        test_file_1=writelines(file[0],totalReads,increment,output_folder,numSeq)
        test_file_2=writelines(file[1],totalReads,increment,output_folder,numSeq)
        return (test_file_1,test_file_2)
    else:
        test_file=writelines(file,totalReads,increment,output_folder,numSeq)
        return test_file

def writelines(fileName,totalReads,increment,output_folder,numSeq=100000):
    FILE = open(fileName, 'r')
    ttl = len(totalReads)
    listIndex = 0
    header,tail=os.path.split(fileName)
    basic.create_directory(output_folder)
    test_file=os.path.join(output_folder,tail)
    with open(test_file, 'w') as f:
        for i in range(0, ttl):
            curRead = ""
            for j in range(0, increment):
                curRead += FILE.readline()
            if (i == totalReads[listIndex]):
                f.write(curRead)
                listIndex += 1
            if(listIndex == numSeq):
                break
    FILE.close()
    f.close()
    return test_file