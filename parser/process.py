import zipfile

from basic import *
from metadata import get_metadata_file_path
import ftplib
import urllib2
from urllib2 import urlopen
from ftplib import FTP, error_perm


def get_affy_parsing_information(rows_array):
    """
    Returns Arraydesignref and Arrayexpressftpfile data.
    """
    result = [[],[]]

    adr = re.compile('Arraydesignref')
    aeff = re.compile('Arrayexpressftpfile')
    mo = re.compile('MetadataOrder')

    i = 0
    for header in rows_array[0]:
        if (adr.search(header) != None or 
            aeff.search(header) != None or
            mo.search(header) != None):
            result[0].append(header)
            elements = []
            for element in rows_array[1:]:
                elements.append(element[i])
            result[1].append(elements)
        i += 1
            
    return result

def get_rnaseq_parsing_information(rows_array):
    """
    Returns Fastq_uri and Organism data.
    """
    result = [[],[]]

    #fu = re.compile('Fastq_uri')
    column_names= ['ENA_EXPERIMENT','ENA_RUN','ORGANISM']
    i = 0
    for header in rows_array[0]:
        if (any([name in header.upper() for name in column_names ]) and 'ORGANISMPART' != header.upper()):  
            result[0].append(header)
            elements = []
            for element in rows_array[1:]:
                elements.append(element[i])
            result[1].append(elements)
        i += 1

    return result

def get_parsing_information(rows_array,thread_info_dict):
    """
    Returns rnaseq or affy data in the form of (type, data).
    """
    #fu = re.compile('Fastq_uri')
    domain=thread_info_dict['domain']
    logging.debug("%s domain",domain)
    if(domain=='affy'):
        adr = 'ARRAYDESIGNREF'
        affy_pi = get_affy_parsing_information(rows_array)
        
        if len(affy_pi[0]) > 1:
            if (adr in affy_pi[0][0].upper() or
                adr in affy_pi[0][1].upper()):
                return ("affy", affy_pi)
            
            else:
                return False
    elif(domain=='rnaseq'):
        fu = 'ENA_EXPERIMENT'
        rnaseq_pi = get_rnaseq_parsing_information(rows_array)
        if len(rnaseq_pi[0]) > 1:    
            if (len(['ENA_EXPERIMENT' for elem in rnaseq_pi[0] if fu in elem.upper()])>0) :
                return ("rnaseq", rnaseq_pi)
            else:
                return False
    else:
        return False

def parse_rnaseq_information(thread_info_dict):
    """
    Parses and downloads rnaseq data.
    
    select distinct(experiment_name),data_available from rnaseq_analyse,rnaseq_data where exp_name=experiment_name;
    
    """

    domain=thread_info_dict['domain']
    dataset=thread_info_dict['dataset']
    server_addr='ftp.sra.ebi.ac.uk'
    
    SRP=[i[0] for i in get_experiment_names(domain+'_data', 'data_available'," experiment_name='"+dataset+"'")]
  
    SRP="".join(SRP) 
    
    ftp = ftplib.FTP(server_addr)
    
    ftp.login()
    
  
    
    target_file='http://www.ebi.ac.uk/ena/data/warehouse/filereport?accession=' + \
        SRP+"&result=read_run&fields=experiment_accession,scientific_name,fastq_ftp,run_accession"    
    
    try:
        f=urllib2.urlopen(target_file)
    except urllib2.URLError, e:
        logging.error('%s Connection to EBI server has timed out: error %s',dataset,e)
        sys.exit(1)
        
    
    line=f.readline()
    line=line.strip('\n').split('\t') 
    org_index=line.index('scientific_name')
    exp_index=line.index('experiment_accession')
    fastq_index=line.index('fastq_ftp')
    
    org_list=[]
    exp_list=[]
    fastq_list=[]
    check_exp_list=[]
    species=thread_info_dict['species']
    for line in f:
        line=line.strip('\n').split('\t') 
        org=line[org_index]
        if(org.lower()!=species.lower()):
            continue
        exp=line[exp_index]
        check_exp_list.append(exp)
        addr=line[fastq_index]
        split_add=addr.split(';')
        if len(split_add)==3:
            split_add=filter(lambda x: re.search('\_[12]\.fast.*',os.path.basename(x))!=None,split_add)
        for addr in split_add:
            if len(addr.strip())>1:
                org_list.append(org)
                exp_list.append(exp)
                fastq_list.append(addr)
            else:
                pass
    ftp.quit()
    
    mtd_checkval=check_mtd(thread_info_dict,check_exp_list)
    
    if(mtd_checkval):
        logging.debug("%s %s",thread_info_dict['dataset'],fastq_list)
        logging.debug("%s %s",thread_info_dict['dataset'],org_list)
        logging.debug("%s %s",thread_info_dict['dataset'],exp_list)
        
        download_fastq_files(thread_info_dict, fastq_list, org_list)
        
        return True
    else:
        return False


def check_mtd(thread_info_dict,check_exp):
    
    organism=thread_info_dict['species']
    organism=organism.replace(' ','_').lower()
    logging.debug('%s Ena_experiment %s',thread_info_dict['dataset'],check_exp)
    metadata_file_path = get_metadata_file_path(thread_info_dict, organism, 'metadata','rnaseq')
    
    with open(metadata_file_path) as f:
        lines = [line for line in f.readlines()]
        
    element=map(lambda x: re.findall('^.RX.*',x),lines)
    
    element=sum(element,[])
    array=element[0].strip().split('\t')
    
    logging.debug('%s Metadata ena_exp %s',thread_info_dict['dataset'],array)
    print check_exp,array
    
    if set(array)==set(check_exp):
        logging.debug('%s Metadata and download files match.',thread_info_dict['dataset'])
        return True
    else:
        logging.error('%s Metadata does not match available download files!',thread_info_dict['dataset'])
        thread_info_dict[organism]='MTD'
        
        return False
        

def parse_ftp_contents(ftp_contents):
    """
    """
    
    parsed_contents = []
    if ftp_contents != False:
        for content in ftp_contents:
            split_content = content.split()
            type_ = split_content[8]
            parsed_contents.append(type_)
    return parsed_contents

def get_ftp_contents(working_dir):
    """
    """
    FTP_ADDRESS = 'ftp-trace.ncbi.nlm.nih.gov'
    ftp_contents = []
    ftp = FTP(FTP_ADDRESS)
    try:
        ftp.login()
        ftp.cwd(working_dir)
        ftp.retrlines('LIST', callback=ftp_contents.append)
        ftp.quit()
    except error_perm, e:
        logging.error("Error with %s %s",working_dir,str(e))
        
        return False

    return ftp_contents


def SRXtoSRR(fastq_uri_list):
    SRR=[]
    for SRX in fastq_uri_list:
        working_dir=os.path.join('sra/sra-instant/reads/ByExp/sra/SRX',SRX[:6],SRX)
        contents=get_ftp_contents(working_dir)
        parsed_contents=parse_ftp_contents(contents)
        SRR.append(parsed_contents)
    return SRR

def download_fastq_files(thread_info_dict, fastq_uri_list, organism_list):
    """
    Downloads rnaseq data.
    """
    file_list = []

    logging.info('%s "Starting downloading RNA-seq data.', thread_info_dict['dataset'])
    queue=Queue.Queue()
    Threads=[]

    for index in xrange(len(fastq_uri_list)):
        file_path = get_fastq_file_path(thread_info_dict, organism_list[index])
        logging.debug("%s",file_path)
        download_file_path=os.path.join(file_path,os.path.basename(fastq_uri_list[index]))
        trunc_dfp=download_file_path.replace('.gz','')
        
        if (not any(map(lambda x: os.path.exists(x), [download_file_path,trunc_dfp]))):
            queue.put(organism_list[index])
            Threads.append(threading.Thread(target=thread_download,
                               args=(fastq_uri_list[index],file_path,thread_info_dict,organism_list[index],queue,)))
            file_list.append(download_file_path)
        
            
        else:
            continue
        
    while not queue.empty():
        time.sleep(2)
        while (threading.activeCount()<=4 and not queue.empty()):
            logging.debug('%s Activate download thread',thread_info_dict['dataset'])
            time.sleep(2)
            thread=Threads.pop()
            thread.start()
    queue.join()
    logging.info('%s "Downloading RNA-seq data complete.', thread_info_dict['dataset'])
    logging.info('%s "Starting unzipping files.', thread_info_dict['dataset'])
    unzip_fastq_files(thread_info_dict, file_list)


    return True
    

def thread_download(uri,file_path,thread_info_dict,organism,queue):
    
    try:
        queue.get()
        logging.info('%s Starting download on a thread',thread_info_dict['dataset'])
        download_file(thread_info_dict, uri, file_path)
        queue.task_done()
        #merge_lanes(file_path)
        return True

    except ValueError:
    
        logging.error('%s Could not convert data to an integer',thread_info_dict['dataset'])
        return False
'''
def merge_lanes(file_path):
    if (os.path.basename(file_path)=='raw_data'):
        return True
    else:
        print file_path
        files=[os.path.join(file_path,filename) for filename in os.listdir(file_path)]
        print files
        separated_files=separate_files_pairing(files)
        print separated_files
        if(len(separated_files[0])>0 and len(separated_files[1])==0):
            output_file=file_path+'_1.fastq'
            subproc=['cat']+separated_files[0]
            with open(output_file, 'w') as f:
                proc = subprocess.Popen(subproc,stdout=f,stderr=subprocess.PIPE)
        elif(len(separated_files[1])>0 and len(separated_files[1])==len(separated_files[2])):
            output_file=file_path+'_1.fastq'
            subproc=['cat']+separated_files[1]
            with open(output_file, 'w') as f:
                proc = subprocess.Popen(subproc,stdout=f,stderr=subprocess.PIPE)
            output_file=file_path+'_2.fastq'
            subproc=['cat']+separated_files[2]
            with open(output_file, 'w') as f:
                proc = subprocess.Popen(subproc,stdout=f,stderr=subprocess.PIPE)
        else:
            print('Unexpected pairing in a single experiment')
            return False
        return True


'''

def get_fastq_file_path(thread_info_dict, organism_list_element):
    """
    Returns the file path for a fastq file.
    """
    thread_dir = get_thread_dir_path(thread_info_dict)
    organism_list_element = organism_list_element.replace(' ', '_').lower()
    #if(order_nr!=''):
    #    fastq_file_dir = os.path.join(thread_dir, 'rnaseq', organism_list_element,'raw_data',order_nr)
    #else:
    fastq_file_dir = os.path.join(thread_dir, 'rnaseq', organism_list_element,'raw_data')
    create_directory(fastq_file_dir)
    file_path = os.path.join(fastq_file_dir)
    return file_path


def download_affy_file(thread_info_dict,uri,file_path):
    """
    Downloads a file.
    """

    logging.info('%s Downloading file: %s',thread_info_dict['dataset'],uri)
    logging.debug("%s %s %s",thread_info_dict['dataset'], file_path, uri)
    print thread_info_dict
    print file_path,'file'
    if "ftp://" in uri:
        try:
            f=urlopen(uri)
            with open(file_path, 'wb') as local_file:
                local_file.write(f.read())
        except:
            logging.info('%s Downloading file failed: %s',thread_info_dict['dataset'],uri)
            return False
    return True

def download_file(thread_info_dict, uri, file_path):
    """
    Downloads a file.
    """
    logging.debug('%s Downloading file %s.', thread_info_dict['dataset'],uri)
    subproc = ["wget","-nc","-P",file_path,uri]
    try:
        logging.info('Download process starting')
        subprocess.check_call(subproc)
    except subprocess.CalledProcessError as e:
        logging.error('%s Download failed, %s is wrong or the %s is not available', thread_info_dict['dataset'],uri, file_path)
    return True

def parse_affy_information(thread_info_dict, affy_information):
    """
    Parses and downloads affy data.
    """
    data_type_list = affy_information[0]
    data_value_list = affy_information[1]
    adr = re.compile('Arraydesignref')
    aeff = re.compile('Arrayexpressftpfile')
    mo = re.compile('MetadataOrder')

    i = 0
    for data_type in data_type_list:
        if (adr.search(data_type) != None):
            arrayexpress_platform_list = data_value_list[i]
        elif (aeff.search(data_type) != None):
            arrayexpress_uri_list = data_value_list[i]
        elif (mo.search(data_type) != None):
            cel_file_list = data_value_list[i]
        i += 1

    download_arrayexpress_files(thread_info_dict, 
                    arrayexpress_uri_list, arrayexpress_platform_list, cel_file_list)
    return True

def download_arrayexpress_files(thread_info_dict, arrayexpress_uri_list, arrayexpress_platform_list, cel_file_list):
    """
    Downloads affy data.
    """
    file_list = []
    temp_dir_list = []


    uri_list = remove_duplicate_uris(arrayexpress_uri_list)
    for uri in uri_list:
        try:
            temp_dir = get_arrayexpress_temp_dir(thread_info_dict)
            create_directory(temp_dir)
            file_name = get_file_name_from_uri(uri)          
            file_path = get_arrayexpress_file_path(temp_dir, file_name)
            download_affy_file(thread_info_dict, uri, file_path)
            file_list.append(file_path)
            temp_dir_list.append(temp_dir)
        except IOError as e:
            return "I/O error({0}): {1}".format(e.errno, e.strerror)
        except ValueError:
            return "Could not convert data to an integer"
        except:
            logging.error('%s Unexpected error.', thread_info_dict['dataset'])
            return "Unexpected error: " + str(sys.exc_info()[0])
    
    logging.info('%s "Downloading microarray data complete.', thread_info_dict['dataset'])

    unzip_arrayexpress_files(thread_info_dict, file_list, temp_dir_list, arrayexpress_platform_list, cel_file_list)
    
    return True
    
def move_arrayexpress_files(thread_info_dict, dir_path, arrayexpress_platform_list, cel_file_list):
    """
    Moves all .CEL files from the temporary directory to a platform specific directory.
    """
    i = 0
    for file_ in cel_file_list:
        dest_dir = get_arrayexpress_dest_dir(thread_info_dict, arrayexpress_platform_list[i])
        source_path = os.path.join(dir_path, file_)
        dest_path = os.path.join(dest_dir, file_)
        create_directory(dest_dir)
        if os.path.exists(source_path):
            os.rename(source_path, dest_path)
        i += 1
    return True    

def get_file_name_from_uri(uri):
    """
    Returns a file name from an uri.
    """
    split_uri = uri.split('/')
    file_name = split_uri[-1:][0]
    return file_name

def get_arrayexpress_dest_dir(thread_info_dict, platform):
    """
    Returns the destination directory for an AFFY file by platform.
    """
    thread_dir = get_thread_dir_path(thread_info_dict)
    dest_dir = os.path.join(thread_dir, 'affy', platform)

    return dest_dir

def get_arrayexpress_temp_dir(thread_info_dict):
    """
    Returns the destination directory for an AFFY file by platform.
    """
    thread_dir = get_thread_dir_path(thread_info_dict)
    temp_dir = os.path.join(thread_dir, 'affy', 'temp')

    return temp_dir

def get_arrayexpress_file_path(dest_dir, file_name):
    """
    Returns the full path for an AFFY file.
    """
    file_path = os.path.join(dest_dir, file_name)

    return file_path

def unzip_arrayexpress_files(thread_info_dict, arrayexpress_file_list, arrayexpress_temp_dir_list, arrayexpress_platform_list, cel_file_list):
    """
    Unzips a list of compressed AFFY files.
    """
    logging.info('%s Unzipping CEL files.', thread_info_dict['dataset'])
    i = 0
   
    
    for file_ in arrayexpress_file_list:

        logging.info('%s Unzipping %s', thread_info_dict['dataset'],file)
        try:
            zf = zipfile.ZipFile(file_)
        except:
            logging.error('%s Error in zip file %s.', thread_info_dict['dataset'],file_)
            thread_info_dict['ZIP']='ZIP'
            return False
           
        for name in zf.namelist():
            zf.extract(name,arrayexpress_temp_dir_list[i])
        zf.close()
        os.remove(file_)
        move_arrayexpress_files(thread_info_dict, arrayexpress_temp_dir_list[i], arrayexpress_platform_list, cel_file_list)
        logging.debug('%s Move unzipped files.', thread_info_dict['dataset'])
        i += 1

    return True

def unzip_fastq_files(thread_info_dict, fastq_list):
    """
    Uncompresses all fastq files.
    """

    for file_ in fastq_list:
        subproc=['gzip','-d',file_]
        run_process(subproc, 'Gzip')
    logging.debug('%s FastQ files are unzipped.', thread_info_dict['dataset'])
    return True


def remove_duplicate_uris(uri_list):
    """
    Removes duplicates uris from a list of uris.
    """
    
    nonempty_urilists=filter(lambda x: x!='', [os.path.join(os.path.dirname(uri),os.path.basename(uri).strip()) for uri in uri_list])
    return list(set(nonempty_urilists))


def download_data(thread_info_dict, rows_array):
    """
    Parses information from parsed sdrf data to download rnaseq or affy data.
    """
    
    parsing_information = get_parsing_information(rows_array,thread_info_dict)
    information_type = parsing_information[0]
    information_value = parsing_information[1]
    
    try:
        if information_type == "rnaseq":
            information_type=parse_rnaseq_information(thread_info_dict)
            return information_type
        elif information_type == "affy":
            parse_affy_information(thread_info_dict, information_value)
            return information_type
        else: 
            logging.error('%s Dataset is uncorrect - missing data.', thread_info_dict['dataset'])
            return False
    except:
        logging.error('%s Unexpected error %s.', thread_info_dict['dataset'],str(sys.exc_info()))
        return False