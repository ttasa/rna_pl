from sdrf import *
from idf import *
from duplicates import *
from process import *
from metadata import *
from clean import *
from r import *
from tab2nc import *
from ncks import *
from organism import *
from rnaseq import *
from movematrix import *
from postprocessing import *
import shutil
import logging


def update_completed_experiment(kwargs,val):
    
    domain=kwargs['domain']
    dataset=kwargs['dataset']
    
    table='rnaseq_analyse' if domain=='rnaseq' else 'affy_data'
    field='exp_name' if domain=='rnaseq' else 'experiment_name'
    
    
    if domain=='rnaseq':
        rnaseq_species=kwargs['species']
        query_string = "update "+table+" set processed='"+val+"', processed_date=curdate() where "+ field +"= '"+dataset+"' and species='"+rnaseq_species+"';"
        
    else:
        query_string="update "+table+" set processed='"+val+"', processed_date=curdate() where "+ field +"= '"+dataset+"';"
    
    print query_string
    logging.debug(query_string)
    return(query(query_string))

def get_sdrf_type(kwargs):
    
    dataset=kwargs['dataset']
    query_string="select sequencing_type from affyseq_experiments where experiment_name='"+dataset+"';"
    query_result=query(query_string)
    type_=[i[0] for i in query_result]
    
    if(len(type_)==1):
        sdrf_type=("".join(type_))
    else:
        sdrf_type=False
    
    return sdrf_type


def get_sdrf_data(kwargs):
    
    root_dir = kwargs['root_dir']
    sdrf_type=get_sdrf_type(kwargs)
    sdrf_file_path = get_sdrf_file_path(kwargs,sdrf_type)
    sdrf_file_object = get_sdrf_file_object(root_dir, sdrf_file_path)
    if(type(sdrf_file_object)!=file): return False
    sdrf_data = get_merged_sdrf_data(sdrf_file_object,kwargs)
    
    try:
        assert type(sdrf_data) == list and len(sdrf_data[0]) > 0
    
    except AssertionError:
    
        sdrf_data=False
        
    
    return sdrf_data
    


def run(thread_info_dict):
    """
    The main method for a thread.
    """
    kwargs=thread_info_dict['General']
    dataset = kwargs['dataset']
    domain=kwargs['domain']

    if(kwargs['meta']):
    
        idf_data = get_idf_data(kwargs)
        sdrf_data=get_sdrf_data(kwargs)
    
        if(not (sdrf_data and idf_data)):
        
            logging.error('%s : Error in IDF/SDRF metadata preparation',dataset)
        
            update_completed_experiment(kwargs,'SDRF')
    
        else:
        
            platform_names=get_platform_names(kwargs,sdrf_data)
            
            logging.debug('%s : Started creating metadata',dataset)
            if(not(create_metadata(kwargs, idf_data, sdrf_data))): return evaluate_experiment(kwargs,platform_names,False)
            logging.debug('%s: Starting downloading data',dataset)
            
            if(not download_data(kwargs, sdrf_data)): return evaluate_experiment(kwargs,platform_names,False)
            
            if('ZIP' in kwargs): return evaluate_experiment(kwargs,platform_names,False)
            logging.info('%s Download of raw files completed ',dataset)
        
    else:
        
        platform_names= get_platform_names(kwargs)  
    
    logging.info('%s Platform names identified are %s',dataset,platform_names)
    
    if(not analysis_pipeline(thread_info_dict)): return evaluate_experiment(kwargs,platform_names,False)
        
    logging.info('%s FastQ files downloaded',dataset)
        
    run_tool_multiplatforms(kwargs,domain,run_tab2nc)
        
    logging.info('%s .nc files created ',dataset)
        
    run_tool_multiplatforms(kwargs,domain,run_ncks)
        
    logging.info('%s .NC files appended (ncks) ',dataset)
        
    run_tool_multiplatforms(kwargs,domain,rename_reorder)
        
    logging.info('%s Postprocessing of final .nc files completed ',dataset)
        
    move_all_matrices(kwargs,domain)
        
    logging.info('%s All data moved ',dataset)
        
    evaluate_experiment(kwargs,platform_names,True)
            
    return True


def evaluate_experiment(kwargs,platform_names,success):
    
    logging.debug( 'Final kwargs : %s ',kwargs )
    
    logging.info('Ending thread: %s',str(kwargs['thread']) )
    
    logging.debug( '%s Platforms: %s ',kwargs['dataset'],platform_names)
    
    experiment_success_decision(kwargs,platform_names,success)
    
    logging.debug('Cleaning data on %s',kwargs['thread'])
    
    #clean_wrapper(kwargs)
    
    return True
    
def experiment_success_decision(kwargs,platform_names,success):
    
    error_experiments=[exp_name for exp_name in platform_names if exp_name in kwargs]
    print error_experiments,'error experiments'
    print platform_names,'platform'
    logging.debug( '%s Experiments with errors %s ',kwargs['dataset'],error_experiments)
    
    str_code=out_analysis_err(kwargs)
    
    if (str_code):
        pass
    
    elif (len(error_experiments)>0 and len(error_experiments)!=len(platform_names)):
        str_code=errorcode_generation(kwargs,error_experiments,platform_names,'X','10')

    elif (len(error_experiments)>0):
        str_code=errorcode_generation(kwargs,error_experiments,platform_names,'','VAR')
    
    elif(not success):
        logging.warning( '%s Experiment failed with an unknown error ',kwargs['dataset'])
        str_code='-1'
        
    elif (len(error_experiments)==0):
        logging.warning( '%s Experiment completed without errors ',kwargs['dataset'])
        str_code='1'
    
    else:
        logging.error('No experiment success criteria satisfied %s',kwargs['thread'])
        return False
    if(kwargs['meta']):
        logging.debug('%s Experiment status update in database. ',kwargs['dataset'])
        update_completed_experiment(kwargs,str_code)
    else:
        logging.debug('%s No experiment status update in database as dataset is not public. ',kwargs['dataset'])
        print 'Experiment exit code: '+str_code,
        logging.debug('%s Experiment exit code %s. ',kwargs['dataset'],str_code)
    
    return True

def out_analysis_err(kwargs):
    err=filter(lambda x: x in kwargs, ['ZIP','MTD'])
    
    if (len(err)>0):
        str_code="".join(err)
        
        logging.warning( '%s Experiment completed with errors: %s ',kwargs['dataset'],str_code)
        return str_code
    
    else:
        return 0
        

def errorcode_generation(kwargs,error_experiments,platform_names,*args):
    
    logging.warning( '%s, %s out of %s platforms contained errors ',kwargs['dataset'],len(error_experiments),len(platform_names))
    error_types=[kwargs[error_exp] for error_exp in error_experiments]
    error_codes=list(set(error_types))
    logging.debug( '%s Error codes are: %s ',kwargs['dataset'],error_codes)
    if (len(error_codes)==1):

        str_code="".join(error_codes)
        str_code=args[0]+str_code
    else:

        str_code=args[1]
        
    logging.warning( '%s Error code prduced: %s ',kwargs['dataset'],str_code)
    
    return str_code

def get_platform_names(kwargs,sdrf_data=None):
    if not kwargs['meta']:
        return [kwargs['species'].replace(' ','_').lower()]
        
    elif ('Arraydesignref' in sdrf_data[0]):
        index_value=sdrf_data[0].index('Arraydesignref')
        
    elif('Organism' in sdrf_data[0]):
        index_value=sdrf_data[0].index('Organism')
    
    else:
        logging.error('%s SDRF does not contain necessary header for platform retrieval.',kwargs['dataset'])
        return False
    domain=kwargs['domain']
    
    exp_names=[experiment_name[index_value] for experiment_name in sdrf_data[1:]]
    
    if(domain=='rnaseq'):
        exp_names=map(lambda x: x.replace(' ','_').lower(),exp_names)
    
    unique_names=list(set(exp_names))
    logging.debug('Platform names are %s', unique_names)
    return unique_names
        
'''
def move_cel_from_local(kwargs):
    
    experiment_dir = get_experiment_type_dir(kwargs,'affy')
    tab2nc_dirs = get_tab2nc_dirs(experiment_dir)
    
    for dest_dir in tab2nc_dirs:
        print (dest_dir)
        base_name = os.path.basename(dest_dir)
        print base_name
        files=os.listdir(os.path.join('JpCEL',base_name,'celfiles'))
        files_path=map(lambda x: os.path.join('JpCEL',base_name,'celfiles',x),files)
        
        map(lambda x: shutil.copy(x,dest_dir),files_path)
        
'''
def analysis_pipeline(thread_info_dict):
    """
    Checks and runs the core pipeline on the data.
    """
    kwargs=thread_info_dict['General']
    domain=kwargs['domain']
    dataset=kwargs['dataset']

    if(domain=='affy'):
            
        run_norm_r_for_all(kwargs)
        logging.info('%s Expression matrix normalized ',dataset)
            
        if(not create_organism_metadata_for_all(kwargs)): return False
        
        logging.info('%s Organism metadata created ',dataset)
        
        return True
        
    elif (domain=='rnaseq'):
        
        logging.debug('%s : Started creating metadata',dataset)
            
        match_dict=SRX_SRR_dict(kwargs)
            
        logging.info('%s FastQ files downloaded',dataset)
        if(run_rnaseq(thread_info_dict,match_dict)): return True
        
        else:
            logging.info('%s Rna-Seq analysis failed',dataset )
            return False


    
def console_output_finished(kwargs):

    dataset = kwargs['dataset']

    LOG_MESSAGE_COMPLETED = "Completed " + dataset + "!"
    logging.info(LOG_MESSAGE_COMPLETED)
    
    return True

def get_sdrf_file_path(kwargs,sdrf_type):
    """
    Returns the file path to a SDRF file from an IDF file path.
    """
    domain=kwargs['domain']
    if(sdrf_type in ['affy','RNA-Seq']):
        logging.debug('%s Experiment is type %s',kwargs['dataset'],domain)
        prefix='.sdrf.txt'
    elif(sdrf_type=='hybrid'):
        logging.debug('%s Experiment is hybrid type',kwargs['dataset'])
        if(domain=='affy'):
            prefix='.hyb.sdrf.txt'
        elif(domain=='rnaseq'):
            prefix='.seq.sdrf.txt'
        else:
            return False
    else:
        return False
    sdrf_file_path = kwargs['dataset']+prefix
    return sdrf_file_path 

def get_sdrf_file_object(root_dir, sdrf_file_name):
    """
    Returns the file object for a SDRF file.
    """
    #cache_directory_path = get_path_to_dir(root_dir, "cache")
    cache_directory_path = get_path_to_dir(root_dir, "cache")
    subproc=['find',cache_directory_path,'-name',sdrf_file_name]
    val=subprocess.Popen(subproc,stdout=subprocess.PIPE)
    out=val.communicate()[0].strip('\n')
    file_object = read_file(out)
    return file_object

def get_merged_sdrf_data(sdrf_file_object,kwargs):
    """
    Returns merged SDRF data from a SDRF file.
    """
    parsed_sdrf_result = parse_sdrf(sdrf_file_object,kwargs)
    logging.debug('%s SDRF data parsed',kwargs['dataset'])
    
    if (type(parsed_sdrf_result) == list and len(parsed_sdrf_result[0]) > 0):
        duplicates = return_duplicates(parsed_sdrf_result)
        if(len(duplicates)>0):
            merged_sdrf = merge_duplicates(duplicates, parsed_sdrf_result)[0]
        else:
            merged_sdrf=parsed_sdrf_result
        logging.debug('%s SDRF data merged',kwargs['dataset'])
        sdrf_file_object.close()
        
        return merged_sdrf
    else:
        logging.error('%s Merging SDRF data failed',kwargs['dataset'])
        return False

def get_idf_data(kwargs):
    """
    Returns IDF data from an IDF file.
    """
    idf_file_name = kwargs['dataset'] + '.idf.txt'
    logging.debug('%s IDF File name',kwargs['dataset'])
    idf_file_object = get_idf_file_object(kwargs['root_dir'], idf_file_name)
    
    parsed_idf_result = parse_idf(idf_file_object)
    idf_file_object.close()

    if (type(parsed_idf_result) == list and len(parsed_idf_result[0]) > 0):
        return parsed_idf_result
    else:
        logging.error('%s IDF parsing failed',kwargs['dataset'])
        return False

def get_idf_file_object(root_dir, idf_file_name):
    """
    Returns the file object for an IDF file.
    """
    idf_directory_path = get_path_to_dir(root_dir, "idf")
    file_object = read_file(os.path.join(idf_directory_path,idf_file_name))

    return file_object
