import sys
import os
import re
import urllib2
import numpy
import threading
import subprocess
import logging
import MySQLdb as mdb
import socket
import time
import Queue


def read_file(path):
    """
    Returns the file object.
    """
    try:
        file_object = open(path, 'rU')
    except IOError as e:
        logging.error("I/O error(%s): (%s)",e.errno, e.strerror)
        return False
    except ValueError:
  
        logging.error("Could not convert data to an integer: ",path)
        return False
    except:
        logging.error("Unexpected error: %s %s",path, sys.exc_info()[0])
        return False
    else:
        return file_object

def write_file(path, content, name):
    """
    Creates a file with the given name and writes the content into it. 
    Returns True if successful.
    """
    try:
        file_path = os.path.join(path, name)
        if os.path.isfile(file_path):
            os.remove(file_path)
        file_object = open(file_path, 'w')
        file_object.write(content)
        file_object.close()
    except IOError as e:
        logging.error("I/O error(%s): (%s)",e.errno, e.strerror)
        return False
    except ValueError:
  
        logging.error("Could not convert data to an integer: ",path)
        return False
    except:
        logging.error("Unexpected error: %s %s",path, sys.exc_info()[0])
        return False
    else:
        file_object.close()
        return True

def write_file_to_path(file_content, file_path):
    """
    Creates a file with the given name and writes the content into it. 
    Returns True if successful.
    """
    try:
        if os.path.isfile(file_path):
            os.remove(file_path)
        file_object = open(file_path, 'w')
        file_object.write(file_content)
        file_object.close()
        
    except IOError as e:
        logging.error("I/O error(%s): (%s)",e.errno, e.strerror)
        return False
    except ValueError:
  
        logging.error("Could not convert data to an integer: ")
        return False
    except:
        logging.error("Unexpected error: %s", sys.exc_info()[0])
        return False
    else:
        file_object.close()
        return True

def rows_to_array(file_object):
    """
    Returns a list of lists: 
    [
    [element1_1, element1_2, ...], 
    [element2_1, element2_2, ...], 
    ...
    ]
    """
    if (type(file_object) == file):
        rows_array = []
        logging.debug("Creating rows array: %s", file_object.name)
        for row in file_object:
            split_row = row.split('\t')
            split_row=map(lambda x: x.strip(),split_row)
            rows_array.append(split_row)
        return rows_array
    else:
        return False

def get_path_to_dir(root_dir, directory):
    """
    Returns a path to a directory in the root directory.
    """
    dir_ = os.path.join(root_dir, directory)

    return dir_

def get_idf_file_names(idf_directory):
    """
    Returns the idf files directory by examining the arguments given for parser.py: 
    one argument returns the whole idf file list in idf/ directory, 
    two arguments returns either the list of first N files in idf/ directory or 
    just a list including the single specified .idf.txt file.
    """
    idf_file_names = os.listdir(idf_directory)
    if len(sys.argv) == 3:
        argument = sys.argv[1]
        split_argument = argument.split('.')
        if (len(split_argument) == 3 and 
            'idf' in split_argument and
            argument in idf_file_names):
            idf_file_names = []
            idf_file_names.append(argument)
        else:
            try:
                argument = int(argument)
                idf_file_names = idf_file_names[0:argument]
            except ValueError:
                logging.error("IDF file names not available")
                return False
    return idf_file_names


def get_path_from_root(root_dir, path_list):
    """
    Returns the full path from the root directory into a new directory.
    For example: /root_dir/path_list[0]/path_list[1]/.../path_list[n]
    """
    new_path_list = [root_dir] + path_list
    path_from_root = '/' + os.path.join(*new_path_list)
    
    return path_from_root

def get_thread_dir_path(thread_info_dict):
    """
    Returns the full path for a directory that the given thread can work in.
    """
    root_dir = thread_info_dict['root_dir']
    thread = thread_info_dict['thread']
    
    thread_dir_name = 'process_' + str(thread)
    thread_dir_path = os.path.join(root_dir, 'data', thread_dir_name)
    
    return thread_dir_path

def evaluate_error(result,error_dict,thread_info_dict,dir_):
    if result in error_dict:
        error_dict[result]
        thread_info_dict[dir_]=error_dict[result]
    return thread_info_dict

def get_lib_file_path(root_dir, file_name):
    """
    Returns the full path for the 'lib' directory in the root working directory.
    """
    return os.path.join(root_dir, 'lib', file_name)

def get_root_from_path(path_):
    """
    This function is probably deprecated and should be removed. 
    """
    dir_ = ''
    i = 0
    while (not (dir_ != 'data' and 
                dir_ != 'final' and
                dir_ != 'lib' and 
                dir_ != 'cache' and
                dir_ != 'idf' and
                dir_ != 'log' and
                dir_ != 'parser') and
                i < 15):
        (path_, dir_) = os.path.split(path_)
        i += 1
    
    return path_

def get_experiment_type_dir(thread_info_dict,type='affy'):
 
    thread_dir_path = get_thread_dir_path(thread_info_dict)
    experiment_dir_path = os.path.join(thread_dir_path, type)

    return experiment_dir_path

def run_tool_multiplatforms(thread_info_dict,exp,tool_function):
    experiment_dir = get_experiment_type_dir(thread_info_dict,exp)

    tool_dirs = get_tool_dirs(experiment_dir)
    logging.debug(" %s Running software on all data.",thread_info_dict['dataset'])
    
    for tool_dir in tool_dirs:
        dir_ = os.path.basename(tool_dir)
        if dir_ in thread_info_dict:
            continue
        tool_function(thread_info_dict, tool_dir,exp)
    
    return True


def get_tool_dirs(experiment_dir):
    
    re_temp = re.compile('temp')
    tool_files_directories = []
    experiment_dirs = os.listdir(experiment_dir)

    for dir_ in experiment_dirs:
        
        tool_dir = os.path.join(experiment_dir, dir_)
        
        if (re_temp.search(dir_) == None and os.path.isdir(tool_dir)):

            tool_files_directories.append(tool_dir)

    return tool_files_directories



def create_directory(dir_path):
    """
    Creates a new directory with the given path.
    """
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    return True

def connect():
    """
    Returns the connection to the database. Must be closed after use with close().
    """
    try:
          
        #logging.info("Hostname %s",socket.gethostname())
        if (socket.gethostname()=='nastik.at.mt.ut.ee'):
            host='localhost'
        else:
            host='nastik'
            
        con = mdb.connect(
                            host,    # address
                            'affy',         # user
                            'parool!',      # password
                            'affyseq_db')   # database
        #logging.debug("Connection %s",con)
        return con

    except mdb.Error, e:
        logging.error("Error %d: %s",e.args[0], e.args[1])
        return False

def query(query_string):
    
    #logging.debug("query string %s",query_string)
    
    con = connect()
    cur = con.cursor()
    cur.execute(query_string)
    rows = cur.fetchall()
    con.close()
     
    return rows

def thread_create(*arg):
    return threading.Thread(target=arg[0], args=arg[1:])

def Queue_handler(thread_count,files,thread_create,*arg):
    queue=Queue.Queue()
    for file in files:
        queue.put(file)
    thread_list = []
    while not queue.empty():
        count=min(thread_count,len(files))
        for thread in range(count):  
            time.sleep(2)
            logging.info( '%s thread',str(thread))
            
            if len(thread_list) < thread_count:
                logging.info('More threads available')
                thread_list.append(thread_create(*arg+(thread,queue,)))
                logging.info('Added new thread: %s', str(thread_list[thread]))    
                thread_list[thread].start()
                logging.info('Started new thread: %s', str(thread_list[thread]))

            elif not (thread_list[thread].isAlive() or queue.empty()):
                logging.info('A thread is dead - starting new thread.')
                logging.info('Current threads: %s', str(len(thread_list)))
                thread_list[thread] = thread_create(*arg+(thread,queue,))
                logging.info('Added new thread: %s', str(thread_list[thread]))
                thread_list[thread].start()
                logging.info('Started new thread: %s', str(thread_list[thread]))
            
            else: pass
        
        time.sleep(2)
    
    queue.join()
    return True

def get_experiment_names(db,selected_column_name ='experiment_name',query_part=''):
  
    if(query_part==''):
        query_string ="select DISTINCT "+ selected_column_name+" from "+db+";"    
    else:
        query_string="select DISTINCT "+ selected_column_name+" from "+db+" where "+ query_part + " ;" 
    print query_string
    #logging.warning(query_string)
   
    return query(query_string)

def get_files(dir_, file_ending,dir_match):
    """
    Returns a list of files with a specific ending from the given directory recursively.
    """
    result_files = []
    
    dir_contents = os.listdir(dir_)
    
    for content in dir_contents:
        
        element = os.path.join(dir_,content)
        if (os.path.isfile(element) and 
            content.endswith(file_ending)):
            if(dir_match==None or os.path.basename(os.path.dirname(element))==dir_match):
                result_files.append(element)
        elif (os.path.isdir(element)):
            
            result_files += get_files(element, file_ending,dir_match)
    
    return result_files


def get_thread_info_dict(root_dir, thread,domain,dataset,species,dataname='dataset'):
    """
    Returns a dictionary of often used data to minimize the amount of arguments.
    Add more data if necessary.
    """
    thread_info_dict = {}

    thread_info_dict['root_dir'] = root_dir
    thread_info_dict['domain'] = domain
    thread_info_dict['thread'] = thread
    thread_info_dict['species']=species
    
    if(os.path.exists(dataset)):
        thread_info_dict['raw_addr']=dataset
        thread_info_dict['meta']=False
        thread_info_dict['dataset']=dataname
    else:
        thread_info_dict['meta']=True
        thread_info_dict['dataset']=dataset
        
    
    return thread_info_dict


def run_process(subproc, software):
    try:
        logging.debug("Running software %s", software)
        logging.debug("Subrocess %s", subproc)
        subprocess.check_call(subproc,stdout=None)
            
    except subprocess.CalledProcessError as e:
        logging.error('Subprocess failed, running %s failed %s:%s ',software,e,e.returncode)
        return 0
    #    
    return True
    #    

def get_address(organism,thread_info_dict,fileend='.gtf',nat='transcriptome'):
    
    root_dir=thread_info_dict['root_dir']
    transcriptome_path=os.path.join(root_dir,'lib',nat,organism)
    if(not(os.path.exists(transcriptome_path))):
        os.makedirs(transcriptome_path)
    files=os.listdir(transcriptome_path)
    for file_ in files:
        if file_.endswith(fileend):
            return (os.path.join(transcriptome_path,file_))
    
    logging.error('FATAL ERROR: %s for %s not found.  Please create/download files ending with %s.' %(nat,organism,fileend))
    os._exit(1)
    return False

def get_species(thread_info_dict):
    
    return thread_info_dict['species'].replace(' ', '_').lower()

def SRX_count_check(match_dict,thread_info_dict):
    
    try:
        assert (len(set(match_dict.values()))>4)
        logging.debug( '%s Enough distinct SRX samples, %s ',
                       thread_info_dict['dataset'],str(len(set(match_dict.values()))))
        return True
    
    except AssertionError:
        logging.error( '%s Not enough distinct SRX samples, only %s ',
                       thread_info_dict['dataset'],str(len(set(match_dict.values()))))

        thread_info_dict['MTD']='MTD'
        return False

def SRX_SRR_dict(thread_info_dict):
    
    
    if(thread_info_dict['meta']):
    
        type_=thread_info_dict['domain']
        SRP=[i[0] for i in get_experiment_names(type_+'_data', 'data_available'," experiment_name='"+thread_info_dict['dataset']+"'")]
      
        SRP="".join(SRP) 
        target_file='http://www.ebi.ac.uk/ena/data/warehouse/filereport?accession=' + \
                        SRP+"&result=read_run&fields=experiment_accession,run_accession"    
                          
        f=urllib2.urlopen(target_file)
        np=numpy.loadtxt(fname=f,dtype='str',skiprows=1,unpack=True)
            
        match_dict=dict(zip(np[1,:],np[0,:]))
            
        logging.debug('%s %s',thread_info_dict['dataset'],match_dict)
    
    else:
        match_dict=match_dict_nometa(thread_info_dict)
        
    return match_dict


def match_dict_nometa(thread_info_dict):
    rawdata_addr=thread_info_dict['raw_data']
    try:
        assert os.path.exists(rawdata_addr)
    except AssertionError as error:
        logging.error('%s Input directory does not exist!',thread_info_dict['dataset'])
        logging.error('%s USER ERROR! Please provide a correct directory with FastQ files instead of %s',os.path.abspath(rawdata_addr))
        sys.exit(1)
    print rawdata_addr
    fq_files=os.listdir(rawdata_addr)
    header=["".join(file_.split('.')[:-1]) for file_ in fq_files if file_.split('.')[-1] in ['fq','fastq']]
    try:
        assert len(header)>0
        match_dict=dict(zip(header,header))
    except AssertionError as error:
        logging.error('%s Raw input files did not contain any FastQ files!',thread_info_dict['dataset'])
        logging.error('%s USER ERROR! Please provide an address with FastQ files',thread_info_dict['dataset'])
        sys.exit(1)
    
    return match_dict

def get_type_list(exp,thread_info_dict):
    """
    Returns a specific list of strings to use for tab2nc software.
    """
    if(exp=='affy'):
        TYPE_LIST = ['organism', 'metadata', 'cdfinfo','matrix']
    
    elif(exp=='rnaseq'):
        if(thread_info_dict['quantifier']=='cufflinks'):
            TYPE_LIST= ['organism', 'genome','transcriptome','cuffmeta','matrix']
    
        else:   
            TYPE_LIST= ['organism', 'genome','transcriptome','matrix']
        
        if(thread_info_dict['meta']):
        
            TYPE_LIST=['metadata']+TYPE_LIST
    
    return TYPE_LIST
