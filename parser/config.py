import ConfigParser
import sys
import os
import basic
import MySQLdb as mdb

def get_config():
    
    Config=ConfigParser.ConfigParser()
    Config.optionxform=str
    if len(sys.argv)>1:
        if sys.argv[1].endswith('.conf'): Config.read(sys.argv[1])
    else:
        Config.read('affy.conf')
    thread_info_dict = dict()
    sections=Config.sections()
    for section in sections:
        options = Config.options(section)
        section_dict=dict()
        for option in options:
            section_dict [option] = Config.get(section, option)
        thread_info_dict[section]=section_dict
    
    return thread_info_dict

def assert_attribute(str_,kwargs):
        msg='USER ERROR: Configuration file incorrectly specified. '+str_+' is missing from specified attributes: '+", ' ".join(kwargs.keys())+'.'                                                                                             
        try:
            
            assert str_ in kwargs.keys(),msg
        except AssertionError as e:
            print(str(e))
            sys.exit(1)
        return True

def assert_software(str_,kwargs):
    
    if str_=='mapper':
        software=['tophat','star']
    elif str_=='domain':
        software=['rnaseq','affy']
    else:
        software=['cufflinks','htseq']
            
    msg='USER ERROR: '+str_+' is not specified as one of '+", ".join(software)+'. Given as '+kwargs[str_]+'.'                                                                                             
        
    try:
        assert kwargs[str_] in software,msg
    except AssertionError as e:
        print(str(e))
        sys.exit(1)
    return True


def assertions(kwargs):
    assert_attribute('domain',kwargs)
    assert_software('domain',kwargs)
    domain=kwargs['domain']
    
    if domain=='rnaseq':
        
        assert_software('mapper',kwargs)
        assert_software('quantifier',kwargs)
    
        assert_attribute('species',kwargs)
        kwargs['species']=kwargs['species'].replace('_',' ')
    return True

def query_experiments(multi,kwargs):
    
    domain=kwargs['domain']
   
    try:
        if(domain=='rnaseq'):
            query_str=get_rnaseq_query_strings(multi,kwargs)
            nr_exp=len(basic.get_experiment_names(domain+'_analyse', 'exp_name',query_str))
            
        else:
            query_str=get_affy_query_strings(multi,kwargs)
            nr_exp=len(basic.get_experiment_names(domain+'_data', 'experiment_name',query_str))
            print nr_exp
    
    except mdb.Error:
        print 'USER ERROR'
        print 'Query string for domain {0} is incorrect: {1}! Please correct it!'.format(domain,query_str)
        sys.exit(1)
    return nr_exp,query_str


def get_rnaseq_query_strings(multi,kwargs):
    
    if multi:
        QUERY_STR=" species='"+kwargs['species']+"' and "+kwargs['query_string']
    else:
        QUERY_STR='exp_name="'+kwargs['dataset']+'" and species="'+kwargs['species']+'"'
    
    return QUERY_STR


def get_affy_query_strings(multi,kwargs):

    if multi:
        QUERY_STR=kwargs['query_string']
    else:
        QUERY_STR=' experiment_name="'+kwargs['dataset']+'"'

    return QUERY_STR

def test_run(kwargs):
    if ('test' in kwargs):
        assert_attribute('domain',kwargs)
        assert_software('domain',kwargs)
        print 'Test run for domain {0}'.format(kwargs['domain'])
        return True
    else:
        return False
            
def rawdata_multiples(kwargs):
    
    dirs=[os.path.join(kwargs['raw_data'],dir_) for dir_ in os.listdir(kwargs['raw_data']) if os.path.isdir(os.path.join(kwargs['raw_data'],dir_))]
    fq_files=[fq_file for fq_file in os.listdir(kwargs['raw_data']) if fq_file.endswith('.fq') or fq_file.endswith('.fastq')]
    print dirs, fq_files
    if len(dirs)==0 and len(fq_files)>2:
        print('Own data files specified at {0}'.format(kwargs['raw_data']))
        path='single_owndata'
    elif len(dirs)>0 and len(fq_files)==0:
        print('Own data folders specified at {0}: {1}'.format(kwargs['raw_data'],", ".join(fq_files)))
        
        fastq_dirs=map(lambda dir_: sum(map(lambda fq_file: fq_file.endswith('.fq') or fq_file.endswith('.fastq'),os.listdir(dir_))),dirs)
        
        if(all(map(lambda x: x>1,fastq_dirs))):
            path='multiple_owndata'
        else:
            print 'USER ERROR'
            print 'Some directories in the data input folder contain less than 2 FastQ files. Please collect or reorganize the input data hierarchy!'
            sys.exit(1)

    
    else:
        
        print 'Number of FastQ files in specified folder is {0} and number of analysis directories is {1}'.format(len(fq_files),len(dirs))
        print 'USER ERROR. \n' + \
            'In case of multiple input datasets please organize the data into separate folders in the input data folder. \n '+ \
            'In case of a single input dataset, please lose the directories from the input path!'
        sys.exit(1)
    
    return path

def test_thread_info(thread_info_dict):
    kwargs=thread_info_dict['General']
    domain=kwargs['domain']
    for key in thread_info_dict.keys():
        if key != 'General':
            thread_info_dict[key]={}
            
    thread_info_dict['General']={}
    thread_info_dict['General']['domain']=domain
    thread_info_dict['General']['meta']=True
    thread_info_dict['General']['mapper']='tophat'
    thread_info_dict['General']['quantifier']='htseq'
    
    if 'thread' in kwargs:
        thread_info_dict['General']['thread']=kwargs['thread']
    path='test'
    return path

def config_check(thread_info_dict):
    try:
        kwargs=thread_info_dict['General']
    except:
        print('ERROR: Configuration file misspecified.\n  Heading "[General]" missing from the configuration file')
        sys.exit(1)
    if(test_run(kwargs)):
        path=test_thread_info(thread_info_dict)
        return path
    
    assertions(kwargs)
    
    domain=kwargs['domain']
    
    if (not('dataset' in kwargs or 'raw_data' in kwargs)):
        
        assert_attribute('query_string',kwargs)
        path='public_query'
        kwargs['meta']=True
        multi=True
        nr_exp,query_str=query_experiments(multi,kwargs)
        
        if(nr_exp==0):
            print 'USER ERROR'
            print 'Number of results from the database for domain {0} is incorrect. Dataset queried was {1}! Please correct the query string "{2}"!'.format(
                    domain,kwargs['dataset'],query_str)
            sys.exit(1)
        
            
    else:
        if 'raw_data' in kwargs and domain=='rnaseq':
          

            raw_datadir=os.path.abspath(kwargs['raw_data'])            
            if(os.path.exists(raw_datadir)):
                home_dir=os.getcwd()
                os.chdir(kwargs['raw_data'])
                kwargs['raw_data']=os.path.abspath('.')
                os.chdir(home_dir)
                kwargs['meta']=False
                if (os.listdir(raw_datadir)>0):
                    pass
                elif os.listdir(os.path.dirname(raw_datadir))>0:
                    kwargs['raw_data']=os.path.dirname(raw_datadir)
                else:
                    print 'USER ERROR'
                    print('Specified user path '+kwargs['raw_data']+' does not contain any files!')
                    sys.exit(1)
            
                path=rawdata_multiples(kwargs)    
                
            else:
                print 'USER ERROR'
                print('Path '+kwargs['raw_data']+' does not exist!')
                sys.exit(1)
                
        ##Raw_data for dataset: please create a softlink if does not exist. Otherwise download metadata.
        elif ('dataset' in kwargs):
            if 'query_string' in kwargs:
                print 'WARNING'
                print 'Query string "{0}" in conf file ignored as dataset {1} was specified!'.format(kwargs['query_string'],kwargs['dataset'])
            kwargs['meta']=True
            path='one_exp'
            multi=False
    
            nr_exp,query_str=query_experiments(multi,kwargs) 
            if nr_exp!=1:
                print 'USER ERROR'
                print 'Number of results from the database for domain {0} is incorrect. Dataset queried was {1}! Please correct the query string "{2}"!'.format(
                      domain,kwargs['dataset'],query_str)
                sys.exit(1)
        else:
            print 'USER ERROR'
            print('Configuration file erroneous! Please correct it!')
            sys.exit(1)
    
    print('Configuration file OK!')
    
    return path
        