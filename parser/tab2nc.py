# netcdf.py
import os
import shutil
import re
from basic import *
import logging


def run_tab2nc(thread_info_dict, cel_file_dir,exp='affy'):
    """
    Executes the tab2nc program in the specified directory.
    """
    if not setup_tab2nc(thread_info_dict, cel_file_dir,exp):
        logging.error("%s Setting tab2nc files up failed.",thread_info_dict['dataset'])
        return False
    
    command_list = get_tab2nc_command_list(thread_info_dict,cel_file_dir,exp)

    for command in command_list:    
        run_process(command,'tab2nc')
    
    return True


def get_tab2nc_command_list(thread_info_dict,cel_file_dir,exp):
    """
    Returns a list for strings for the tab2nc software to execute.
    """
    command_list = []
    type_list = get_type_list(exp,thread_info_dict)
    for type_ in type_list:
        tnc_file_name = type_ + '.tnc'
        tnc_file_path = os.path.join(cel_file_dir, tnc_file_name)
        nc_file_name = type_ + '.nc'
        nc_file_path = os.path.join(cel_file_dir, nc_file_name)
        regular_file_path = os.path.join(cel_file_dir, type_)
        command = ["tab2nc",tnc_file_path,regular_file_path,nc_file_path]
        command_list.append(command)
    return command_list


def setup_tab2nc(thread_info_dict, cel_file_dir,exp):
    """
    Copies the matrix.tnc file to the specified directory.
    """
    root_dir = thread_info_dict['root_dir']
    if not is_tab2nc_possible(thread_info_dict, cel_file_dir,exp):
        return False
    
    if(exp=='affy'):
        FILE_NAME_LIST = ['matrix.tnc', 'cdfinfo.tnc', 'organism.tnc']
    else:
        FILE_NAME_LIST = ['matrix.tnc', 'genome.tnc', 'organism.tnc','transcriptome.tnc']
        
    for file_name in FILE_NAME_LIST:
        
            tnc_file_path = os.path.join(root_dir, 'lib', file_name)
            new_tnc_file_path = os.path.join(cel_file_dir, file_name)
            logging.debug("%s Copying TNC files.",thread_info_dict['dataset'])
            logging.debug("%s Copying TNC file %s %s.",thread_info_dict['dataset'], tnc_file_path, new_tnc_file_path)
            try:
                shutil.copyfile(tnc_file_path, new_tnc_file_path)
            except IOError as e:
                thread_info_dict[os.path.basename(cel_file_dir)]='T2C'
                logging.error("%s Can not setup tab2nc - missing necessary files %s.",thread_info_dict['dataset'],tnc_file_path)
                
    
    
    return True

def is_tab2nc_possible(thread_info_dict, cel_file_dir,exp):
    """
    Returns a boolean value to see if executing tab2nc software is possible
    in the given directory.
    """
  
    type_list = get_type_list(exp,thread_info_dict)
    for type_ in type_list:
    
        regular_file_path = os.path.join(cel_file_dir, type_)
        
        if not os.path.exists(regular_file_path):
            
            logging.error("%s tab2nc is not possible - missing necessary files %s.",thread_info_dict['dataset'],type_)
            
            thread_info_dict[os.path.basename(cel_file_dir)]='T2C'
            
            return False
            
    return True





