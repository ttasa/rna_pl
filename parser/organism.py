# organism.py
import os
from basic import *
import logging

def create_organism_metadata_for_all(thread_info_dict):
    """
    Creates the organism metadata files for all platforms.
    """
    root_dir = thread_info_dict['root_dir']
    affy_dir = get_experiment_type_dir(thread_info_dict)
    cel_files_dir = get_tool_dirs(affy_dir)
    
    for cel_file_dir in cel_files_dir:
        dir_ = os.path.basename(cel_file_dir)
        if dir_ in thread_info_dict:
            logging.error('%s %s has encountered an error and wont be processed!"',thread_info_dict['dataset'],dir_)

            continue
        if not create_organism_metadata(root_dir, cel_file_dir):
            logging.error('Something wrong with data.',thread_info_dict['dataset'])
            
            return False
    return True

def create_organism_metadata(root_dir, cel_file_dir):
    """
    Creates the organism metadata files for a platform.
    """
    metadata = get_organism(cel_file_dir, root_dir)
    if metadata != False:
        metadata_file_path = os.path.join(cel_file_dir,'organism')
        write_file_to_path(metadata, metadata_file_path)
        return True
    else:
        return False

def get_organism_data_path(root_dir):
    """
    Returns a string holding the full path to array_target_list.txt file.
    """
    organism_data_path = os.path.join(root_dir, 'lib', 'array_target_list.txt')

    return organism_data_path

def get_organism_data_dict(root_dir):
    """
    Returns a dictionary with organism data.
    """
    organism_data_dict = {}
    organism_data_path = get_organism_data_path(root_dir)
    file_object = read_file(organism_data_path)
    rows_array = rows_to_array(file_object)
    for row in rows_array:
        key = row[0]
        value = row[3]
        organism_data_dict[key] = value

    return organism_data_dict

def get_organism(cel_file_dir, root_dir):
    """
    Returns an organism from cdfname.
    """
    organism = ''
    organism_data_dict = get_organism_data_dict(root_dir)
    cdfname = get_cdfname(cel_file_dir)
    logging.debug('Cdfname %s',cdfname)
    if cdfname != False:
        try:
            organism = organism_data_dict[cdfname]
        except KeyError as e:
            logging.error('Key not in array_list.txt')
           
            return False
        return organism
    else:
        return False

def get_cdfname(cel_file_dir):
    """
    Returns the content from cdfinfo file.
    """

    cdfinfo_path = os.path.join(cel_file_dir,'cdfinfo')

    file_object = read_file(cdfinfo_path)

    rows_array = rows_to_array(file_object)
    if rows_array != False:
        cdfname = rows_array[0][0]
        cdfname = cdfname.replace("\n", "")
        logging.debug('Found cdfname: %s',cdfname)
        
        return cdfname
    else:
        logging.error('Missing cdfname!')
        return False

