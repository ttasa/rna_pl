# update.py
import MySQLdb as mdb
import os
import pickle
import shutil
import sys
import struct
sys.path.insert(0,'/group/software/public/affyseq_dev/parser')
from basic import get_files
from ftplib import FTP
from basic import *
from idf import *
#from basic import create_directory
import time
import ftplib
from datetime import datetime, date

"""
DATABASE: affyseq_db
USER: affy
PASSWORD: parool!

TABLE: affyseq_experiments
+-------------------+-------------+------+-----+---------+----------------+
| Field             | Type        | Null | Key | Default | Extra          |
+-------------------+-------------+------+-----+---------+----------------+
| id                | int(11)     | NO   | PRI | NULL    | auto_increment |
| experiment_type   | varchar(4)  | NO   |     | NULL    |                |
| experiment_name   | varchar(20) | NO   | UNI | NULL    |                |
| server_version    | date        | NO   |     | NULL    |                |
| local_version     | date        | YES  |     | NULL    |                |
| is_local_updated  | tinyint(1)  | YES  |     | 0       |                |
| processed_version | date        | YES  |     | NULL    |                |
+-------------------+-------------+------+-----+---------+----------------+
"""


def run():
    """
    """
    USAGE = 'Usage:\n' + \
            'sync - gets ftp data dates and updates database dates ' + \
            'check - reports old datasets ' + \
            'update - replaces old datasets sdrf and idf files with new ones ' + \
            'python update.py -s            : syncs the database with live data\n' + \
            'python update.py -s type       : -,,- for the experiment type\n' + \
            'python update.py -s type name  : -,,- for the specific experiment\n' + \
            'python update.py -u            : updates all datasets\n' + \
            'python update.py -c            : checks all datasets\n' + \
            '\nExample:\n' + \
            'python update.py -s MEXP E-MEXP-1'

    SYNC =              '-s'
    UPDATE =            '-u'
    CHECK =             '-c'

    if len(sys.argv) == 2:
        if sys.argv[1] == SYNC:
            return sync()
        elif sys.argv[1] == UPDATE:
            return update()
        elif sys.argv[1] == CHECK:
            return check()
    elif len(sys.argv) == 3:
        if (sys.argv[1] == SYNC and
            len(sys.argv[2]) == 4):
            return sync_specific(sys.argv[2])
    elif len(sys.argv) == 4:
        if (sys.argv[1] == SYNC and
            len(sys.argv[2]) == 4 and
            len(sys.argv[3]) > 7):
            return sync_specific_name(sys.argv[2], sys.argv[3])
    return USAGE


"""
### FTP

"""

def get_ftp_contents(working_dir):
    """
    """
    FTP_ADDRESS = 'ftp.ebi.ac.uk'
    ftp_contents = []
    ftp = FTP(FTP_ADDRESS)
    try:
        ftp.login()
        ftp.cwd(working_dir)
        ftp.retrlines('LIST', callback=ftp_contents.append)
    except:
        print("Error with " + working_dir + ": " + str(working_dir))
        return False

    return ftp_contents

def parse_ftp_contents(ftp_contents):
    """
    """
    parsed_contents = {}
    if ftp_contents != False:
        for content in ftp_contents:
            split_content = content.split()
            experiment_list = split_content[5:]
            month = monthToNum(experiment_list[0])
            day = experiment_list[1]
            year = experiment_list[2]
            type_ = experiment_list[3]
            if ":" in year:
                year = date.today().year
            parsed_contents[type_] = str(year) + '-' + str(month) + '-' + str(day)

    return parsed_contents

def get_experiment_type_list():
    """
    """
    FTP_DIRECTORY = 'pub/databases/microarray/data/experiment/'
    working_dir = FTP_DIRECTORY
    ftp_contents = get_ftp_contents(working_dir)
    experiment_type_list = parse_ftp_contents(ftp_contents)    
    return experiment_type_list

def get_experiment_list(experiment_type):
    """
    """
    FTP_DIRECTORY = 'pub/databases/microarray/data/experiment/'
    working_dir = FTP_DIRECTORY + experiment_type.upper()
    ftp_contents = get_ftp_contents(working_dir)
    experiment_list = parse_ftp_contents(ftp_contents)

    return experiment_list

def get_experiment_file_list(experiment_type, experiment_name):
    """
    """
    FTP_DIRECTORY = 'pub/databases/microarray/data/experiment/'
    working_dir = FTP_DIRECTORY + experiment_type.upper() + '/' + experiment_name.upper()
    ftp_contents = get_ftp_contents(working_dir)
    experiment_file_list = parse_ftp_contents(ftp_contents)

    return experiment_file_list

def get_experiment_server_version(experiment_type, experiment_name):
    """
    """
    sdrf_file_name = experiment_name + '.sdrf.txt'
    sdrf_hybrid_name= experiment_name + '.seq.sdrf.txt'
    idf_file_name = experiment_name + '.idf.txt'
    experiment_file_list = get_experiment_file_list(experiment_type, experiment_name)
    print experiment_file_list
    if len(experiment_file_list) > 0:
        try:
            if (sdrf_file_name in experiment_file_list):
                sdrf_date_string = experiment_file_list[sdrf_file_name]
            else:
                sdrf_date_string = experiment_file_list[sdrf_hybrid_name]
            idf_date_string = experiment_file_list[idf_file_name]
            sdrf_date = get_date_from_string(sdrf_date_string)
            idf_date = get_date_from_string(idf_date_string)
            latest_date = max([sdrf_date,idf_date])
            return latest_date
        except KeyError, e:
            print("Error: " + str(e))


"""
### DATABASE
"""



def insert_server_experiment(exp_type, exp_name, exp_date):
    """
    """
    DB_TABLE = "affyseq_experiments"
    query_string =  "INSERT INTO " + DB_TABLE + " " + \
                    "(experiment_type, experiment_name, server_version) VALUE " + \
                    "('" + \
                        exp_type + "','" + \
                        exp_name + "','" + \
                        str(exp_date) + \
                    "')" + \
                    ";"
    return query(query_string)

def update_server_experiment(exp_type, exp_name, exp_date):
    """
    """
    DB_TABLE = "affyseq_experiments"
    FALSE = 0
    query_string =  "UPDATE " + DB_TABLE + " " + \
                    "SET server_version=" + "'" + str(exp_date) + "'" + "," + \
                    "is_local_updated=" + str(FALSE) + " " + \
                    "WHERE experiment_type=" + "'" + exp_type + "'" + " " + \
                    "AND experiment_name=" + "'" + exp_name + "'" + \
                    ";"
    return query(query_string)

def insert_local_experiment_version(exp_name, exp_date):
    """
    """
    DB_TABLE = "affyseq_experiments"
    query_string =  "UPDATE " + DB_TABLE + " " + \
                    "SET local_version=" + "'" + str(exp_date) + "'" + " " + \
                    "WHERE experiment_name=" + "'" + exp_name + "'" + \
                    ";"

    return query(query_string)

def get_experiment(exp_name, exp_type=''):
    """
    """
    DB_TABLE = "affyseq_experiments"
    if(exp_type!=''):
        query_string =  "SELECT * FROM " + DB_TABLE + " " + \
                    "WHERE experiment_type=" + "'" + exp_type + "'" + \
                    "AND experiment_name=" +    "'" + exp_name + "'" + \
                    ";"
    else:
        query_string =  "SELECT * FROM " + DB_TABLE + " " + \
                    "WHERE experiment_name=" +    "'" + exp_name + "'" + \
                    ";"
                    
    return query(query_string)



def set_experiment_updated(exp_type, exp_name):
    """
    """
    DB_TABLE = "affyseq_experiments"
    TRUE = 1
    query_string =  "UPDATE " + DB_TABLE + " " + \
                    "SET is_local_updated=" + str(TRUE) + " " + \
                    "WHERE experiment_type=" + "'" + exp_type + "'" + " " + \
                    "AND experiment_name=" + "'" + exp_name + "'" + \
                    ";"
    return query(query_string)

def insert_experimenttype_table(table_name,sequencing_type):
    
    query_string=  "INSERT INTO "+table_name+" (experiment_name) select experiment_name from affyseq_experiments "+ \
    " where sequencing_type='"+sequencing_type+"' AND experiment_name NOT IN (select experiment_name from "+table_name+");"
    print query_string
    return query(query_string)

def insert_processed_version(exp_name, exp_process_date):
    """
    """
    DB_TABLE = "affyseq_experiments"
    query_string =  "UPDATE " + DB_TABLE + " " + \
                    "SET processed_version=" + str(exp_process_date) + " " + \
                    "WHERE experiment_name=" + "'" + exp_name + "'" + \
                    ";"
    return query(query_string)

"""
### MISC

"""
def get_date_from_string(date_string):
    """
    """
    date = datetime.strptime(date_string,'%Y-%m-%d').date()

    return date

def monthToNum(date):
    return{
            'Jan' : '01',
            'Feb' : '02',
            'Mar' : '03',
            'Apr' : '04',
            'May' : '05',
            'Jun' : '06',
            'Jul' : '07',
            'Aug' : '08',
            'Sep' : '09', 
            'Oct' : '10',
            'Nov' : '11',
            'Dec' : '12'
    }[date]

def bit_to_bool(bit_value):
    if bit_value == '\x00':
        print False
    elif bit_value == '\x01':
        print True

"""
### LOCAL

"""
def get_local_experiment(exp_name):
    """
    """
    sdrf_file_name = exp_name + ".sdrf.txt"
    idf_file_name = exp_name + ".idf.txt"
    sdrf_dir_name = 'cache'
    idf_dir_name = 'idf'
    cur_dir = os.getcwd()
    sdrf_file = os.path.join(cur_dir,sdrf_dir_name,sdrf_file_name)
    idf_file = os.path.join(cur_dir,idf_dir_name,idf_file_name)
    if(not(os.path.exists(sdrf_file) and os.path.exists(idf_file))):
        return None
    try:
        sdrf_date = os.stat(sdrf_file)[8]
        idf_date = os.stat(idf_file)[8]
        latest_ctime = time.ctime(max([sdrf_date, idf_date]))
        latest_date = datetime.strptime(latest_ctime, "%a %b %d %H:%M:%S %Y").date()
        local_experiment = {exp_name:latest_date}
        return local_experiment
    except OSError, e:
        ERROR_MESSAGE = "Something wrong with "+ exp_name +" - "\
                                "probably some of the files are missing."
        print(ERROR_MESSAGE)
        
def get_local_metadata_file_path(exp_name, file_type):
    parent_dir = '..'
    cur_dir = os.getcwd()

    if file_type == "sdrf":
        file_name = exp_name + ".sdrf.txt"
        dir_name = 'cache'
    elif file_type == "idf":
        file_name = exp_name + ".idf.txt"
        dir_name = 'idf'
    
    file_path = os.path.join(cur_dir, parent_dir, dir_name, file_name)
    return file_path

"""
### SYNC

"""
def sync():
    """
    """
    old_working_dir = os.getcwd()
    new_working_dir = os.path.join(old_working_dir,'cache')
    exp_type_list = get_experiment_type_list()
    for exp_type in exp_type_list:
        exp_list = get_experiment_list(exp_type)
        i = 0
        for exp_name in exp_list:
            exp_log = str(exp_type) + ";" + str(exp_name)
            process_exp_name_result = process_experiment_name(exp_type, exp_name)
            print(process_exp_name_result + exp_log)
            i += 0
    return "\nCompleted syncing server experiments.\n"
def sync_specific(exp_type):
    """
    """
    exp_list = get_experiment_list(exp_type)
    i = 0
    for exp_name in exp_list:
        exp_log = str(exp_type) + ";" + str(exp_name)
        process_exp_name_result = process_experiment_name(exp_type, exp_name)
        print(process_exp_name_result + exp_log)
        i += 0
    return "\nCompleted syncing server experiments.\n"

def sync_specific_name(exp_type, exp_name):
    """
    """
    exp_log = str(exp_type) + ";" + str(exp_name)
    process_exp_name_result = process_experiment_name(exp_type, exp_name)
    print(process_exp_name_result + exp_log)
    return "\nCompleted syncing server experiment.\n"

def process_experiment_name(exp_type, exp_name):
    exp = get_experiment(exp_name,exp_type)
    print exp
    print exp_type, exp_name
    exp_ser_version = get_experiment_server_version(exp_type, exp_name)
    print exp_ser_version
    if exp_ser_version == None:
        return ("Could not access server files for: ")
    elif len(exp) > 0:
        process_ser_exp_result = process_server_experiment(
                                                    exp_type, 
                                                    exp_name, 
                                                    exp_ser_version)
        return (process_ser_exp_result)
    else:
        insert_server_experiment(exp_type, exp_name, exp_ser_version)
        return ("Inserting: ")
    return "Processing experiment name failed: "

def process_server_experiment(exp_type, exp_name, exp_ser_version): 
    loc_exp = get_local_experiment(exp_name)
    if loc_exp != None:
        exp_loc_version = loc_exp[exp_name]
        if exp_ser_version > exp_loc_version:
            update_server_experiment(exp_type, exp_name, exp_ser_version)
            return "Updating: "
        else:
            return "Up to date: "
    else:
        update_server_experiment(exp_type, exp_name, exp_ser_version)
        return "Processing server experiment failed since some files were missing \n " + \
            'These will be downloaded in the next update: '

"""
### UPDATE

"""

def update():
    #not_updated_experiments = get_not_updated_experiments()
    #download_not_updated_experiments(not_updated_experiments)
    print("Completed downloading SDRF and IDF files.")
    '''Move files in the cache folder to correct directories'''
    process_sdrf_files()
    #sync_not_updated_experiments(not_updated_experiments)    
    print("Completed syncing local experiments.")
    return True

def process_sdrf_files():
    #move_uncategorized_experiments()
    #fill_tables()
    #Siit RNA-Seq vs Hybrid workflow.
    
    
    #sort_rnaseq_experiments('RNA-Seq')
    #sort_rnaseq_experiments('hybrid')
    check_data_availability_rna('RNA-Seq')
    #check_data_availability_rna('hybrid')
    
    #experiment_superseries('rnaseq_data')
    #fill_rnaseq_analyse_table('RNA-Seq')
    #fill_rnaseq_analyse_table('hybrid')
    #get_experiment_size()
    #experiment_superseries('affy_data')
    #platforms=read_existing_platforms()
    #print platforms
    #check_data_availability_affytb(platforms,'affy')
    #check_data_availability_affytb(platforms,'hybrid')
    #process_query_all()
    return True

def fill_tables():
    
    insert_experimenttype_table("rnaseq_data","RNA-Seq")
    print 'all'
    
    insert_experimenttype_table("affy_data","affy")
    insert_experimenttype_table("affy_data","hybrid")
    insert_experimenttype_table("rnaseq_data","hybrid")
    return True

def update_sequencing_type(sequencing_type, exp_name):
    """
    """
    DB_TABLE = "affyseq_experiments"
    FALSE = 0
    query_string =  "UPDATE " + DB_TABLE + " " + \
                    "SET sequencing_type=" + "'" + str(sequencing_type) + "' "  + \
                    "WHERE experiment_name=" + "'" + exp_name + "'" + " " + ";"
    return query(query_string)

def update_superseries(superseries, exp_name,table_name):
    """
    """
    FALSE = 0
    query_string =  "UPDATE " + table_name + " " + \
                    "SET SuperSeries=" + "'" + str(superseries) + "' "  + \
                    "WHERE experiment_name=" + "'" + exp_name + "'" + " " + ";"
    return query(query_string)


def update_rnaseq_analyse(fileShort,key, value):
    DB_TABLE = "rnaseq_analyse"
    query_string =  "INSERT INTO " + DB_TABLE + " " + \
                "(exp_name, species, samples) VALUE " + \
                "('" + \
                    fileShort + "','" + \
                    key + "','" + \
                    str(value) + \
                "')" + \
                ";"
    return query(query_string)

def uncounted_rnaseq_Exp(type):
    
    query_string="SELECT rnaseq_data.experiment_name FROM rnaseq_data,affyseq_experiments "+\
    "WHERE rnaseq_data.experiment_name=affyseq_experiments.experiment_name "+ \
    "and rnaseq_data.data_available LIKE 'SRP%' and affyseq_experiments.sequencing_type='"+type+"' "+\
    "and rnaseq_data.experiment_name NOT IN (select exp_name from rnaseq_analyse);"

    return query(query_string)

def update_fulltranscriptome(sequencing_type, exp_name):
    """
    """
    DB_TABLE = "rnaseq_data"
    FALSE = 0
    query_string =  "UPDATE " + DB_TABLE + " " + \
                    "SET full_transcriptome=" + "'" + str(sequencing_type) + "' "  + \
                    "WHERE experiment_name=" + "'" + exp_name + "'" + " " + ";"
    return query(query_string)

def update_data_availability(DB_table,sequencing_type, exp_name):
    """
    """
    FALSE = 0
    query_string =  "UPDATE " + DB_table + " " + \
                    "SET data_available=" + "'" + str(sequencing_type) + "' "  + \
                    "WHERE experiment_name=" + "'" + exp_name + "'" + " " + ";"
    print query_string
    return query(query_string)

def update_size(exp_name, species, size):
    """
    """
    FALSE = 0
    query_string =  "UPDATE rnaseq_analyse " + \
                    "SET size=" + "'" + str(size) + "' "  + \
                    "WHERE exp_name=" + "'" + exp_name + "'" + " AND species = '" + species +"' ;"
    print query_string
    return query(query_string)

def process_query_all():
    processed_query('affy_data')
    #processed_query('rnaseq_data')

    return True

def processed_query(db_table):
    query_string= "update "+db_table+" set processed='0' where data_available in ('1','2') and processed is NULL;"
    print query_string
    return query(query_string)
    
def move_uncategorized_experiments():
    old_working_dir = os.getcwd()
    new_working_dir = os.path.join(old_working_dir,'cache')
    files=os.listdir(new_working_dir)
    for fileShort in files:
        file=os.path.join(new_working_dir,fileShort)
        exp_name=fileShort.replace('.sdrf.txt','')
        if (os.path.isfile(file)):
            with open(file) as f:
                head = [next(f).strip('\n').replace('"',"").replace(" ","").split('\t') for x in xrange(2)]
                if(any(x in file for x in ['.hyb.sdrf.','.seq.sdrf.'])):
                    if('.hyb.sdrf.txt' in fileShort):
                        exp_name=fileShort.replace('.hyb.sdrf.txt','')
                    else:
                        exp_name=fileShort.replace('.seq.sdrf.txt','')
                    dir_type='hybrid'
                elif( any (x in head[0] for x in ['ArrayDesignREF'])) :
                    dir_type='affy'
                elif ( any(x in head[0] for x in ['Comment[ENA_EXPERIMENT]','Comment[ENA_RUN]','Comment[LIBRARY_STRATEGY]'])):
                    dir_type='RNA-Seq'
                elif('TechnologyType' in head[0]):
                    index=head[0].index('TechnologyType')
                    value=head[1][index]
                    if(value in ['sequencing assay','high_throughput_sequencing']):
                        dir_type='RNA-Seq'
                    elif(value=='array assay'):
                        dir_type='affy'
                    else:
                        dir_type='other'
                else:
                    dir_type='other'
                if( len(get_experiment(exp_name))==1):
                    update_sequencing_type(dir_type,exp_name)
                else:
                    exp_type=exp_name.split('-')[1]
                    process_experiment_name(exp_type, exp_name)
    return True

def find_SRP(string):
    print string
    if (string.find('RP')!=-1):
        return string
    else:
        return False
    
def is_superseries(string):
    if (string.find('SuperSeries')!=-1):
        return True
    else:
        return False

def is_superseries_or_SRP(parsed_idf_result,function=find_SRP,value="__SECONDARYACCESSION"):
    header=parsed_idf_result[0]
    upper_header=map(lambda x: x.upper(), header)
    print upper_header,value
    print value in upper_header
    if value in upper_header:
        index=upper_header.index(value)
        string=parsed_idf_result[1][index]
        print string,'string'
        result=function(string)
        return result
    else:
        return False

def get_idf_file_object(root_dir, idf_file_name):
    """
    Returns the file object for an IDF file.
    """
    idf_directory_path = get_path_to_dir(root_dir, "idf")
    file_object = read_file(os.path.join(idf_directory_path,idf_file_name))

    return file_object

def prepare_idf(old_working_dir,fileShort):
    idf_file_object = get_idf_file_object(old_working_dir,fileShort+'.idf.txt')
    parsed_idf_result = parse_idf(idf_file_object)
    idf_file_object.close()
    return parsed_idf_result

def superser_vs_SRP(parsed_idf_result):
    superseries=is_superseries_or_SRP(parsed_idf_result,is_superseries,"EXPERIMENTDESCRIPTION")
    if superseries:
        return ('SuperSeries')
    else:
        print parsed_idf_result
        SRP=is_superseries_or_SRP(parsed_idf_result,find_SRP,"__SECONDARYACCESSION")
        print SRP
        if(SRP):
            return (SRP)
        else:
            return ('Other')





def check_data_availability_rna(type):
    old_working_dir = os.getcwd()
    new_working_dir = os.path.join(old_working_dir,'cache')    
    if(type=='RNA-Seq'):
        prefix='.sdrf.txt'
        
    elif(type=='hybrid'):
        prefix='.seq.sdrf.txt'
    else:
        return False   
    
    query_string="select rnaseq_data.experiment_name from rnaseq_data, affyseq_experiments " +\
    "where rnaseq_data.experiment_name=affyseq_experiments.experiment_name and " +\
    " affyseq_experiments.sequencing_type='"+type+"' and rnaseq_data.full_transcriptome='1';"
       
    raw_query_results=query(query_string)
    files = [exp[0] for exp in raw_query_results]  
    ##"Other test"
   
    #files=[i[0] for i in get_experiment_names('rnaseq_data','experiment_name'," full_transcriptome='1'")] 
    #files=os.listdir(new_working_dir)
    org_dict={}
    for fileShort in files:
        print fileShort
        local_dict={}
        run_list=[]
        
        file=os.path.join(new_working_dir,fileShort+prefix)
        if (os.path.isfile(file)):
            with open(file) as f:
                line=f.next().strip('\n').replace('"',"").replace(" ","").split('\t')
                #Andmebaasi uuendamine ja t2itmine SRP v22rtustega
                if (any([i in line for i in ['Comment[FASTQ_URI]','Comment[ENA_RUN]','Comment[ENA_EXPERIMENT]']])):
                    parsed_idf_result=prepare_idf(old_working_dir,fileShort)
                    result=superser_vs_SRP(parsed_idf_result)
                    print result
                    update_data_availability('rnaseq_data',str(result), fileShort)
                else:
                    update_data_availability('rnaseq_data',str(0), fileShort)
    return True
               
def fill_rnaseq_analyse_table(type):
    
    old_working_dir = os.getcwd()
    new_working_dir = os.path.join(old_working_dir,'cache')
    
    if(type=='RNA-Seq'):
        prefix='.sdrf.txt'
    elif(type=='hybrid'):
        prefix='.seq.sdrf.txt'
    else:
        return False 
    
    files=[i[0] for i in uncounted_rnaseq_Exp(type)] 
   
    for fileShort in files:
        print fileShort
        local_dict={}
        run_list=[]
        file=os.path.join(new_working_dir,fileShort+prefix)
        if (os.path.isfile(file)):
            with open(file) as f:
                line=f.next().strip('\n').replace('"',"").replace(" ","").split('\t')
                line=map(lambda x: x.upper(), line)
                org_index=line.index('Characteristics[Organism]'.upper())
                exp_index=line.index('Comment[ENA_EXPERIMENT]'.upper())
                #fastq_index=line.index('Comment[FASTQ_URI]'.upper())
                for line in f:
                    line=line.strip('\n').replace('"',"").replace('\'',"").split('\t')
                    #if(len(line[fastq_index])>0):
                    if line[org_index] not in local_dict: 
                        local_dict[line[org_index]]=1
                        run_list.append(line[exp_index])
                    elif (line[exp_index] not in run_list):
                        local_dict[line[org_index]]+=1
                        run_list.append(line[exp_index])
                    else: pass

                for key,value in local_dict.iteritems():
                    update_rnaseq_analyse(fileShort,key,value)
            #Add to data base the filename, species and nr of samples gathered, if fastq_uri then ena=1
                #filter_dict=[key for key,value in local_dict.iteritems() if value >=5]
                #for key in filter_dict:
                #    if key not in org_dict:
                #        org_dict[key]=1
                #    else:
                #        org_dict[key]+=1
                #  break
              
    #add='/group/software/public/affyseq_dev/exp_listENA.txt'
    #import csv
    #file_object=csv.writer(open(add,'wb'))
    #for key, val in org_dict.items():
    #    file_object.writerow([key, val])
 #   return org_dict
 
                #Counters for organism, filter at least 5.
    return True

def read_existing_platforms():
    array_target_file=get_lib_file_path(os.getcwd(), 'array_target_list.txt')
    with open(array_target_file,'r') as array_file:
        rows_array = []
        for row in array_file:
            split_row = row.split('\t')
            rows_array.append(split_row[1])
    return rows_array
            
            
            
def check_data_availability_affytb(existing_platforms,type):
    
    old_working_dir = os.getcwd()
    if (type=='affy'):
        prefix='.sdrf.txt'
        
    elif(type=='hybrid'):
        prefix='.hyb.sdrf.txt'
    else:
        return False   
    
    query_string="select affy_data.experiment_name from affy_data,affyseq_experiments "+ \
        "where affy_data.experiment_name=affyseq_experiments.experiment_name and "+ \
        "affyseq_experiments.sequencing_type='"+ type+ "' ;"
    raw_query_results=query(query_string)
    
    #Siia andmebaasidest puuduolevate andmete v2lja v6tmine, et igakoord uuesti ei peaks tegema.
    new_working_dir = os.path.join(old_working_dir,'cache')
    files=[i[0] for i in raw_query_results] 
    for fileShort in files:
        print fileShort
        file=os.path.join(new_working_dir,fileShort+prefix)
        assigned=False
        wrong_platforms=False
        right_platforms=False
        if (os.path.isfile(file)):
            with open(file) as f:
                line=f.next().strip('\n').replace('"',"").replace(" ","").split('\t')
                if all([i in line for i in ['ArrayDataFile','Comment[ArrayExpressFTPfile]','ArrayDesignREF']]):
                    index1=line.index('ArrayDataFile')
                    index2=line.index('ArrayDesignREF')
                    for line in f:
                        line=line.strip('\n').replace('"',"").replace(" ","").split('\t')
                        if(len(line)==1):
                            continue
                        if(line[index2] not in existing_platforms):
                            wrong_platforms=True
                        else:
                            right_platforms=True
                        if( line[index1].upper().endswith('.CEL')):
                            assigned=True
                        else:
                            pass
                    if(assigned==False):
                        update_data_availability('affy_data',str(0), fileShort)
                    elif(assigned==True and right_platforms==True and wrong_platforms==True):
                        update_data_availability('affy_data',str(2), fileShort)
                    elif(assigned==True and right_platforms==True and wrong_platforms==False):
                        update_data_availability('affy_data',str(1), fileShort)
                    elif(assigned==True and right_platforms==False and wrong_platforms==True):
                        update_data_availability('affy_data',str(-1), fileShort)
                else:
                    update_data_availability('affy_data',str(0), fileShort)
    return True



import urllib2

def experiment_superseries(table_name):
    
    working_dir=os.getcwd()
    files=[i[0] for i in get_experiment_names(table_name,'experiment_name'," SuperSeries is NULL")]
    
    for fileShort in files:
        print fileShort
        parsed_idf_result=prepare_idf(working_dir,fileShort)
        superseries=is_superseries_or_SRP(parsed_idf_result,is_superseries,"EXPERIMENTDESCRIPTION")
        update_superseries(int(superseries), fileShort,table_name)
    return True

def superseries_rnaseq():
    
    working_dir=os.getcwd()
    files=[i[0] for i in get_experiment_names('rnaseq_data','experiment_name'," SuperSeries is NULL")]
    
    for fileShort in files:
        print fileShort
        parsed_idf_result=prepare_idf(working_dir,fileShort)
        superseries=is_superseries_or_SRP(parsed_idf_result,is_superseries,"EXPERIMENTDESCRIPTION")
        update_superseries(int(superseries), fileShort)
    return True


def get_experiment_size():
    
    old_working_dir = os.getcwd()
    #files=[i[0] for i in get_experiment_names('rnaseq_analyse','exp_name'," size is NULL")]
    files=[i[0] for i in get_experiment_names('rnaseq_analyse','exp_name'," size is NULL")]
    server_addr='ftp.sra.ebi.ac.uk'
    ftp = ftplib.FTP(server_addr)
    ftp.login()
    for fileShort in files:
        print fileShort
        parsed_idf_result=prepare_idf(old_working_dir,fileShort)
        
        SRP=is_superseries_or_SRP(parsed_idf_result,find_SRP,"__SECONDARYACCESSION")
        
        target_file='http://www.ebi.ac.uk/ena/data/warehouse/filereport?accession=' + \
            SRP+"&result=read_run&fields=experiment_accession,scientific_name,fastq_ftp"    
        
        #target_file='TestRead.txt'
        #print target_file
        local_dict={}
        run_list=[]
        f=urllib2.urlopen(target_file)
        #f=open(target_file,'r')
        line=f.readline()
        line=line.strip('\n').split('\t') 
        org_index=line.index('scientific_name')
        run_index=line.index('experiment_accession')
        fastq_index=line.index('fastq_ftp')
        for line in f:
            line=line.strip('\n').split('\t') 
            print line 
            addr=line[fastq_index]
            split_add=addr.split(';')
            for addr in split_add:
                ind=addr.find('vol1/')
                addr=addr[ind:]
                print addr
                if len(addr.strip())>1:
                    try:
                        size=ftp.size(addr)
                    except:
                        local_dict[line[org_index]]=-10
                        break
                else:
                    size=0
                if line[org_index] not in local_dict: 
                    local_dict[line[org_index]]=size
                elif (line[org_index] not in run_list):
                    local_dict[line[org_index]]+=size
                   
                        
                else: pass
                run_list.append(line[run_index])
        for key,value in local_dict.iteritems():
            update_size(fileShort,key,value)
    ftp.quit()
    return True

def sort_rnaseq_experiments(type):
    
    old_working_dir = os.getcwd()
    new_working_dir = os.path.join(old_working_dir,'cache')
    
    if (type=='RNA-Seq'):
        prefix='.sdrf.txt'
        
    elif(type=='hybrid'):
        prefix='.seq.sdrf.txt'
    else:
        return False   
    
    query_string="select rnaseq_data.experiment_name from rnaseq_data, affyseq_experiments " +\
    "where rnaseq_data.experiment_name=affyseq_experiments.experiment_name and " + \
    " affyseq_experiments.sequencing_type='"+type+"'; "
    
    raw_query_results=query(query_string)
    files = [exp[0] for exp in raw_query_results]  
        
    #create_directory(new_working_dir)
    #files=os.listdir(new_working_dir
    for fileShort in files:
        assigned=False
        material,strategy,source=[],[],[]
        file=os.path.join(new_working_dir,fileShort+prefix)
        if (os.path.isfile(file)):
            print fileShort
            with open(file) as f:
                    line=f.next().strip('\n').replace('"',"").replace(" ","").split('\t')
                    if all([i in line for i in ['MaterialType','Comment[LIBRARY_STRATEGY]','Comment[LIBRARY_SOURCE]']]) :
                        index1=line.index('MaterialType')
                        index2=line.index('Comment[LIBRARY_STRATEGY]')
                        index3=line.index('Comment[LIBRARY_SOURCE]')
                    else:
                        update_fulltranscriptome(str(0), fileShort)
                        continue
                    for line in f:
                        line=line.strip('\n').replace('"',"").replace(" ","").split('\t')
                        if( line[index2].upper()=='RNA-SEQ' and line[index3].upper()=='TRANSCRIPTOMIC' and line[index1].upper()=='TOTALRNA'):
                            update_fulltranscriptome(str(1), fileShort)
                            assigned=True
                            break
                        else:
                            pass
                    if(assigned==False):
                        update_fulltranscriptome(str(0), fileShort)
                    else:
                        pass
    return True                
    
def sdrf_new_path(dir_type,new_working_dir,fileShort):
    path=os.path.join(new_working_dir,dir_type,fileShort)
    return path
    
def download_not_updated_experiments(not_updated_experiments):
    i = 0
    for not_updated_exp in not_updated_experiments:
        exp_type = not_updated_exp[1]
        exp_name = not_updated_exp[2]
        download_sdrf_file(exp_type, exp_name)
        download_idf_file(exp_type, exp_name)
        log = "Downloading SDRF and IDF files for local experiment: " + exp_name
        console_output_progress(i, len(not_updated_experiments), log)
        i += 1
    return True

def download_sdrf_file(exp_type, exp_name):
    """
    """
    old_working_dir = os.getcwd()
    new_working_dir = os.path.join(old_working_dir,'cache')
    os.chdir(new_working_dir)
    ADDRESS = "ftp://ftp.ebi.ac.uk/pub/databases/microarray/data/experiment/"
    path = ADDRESS + exp_type.upper() + "/" + exp_name.upper() + "/"
    command = "wget -N -nv -nd -r -A '*.sdrf.txt' '"+path+"'"
    os.system(command)
    os.chdir(old_working_dir)
    return True

def download_idf_file(exp_type, exp_name):
    """
    """
    old_working_dir = os.getcwd()
    new_working_dir = os.path.join(old_working_dir,'idf')
    os.chdir(new_working_dir)
    ADDRESS = "ftp://ftp.ebi.ac.uk/pub/databases/microarray/data/experiment/"
    path = ADDRESS + exp_type.upper() + "/" + exp_name.upper() + "/"
    command = "wget -N -nv -nd -r -A '*.idf.txt' '"+path+"'"
    os.system(command)
    os.chdir(old_working_dir)
    return True

def sync_not_updated_experiments(not_updated_experiments):
    i = 0
    for not_updated_exp in not_updated_experiments:
        exp_type = not_updated_exp[1]
        exp_name = not_updated_exp[2]
        loc_exp = get_local_experiment(exp_name)
        log = "Syncing local experiment: " + exp_name
        print(log)
        #console_output_progress(i, len(idf_dir_contents), log)
        if isinstance(loc_exp, dict):
            loc_exp_date = loc_exp[exp_name]
            insert_local_experiment_version(exp_name, loc_exp_date)
            set_experiment_updated(exp_type, exp_name)
        else:
            print("Error: " + exp_name)
        i += 1
    return "Completed syncing local experiments."

"""
### CHECK

"""
def get_not_updated_experiments():
    """
    """
    DB_TABLE = "affyseq_experiments"
    query_string =  "SELECT * FROM " + DB_TABLE + " " + \
                    "WHERE is_local_updated=0" + \
                    ";"
    return query(query_string)
def check():
    """
    """
    not_updated_experiments = get_not_updated_experiments()
    for not_updated_exp in not_updated_experiments:
        log = str(not_updated_exp[2])
        print("Needs updating: " + log)

    return "Completed checking local experiments."

"""
###

### CONSOLE

"""
def console_output_progress(progress_cur, progress_max, progress_string):
    # Prints the progress to the console
    progress = float(progress_cur/(progress_max*1.0)*100)
    progress_bar_coefficient = int(progress/5)
    progress_bar = "[" + progress_bar_coefficient*"#" + (20-progress_bar_coefficient)*" " + "]"
    print "Progress: " + progress_bar + " %.2f" %  progress + "% | " + progress_string, " \r",

"""
### MAIN

"""
print(run())