import re
from collections import defaultdict

def return_all_duplicates(parsed_sdrf):
    """
    Checks the parsed SDRF file column names for duplicates. 
    Argument is a two-dimensional list. 
    Returns a tuple of tuples with the duplicate element and indexes, e.g. 
    (('Characteristics [OrganismPart]', [9, 12]),).
    """ 
    column_names = parsed_sdrf[0]
    
    tally = defaultdict(list)
    for i,item in enumerate(column_names):
        tally[item].append(i)
    all_duplicates = ((key,locs) for key,locs in tally.items() 
                            if len(locs)>1)

    return all_duplicates

def return_duplicates(parsed_sdrf):
    """
    Parses the duplicates from return_all_duplicates() and returns them.
    """
    correct_duplicates = ()
    all_duplicates = return_all_duplicates(parsed_sdrf)
    for dup in all_duplicates:
        element = dup[0]
        element = element.replace(' ', '')
        element = element.replace('\n', '')
        element = element.lower()
        
        correct_duplicates = correct_duplicates + (dup,)
    return correct_duplicates

def merge_duplicates(duplicates, parsed_sdrf):
    """
    This functions merges the duplicates returned by return_duplicates(all_duplicates).
    Argument is a tuple of tuples.
    Returns a two-dimensional list (parsed SDRF file with duplicate columns merged).
    """
    merged_sdrf = []
    merged_row=merge_header(parsed_sdrf[0], duplicates)
    merged_sdrf.append(merged_row)
    for row_index in range(1, len(parsed_sdrf)):
        row = parsed_sdrf[row_index]
        merged_row = merge_row(row, duplicates)
        merged_sdrf.append(merged_row)

    return [merged_sdrf, len(duplicates)]

def merge_header(header_row, duplicates):
    """
    Merges the duplicate headers.
    """
    all_duplicate_indexes = get_all_duplicate_indexes(duplicates)

    merged_row = []
    for index in range(0, len(header_row)):
        parsedDuplicates = 0
        for duplicate in duplicates:
            duplicate_indexes = duplicate[1]
            if not index in all_duplicate_indexes and parsedDuplicates == 0:
                merged_row.append(header_row[index])
            elif index == duplicate_indexes[0]:
                merged_row.append(header_row[index])
            parsedDuplicates += 1

    return merged_row

def merge_row(row, duplicates):
    """
    Merges the duplicate rows.
    """
    merged_row = []
    all_duplicate_indexes = get_all_duplicate_indexes(duplicates)

    for index in range(0, len(row)):
        parsed_duplicates = 0
        for duplicate in duplicates:
            duplicate_indexes = duplicate[1]
            if not index in all_duplicate_indexes and parsed_duplicates == 0:
                merged_row.append(row[index])
            elif index == duplicate_indexes[0]:
                content_to_merge = []
                for duplicate_index in duplicate_indexes:
                    check_empty_string = re.match('^\s*$', row[duplicate_index])
                    check_not_specified = re.search('not specified', row[duplicate_index])
                    if (check_empty_string != None or
                        check_not_specified != None):
                        row[duplicate_index] = "NA"
                    else:
                        content_to_merge.append(row[duplicate_index])
                if len(content_to_merge) == 1:
                    merged_row.append(content_to_merge[0])
                else:
                    merged_row.append(row[duplicate_indexes[0]])

            parsed_duplicates += 1

    return merged_row

def get_all_duplicate_indexes(duplicates):
    """
    Returns the indexes for all of the duplicates.
    """
    all_duplicate_indexes = []
    for duplicate in duplicates:
        all_duplicate_indexes += duplicate[1]
    return all_duplicate_indexes