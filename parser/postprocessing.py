from basic import *


def rename_reorder(thread_info_dict, tab2nc_dir,exp):
    subproc=rename_command(thread_info_dict,tab2nc_dir)
    
    result=run_process(subproc,'Postprocessing scripts')
    if(not(result)):
        logging.error(' Postprocessing failed. Command used was %s. ', subproc)
        species=thread_info_dict['species'].replace(' ', '_').lower()
        thread_info_dict[species]='PPR'
    
    return True
    
    
def rename_command(thread_info_dict,tab2nc_dir):
    root_dir = thread_info_dict['root_dir']
    r_scriptpath=get_lib_file_path(root_dir, 'RenameNCVar.R')
    mat_dir=os.path.join(tab2nc_dir,'matrix.nc')
    subproc=['Rscript',r_scriptpath,mat_dir]
    return subproc

