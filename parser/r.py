# r.py
import shutil
from basic import *


def run_norm_r_for_all(thread_info_dict):
    """
    Executes the norm.R script for all platforms.
    """
    logging.debug(" %s Processing all data with norm.R script.",thread_info_dict['dataset'])
   
    affy_dir = get_experiment_type_dir(thread_info_dict)
    cel_files_dir = get_tool_dirs(affy_dir)
    logging.debug(" %s Directories in which cel files can be found %s.",thread_info_dict['dataset'],cel_files_dir)
    for cel_file_dir in cel_files_dir:
        
        run_norm_r(thread_info_dict, cel_file_dir)

    return True

def run_norm_r(thread_info_dict, cel_file_dir):
    """
    Executes the norm.R script in the specified directory.
    """
    root_dir = thread_info_dict['root_dir']
    (path_, dir_) = os.path.split(cel_file_dir)
    if not check_for_data(thread_info_dict, cel_file_dir):
        
        logging.error(" %s CEL data not found %s.",thread_info_dict['dataset'],dir)
        thread_info_dict[dir_]='CEL'
        return False

    cel_to_celfiles_command = get_cel_to_celfiles_command(cel_file_dir)
    norm_r_command = get_norm_r_command(cel_file_dir)
    logging.info(" %s Setting up R.",thread_info_dict['dataset'])
    setup_norm_r(root_dir, cel_file_dir)


    logging.info(" %s Merging all .CEL files into celfiles.",thread_info_dict['dataset'])
    os.system(cel_to_celfiles_command)

   
    logging.info(" %s Processing %s with norm.R script",thread_info_dict['dataset'],cel_file_dir)
    error_dict={10:'MEM',11:'CDF',-11:'TCEL',12:'NAN'}
    logging.debug(" %s R command to be run %s.",thread_info_dict['dataset'],norm_r_command)
    result=run_process(norm_r_command,'R')
    logging.debug(" %s Result of run R process %s.",thread_info_dict['dataset'],result)
    thread_info_dict=evaluate_error(result,error_dict,thread_info_dict,dir_)
   
    return True



def evaluate_error(result,error_dict,thread_info_dict,dir_):
    if result in error_dict:
        error_dict[result]
        thread_info_dict[dir_]=error_dict[result]
    return thread_info_dict

def run_process(subproc, software):
    try:
       
        logging.debug('Running %s... ', software)
        subprocess.check_call(subproc,stdout=None,stderr=None)
            
    except subprocess.CalledProcessError as e:
        logging.error(" subprocess failed, running %s failed %s " ,software,e)

        return e.returncode
    #    
    return True




def check_for_data(thread_info_dict, cel_file_dir):
    """
    Returns a boolean value that represents whether any .CEL files exist or not.
    """
    cel_file_dir_contents = os.listdir(cel_file_dir)
    for content in cel_file_dir_contents:
        if '.cel' in content.lower():
            return True

    logging.info(" %s Data not found %s.",thread_info_dict['dataset'],cel_file_dir)

    return False

def get_norm_r_command(cel_file_dir):
    """
    Returns a string for the norm.R script to execute.
    """
    NORM_R = ["/group/software/public/irap_0.5.0/R3/bin/Rscript",cel_file_dir+"/norm.R",cel_file_dir+"/celfiles",cel_file_dir]
    return NORM_R

def get_cel_to_celfiles_command(cel_file_dir):
    """
    Returns a string for the system to execute (merge .CEL files).
    """
    CEL_TO_CELFILES = 'ls ' + cel_file_dir + '/*.[Cc][eE][lL] > ' + \
                        cel_file_dir + '/celfiles'
    return CEL_TO_CELFILES

def setup_norm_r(root_dir, target_dir):
    """
    Copies the norm.R file from the root directory to the specified directory.
    """ 
    norm_r_path = get_norm_r_path(root_dir)
    new_norm_r_path = get_new_norm_r_path(target_dir)
    shutil.copyfile(norm_r_path, new_norm_r_path)

    return True

def get_norm_r_path(root_dir):
    """
    Returns a full path to the norm.R file in the root/lib directory.
    """
    NORM_R_NAME = "norm.R"    
    norm_r_path = os.path.join(root_dir, 'lib', NORM_R_NAME)

    return norm_r_path

def get_new_norm_r_path(target_dir):
    """
    Returns a full path to the norm.R file for the specified directory.
    """
    NORM_R_NAME = "norm.R"
    new_norm_r_path = os.path.join(target_dir, NORM_R_NAME)

    return new_norm_r_path