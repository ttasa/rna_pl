# clean.py

import os
import shutil
from basic import get_thread_dir_path
import logging

def clean(root_dir):
    clean_root(root_dir)
    clean_parser(root_dir)
    return True

def clean_root(root_dir):
    op_sys = os.name
    cur_dir_contents = os.listdir(root_dir)
    
    for content in cur_dir_contents:
        if op_sys == "posix":
            content = content.split('/')
        elif op_sys == "nt":
            content = content.split('\\')
        
        file_name = content[-1]
        split_file_name = file_name.split('.')

        if (len(split_file_name) > 1 and 
            split_file_name[1] == 'pyc'):
            os.remove(file_name)

    return True

def clean_parser(root_dir):
    
    parser_dir = os.path.join(root_dir, 'parser')
    parser_dir_contents = os.listdir(parser_dir)
    
    for content in parser_dir_contents:
        logging.debug('%s Removing content ', content)       
        split_file_name = content.split('.')
        if (len(split_file_name) > 1 and 
            split_file_name[1] == 'pyc'):
           
            dir_to_del = os.path.join(parser_dir,content)
            os.remove(dir_to_del)

    return True

def clean_wrapper(kwargs):
    
    thread_dir = get_thread_dir_path(kwargs)
    delete_path=os.path.join(thread_dir,kwargs['domain'])
    logging.debug( 'Starting data cleaning from %s',delete_path)
    print kwargs,'kwargs'
    if('raw_data' in kwargs):
        raw_data=kwargs['raw_data']        
        if ( 'parent_dir' in kwargs):
            raw_data=os.path.dirname(raw_data)

        remove_data(raw_data,delete_path,kwargs)
        
    else:
        
        clean_data(delete_path)
        logging.debug( 'Directory tree from  %s is deleted',delete_path)
    
    return True


def rmgeneric(path, __func__):
    
    ERROR_STR= """Error removing %(path)s, %(error)s """    
    try:
        __func__(path)
        logging.debug( 'Removed %s', path)
    except OSError, (errno, strerror):
        logging.error( ERROR_STR % {'path' : path, 'error': strerror })
            

def remove_data(raw_data,delete_path,kwargs):
    
    if not os.path.isdir(delete_path):
        return
    
    if os.path.abspath(delete_path) == os.path.abspath(raw_data):
        
        logging.debug( 'Keep %s',delete_path)
        return 
    
    elif os.path.abspath(delete_path) in os.path.abspath(raw_data):
        
        files=os.listdir(delete_path)
        
        for x in files:
                
            if os.path.isfile(os.path.join(delete_path,x)):
                f=os.remove
                rmgeneric(os.path.join(delete_path,x), f)
            elif os.path.isdir(os.path.join(delete_path,x)):
                remove_data(raw_data,os.path.join(delete_path,x),kwargs)
            
    else:
        clean_data(delete_path)
            
    return True 
        


def clean_data(delete_path):

    if os.path.exists(delete_path):
        logging.debug('Delete all %s',delete_path)
        
        return shutil.rmtree(delete_path)
    else:
        return True



def clean_all_data(root_dir):
    data_dir = os.path.join(root_dir, 'data')
    if os.path.exists(data_dir):
        logging.debug('%s Found_old_data ', data_dir) 
        data_dir_contents = os.listdir(data_dir)
    else:
        return True
    for content in data_dir_contents:
        content_dir = os.path.join(data_dir, content)
        logging.debug('%s Removing content ', content_dir) 
        shutil.rmtree(content_dir)
    return True