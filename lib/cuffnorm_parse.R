#setwd('/home/ttasa/rna_pl/RNAproject/lib/cuffnorm')
args <- commandArgs(trailingOnly = TRUE)
#samples.table ==args[1]

assembly=args[5]

samples.table=args[1]
options(scipen=15)
samples.table=read.table(samples.table,header=T)
files=as.character(samples.table$file)
extract=function(x){
  tmp=unlist(strsplit(x,'/'))
  result=(tmp[length(tmp)-1])
  result
}
res=unname(sapply(files, extract))
##res consists of sample names
##args[2]
gene.attr=args[2]
genes.attr=read.table(gene.attr,header=T,sep='\t')
if(assembly){
genes.attr$gene_short_name=as.character(genes.attr$gene_short_name)
gene_names=genes.attr$gene_short_name
cufflinks_metadata=subset(genes.attr,select=c('tracking_id','tss_id','locus'))
write.table('m * {string Locus_id[]; string TSS_ID[]; string GenomicInterval[] }',file.path(args[4],'cufflinks.tnc'),row.names=F,col.names=F,quote=F)
}else{
  gene_names=subset(genes.attr,select=c('tracking_id'))
  cufflinks_metadata=subset(genes.attr,select=c('locus'))
  write.table('m * {string GenomicInterval[] }',file.path(args[4],'cufflinks.tnc'),row.names=F,col.names=F,quote=F)
  
}

#Total consists of splitted gene data
##args[3] genes.fpkm_table
gene.fpkm=args[3]
data_mat=read.table(gene.fpkm,header=T,sep='\t')
#val=new[,c('gene_short_name',names(data_mat)[-1])]
val=cbind(gene_names,data_mat[,-1])
##args[4] output-file
write.table(t(res),file=file.path(args[4],'matrix'),col.names=F,row.names=F,quote=F,sep='\t')
write.table(val,file=file.path(args[4],'matrix'),sep='\t',col.names=F,quote=F,row.names=F,append=T)
write.table(cufflinks_metadata,file=file.path(args[4],'cufflinks'),sep='\t',quote=F,row.names=F,col.names=F)