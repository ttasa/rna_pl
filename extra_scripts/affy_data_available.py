import sys
sys.path.insert(0,'/group/software/public/affyseq_dev/parser')

from basic import get_files
import os
import MySQLdb as mdb

def query(query_string):
    con = connect()
    cur = con.cursor()
    cur.execute(query_string)
    rows = cur.fetchall()
    con.close()
    return rows

def get_experiment_names(selected_column_name ='experiment_name',column_name='',condition='',column_name2='',condition2='',domain='rnaseq'):
  
    if(column_name==''):
        query_string ="select "+ selected_column_name+" from "+domain+"_data;"    
    elif(column_name2==''):
        query_string="select "+ selected_column_name+" from "+domain+"_data where "+column_name+ " = '"+ condition + "' ;" 
    elif(column_name2!=''):
        query_string="select "+ selected_column_name+" from "+domain+"_data where "+column_name+ " = '"+ condition + "' and "+column_name2 + " = '"+ condition2+"' ;" 
    print query_string
    return query(query_string)

def connect():
    """
    Returns the connection to the database. Must be closed after use with close().
    """
    try:
        con = mdb.connect(
                            'localhost',    # address
                            'affy',         # user
                            'parool!',      # password
                            'affyseq_db')   # database
        return con

    except mdb.Error, e:
        return "Error %d: %s" % (e.args[0], e.args[1])

def update_data_availablity(DB_TABLE,is_data_available, exp_name):
    """
    """
    FALSE = 0
    query_string =  "UPDATE " + DB_TABLE + " " + \
                    "SET data_available=" + "'" + str(is_data_available) + "' "  + \
                    "WHERE experiment_name=" + "'" + exp_name + "'" + " " + ";"
    return query(query_string)

def check_data_availability_affy():
    old_working_dir = os.getcwd()
    new_working_dir = os.path.join(old_working_dir,'cache')
    files=[i[0] for i in get_experiment_names('affy','experiment_name')] 
    files=get_files('/group/software/public/affyseq_dev/AffyDataAvalTest','.sdrf.txt',None)
    #files=os.listdir(new_working_dir)
    for file in files:
        assigned=False
        if (os.path.isfile(file)):
            with open(file) as f:
                line=f.next().strip('\n').replace('"',"").replace(" ","").split('\t')
                if all([i in line for i in ['ArrayDataFile','Comment[ArrayExpressFTPfile]','ArrayDesignREF']]):
                    index1=line.index('ArrayDataFile')
                    for line in f:
                        line=line.strip('\n').replace('"',"").replace(" ","").split('\t')
                        if( line[index1].upper().endswith('.CEL')):
                        
                            update_data_availablity('affy_data',str(1), file)
                            assigned=True
                            break
                        else:
                            pass
                    if(assigned==False):
                        update_data_availablity('affy_data',str(0), file)
                    else:
                        pass
                else:
                    update_data_availablity('affy_data',str(0), file)
    return True

def main():
    check_data_availability_affy()
    
if __name__ == "__main__":
    main()