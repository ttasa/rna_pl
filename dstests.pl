#!/usr/bin/perl

# Validate NetCDF datasets in provided directories
# and their subdirectories for ExpressView format conformance.

use strict;
use warnings;

use Fcntl;
use PDL;
use PDL::Char;
use PDL::NetCDF;

# Dataset validation

sub check_dataset {

	my ($nc) = @_;
	
	# Check variables
	
	my $vars = $nc->getvariablenames();
	my $m;
	my $n;

	# Validation matrix of required variables:
	# variable name, type, dimensionality, required dimension names.
	
	my $vmatrix = [
		['gene', 					'char', 	2, ['m']],
		['array', 					'char', 	2, ['n']],
		['data', 					'double', 	2, ['m', 'n']],
		['Organism', 				'char', 	2, ['n']],
		['MetadataOrder', 			'char', 	2, ['n']],
		['ExperimentDescription', 	'char', 	1, []],
		['InvestigationTitle', 		'char', 	1, []],
		['__DatasetType',	 		'char', 	1, []],
		['__FileFormat',	 		'char', 	1, []],
	];
	
	foreach(@$vmatrix) {		
		my $rqname     = $_->[0];
		my $rqtype     = $_->[1];
		my $rqdim      = $_->[2];
		my $rqdimnames = $_->[3];
		
		my $type;
		my $dims;
		
		# Existence check
		
		die("Variable '$rqname' missing in dataset\n")
			unless (array_in_bool($vars, $rqname));
		
		# Type check
			
		$type = $nc->getvariabletype($rqname)->realctype();
		$type =~ s/unsigned //;
			
		die("Variable '$rqname' has type $type, but requires $rqtype\n")
			unless($type eq $rqtype);
		
		# Dimensions check
		
		$dims = $nc->getdimensionnames($rqname);
		
		die("Variable '$rqname' has " . scalar @$dims . " dimensions, but $rqdim required\n")
			unless(scalar @$dims == $rqdim);		
		foreach (0 .. @$rqdimnames - 1) {
			die("Variable '$rqname' requires dimension '" . $rqdimnames->[$_] . "' on position " . ($_+1) . "\n")
				unless($rqdimnames->[$_] eq $dims->[$_]);
		}
		
		array_remove($vars, $rqname);		
	}
	
	# Of the remaining metadata fields, all must be
	# of type string[m|n] or double[m|n];
	
	foreach (@$vars) {		
		next if (/^__/); # ignore extra metadata fields
		next if (/^(DatasetLink|DatasetID)$/);
		
		my $dims = $nc->getdimensionnames($_);
		my $type = $nc->getvariabletype($_)->realctype();
		
		die("Variable '$_' must be a char[] or a double\n")
			unless($type =~ /char$/ || $type =~ /double$/);
		die("Variable '$_' requires dimension 'm' or 'n' on position 1\n")
			unless($dims->[0] eq "m" || $dims->[0] eq "n");
		die("Variable '$_' has " . scalar @$dims . " dimensions, but 2 required\n")
			if($type =~ /char$/ && !(scalar @$dims == 2));
	}
	
	# MetadataOrder and array must be equal sets

	die("'MetadataOrder' and 'array' must be equal since EVDF 1.1\n")
		unless array_equals(
			array_from_pdl($nc->get("MetadataOrder")),
			array_from_pdl($nc->get("array"))
		);
}

sub check_directory {

	my ($dirname) = @_;
	
	my @dss;
	my $ref_genes;
	my $organism;
	my $curr_genes;
	my $per_file_indexes;
	
	chdir $dirname;
	
	@dss = glob("*.nc");		
	$per_file_indexes = (glob("*.index") ? 1 : 0);
	
	eval {	
		foreach (@dss) {
			
			# Check individual validity of dataset
		
			my $nc = PDL::NetCDF->new($_, {
				MODE => 0 | O_RDONLY,
				SLOW_CHAR_FETCH => 1,
			});
			print STDERR "Checking $dirname/$_ ... ";
			check_dataset($nc);
			
			# Check if genes match in all datasets, unless
			# per-file indexes are used

			if (!$per_file_indexes) {
				unless (defined $ref_genes) {
					$ref_genes = array_from_pdl($nc->get("gene"));				
				}
				else {
					$curr_genes = array_from_pdl($nc->get("gene"));
					die("Gene vectors must be equal over all datasets in a platform\n")
						unless(array_equals($curr_genes, $ref_genes));
				}
			}

			print STDERR "OK\n";
		}
	};
	if ($@) {
		chomp $@;
		die("$@!\n");
	}
}

sub main {
	print "Usage: dstests.pl datset_directory1 [dataset_directory2 ...]\n"
		if(!@ARGV || $ARGV[0] eq "--help");

	foreach (@ARGV) {	
		unless (-d $_) {
			next;
		}		
		unless (-r $_ && -x $_) {
			print STDERR "$_ is not a valid directory! Check directory name and permissions.\n";			
			next;
		}
		
		check_directory($_);
	}
}

main();

# Aux functions from ExpressView::Util

sub array_equals {
	
	# Checks whether two arrays are of the same length
	# and have [eq]ual values on corresponding positions.
	
	my ($a, $b) = @_;
	
	return 0 if (@$a != @$b);	
	foreach(0 .. @$a - 1) {
		return 0 unless ($a->[$_] eq $b->[$_]); }
	1;
}

sub set_equals {

	# Checks whether two sets are equal (multisets allowed).
	
	my ($a, $b) = @_;
	
	return 0 if (@$a != @$b);
	
	my @sa = sort(@$a);
	my @sb = sort(@$b);
	
	array_equals(\@sa, \@sb);
}

sub array_in {

	# Check if a given value exists in array,
	# return its position. Otherwise undef.
	
	my ($a, $val) = (@_);
	
	foreach (0 .. @$a - 1) { return $_ if (@$a[$_] eq $val); }
	undef;
}

sub array_in_bool {

	# Check if a given value exists in array,
	# return true or false.
	
	defined array_in(@_) ? 1 : 0;
}

sub array_remove {
	
	# Remove matched element(s) from array and return
	#
	# $a - array reference
	# $v - element value
	
	my ($a, $v) = @_;
	my $alen = @$a;
	
	for (my $i=0; $i<$alen; $i++) {
		if ($v eq $a->[$i]) {
			splice(@$a, $i, 1);
			$i++;
			$alen--;
		}
	} @$a;	
}

sub array_from_pdl {

	# Convert a PDL object to a Perl array
	# Must be a 1-dimensional vector of strings,
	# i.e. 2-dimensional matrix of chars for PDL::Char,
	# a standard vector for PDL

	my $pdl = shift;
	my @dims;
	my @out;
	my $n;
	
	if (ref $pdl eq "PDL") {
		die("array_from_pdl() expects a vector")
			unless ((@dims = $pdl->dims($pdl)) == 1);
			
		$n = $dims[0];
		foreach (0 .. $n - 1) {
			push @out, $pdl->at($_);
		}			
	}
	elsif (ref $pdl eq "PDL::Char") {		
		diesub("array_from_pdl() expects a vector of strings for PDL::Char")
			unless ((@dims = $pdl->dims($pdl)) == 2);
			
		$n = $dims[1];
		foreach (0 .. $n - 1) {
			push @out, $pdl->atstr($_);
		}
	}
	else {
		die("array_from_pdl() needs a PDL or PDL::Char object");
	}
	
	\@out;
}

sub array_to_pdl {

	# Convert a Perl array of strings to
	# a PDL::Char object

	PDL::Char->new(shift);
}

1;


