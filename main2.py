# main.py
import os
import sys
import copy
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), "parser"))
import affyseq
import config
import Queue
import basic
import logging
import MySQLdb as mdb

### Adds the affyseq_parser/parser directory to the environment path.

"""
### DATABASE
"""

def get_usage():
    """
    """
    USAGE = 'Default mapper: STAR\n' + \
    'Default quantifier: htseq\n' + \
    '\n=== AFFYSEQ ===\n' + \
            'Usage:\n' + \
        'python main2.py affy.conf' + \
        'python main2.py' 
        

    return USAGE


def get_thread(kwargs):
    
    if 'thread' in kwargs:
    
        thread=kwargs['thread']
    else:
        for i in create_thread_generator(kwargs):
            pass
        
def create_thread_generator(kwargs):
    
    range_values=range(1000)
    for process_val in range_values:
        
        kwargs['thread']=process_val
        thread_dir=basic.get_thread_dir_path(kwargs)
        addr=os.path.join(thread_dir,kwargs['domain'])
        if((os.path.exists(addr)) and len(os.listdir(addr))>0 ):
            yield True
        else:
            break 

def add_rootdir(kwargs):
    
    root_dir = os.path.dirname(os.path.abspath(__file__))
    kwargs['root_dir']=root_dir
    return True

def run():
    
    thread_info_dict=config.get_config()
    
    path=config.config_check(thread_info_dict)
    
    kwargs=thread_info_dict['General']
   
    add_rootdir(kwargs)
    #clean.clean_all_data(root_dir)
    if (not(os.path.isdir("./log"))):
        os.mkdir('log')
    FINISHED = "Finished running!"
    
    domain=kwargs['domain']
   
    get_thread(kwargs)
   
    if (path=='public_query'):
        
        if(domain=='rnaseq'):
            species=kwargs['species']
        
        logging.basicConfig(filename='log/'+domain+'.log',format='%(asctime)s %(levelname)s: %(message)s', datefmt='%d/%m/%Y:%I:%M:%S %Z',level=logging.DEBUG)
        logging.debug('Domain: %s', domain)
        execute_all_public(thread_info_dict)
        #clean.clean_parser(root_dir)

        
    elif (path=='test'):
        queue=Queue.Queue()
        logging.basicConfig(filename='log/test'+domain+'.log',format='%(asctime)s %(levelname)s: %(message)s', datefmt='%d/%m/%Y:%I:%M:%S %Z',level=logging.DEBUG)
        if (domain=="rnaseq"):
            kwargs['dataset']='E-GEOD-48829'
            queue.put('E-GEOD-48829')
            
            kwargs['species']='Escherichia coli str. K-12 substr. MG1655'
        elif (domain=='affy'):
            kwargs['dataset']='E-GEOD-2218'
            queue.put('E-GEOD-2218')
        else:
            return get_usage()
            
        execute(thread_info_dict,kwargs['thread'],queue)
        

    elif ('owndata' in path):
        
        queue=Queue.Queue()
        
        if ('name' in kwargs):
            log_name=kwargs['name']
        else:
            log_name=os.path.basename(kwargs['raw_data'])
        
        if 'dataset' not in kwargs:
            kwargs['dataset']=log_name
        
        logging.basicConfig(filename='log/'+log_name+'.log',format='%(asctime)s %(levelname)s: %(message)s', datefmt='%d/%m/%Y:%I:%M:%S %Z',level=logging.DEBUG)
        
        if (path=='single_owndata'):
        
            queue.put(kwargs['raw_data'])
            execute(thread_info_dict,kwargs['thread'],queue)
        
    
        elif(path=='multiple_owndata'):
            
            dirs=[os.path.join(kwargs['raw_data'],dir_) for dir_ in os.listdir(kwargs['raw_data']) if os.path.isdir(os.path.join(kwargs['raw_data'],dir_))]
            dataset=kwargs['dataset']
            kwargs['parent_dir']=kwargs['raw_data']
            for nr,dir_ in enumerate(dirs):
                kwargs['raw_data']=dir_
                kwargs['dataset']=dataset+'_'+str(nr)
                if (basic.get_species(kwargs) in kwargs):
                    del kwargs[basic.get_species(kwargs)]
                queue.put(dir_)
                logging.debug(' %s Experiment to be handled: ',file) 
                execute(thread_info_dict,kwargs['thread'],queue)
    
    elif (path=='one_exp'):
        dataset=kwargs['dataset']
        queue=Queue.Queue()
        queue.put(dataset)
        logging.basicConfig(filename=os.path.abspath('log/'+dataset+'.log'),format='%(asctime)s %(levelname)s: %(message)s', datefmt='%d/%m/%Y:%I:%M:%S %Z',level=logging.DEBUG)
        logging.debug('%s Domain: %s', dataset, domain)
        execute(thread_info_dict,kwargs['thread'],queue)
    
    else:
        
        return get_usage()
    
    return FINISHED


def execute_all_public(thread_info_dict):
    kwargs=thread_info_dict['General']
    #files = get_idf_files_list()    
    domain=kwargs['domain']
    if(domain=="rnaseq"):
        ##Query_string=thread_info_dict
        #files=[i[0]+'.idf' for i in basic.get_experiment_names(domain+'_analyse', 'exp_name'," species='"+kwargs['species']+"' and size>'0' and samples>'4' order by size LIMIT 12")]
        files=[i[0] for i in basic.get_experiment_names(domain+'_analyse', 'exp_name'," species='"+kwargs['species']+"' and "+kwargs['query_string'])]

        logging.info('%s selected files',files)
        queue=Queue.Queue()
        for file_ in files:
            
            queue.put(file_)
            kwargs['dataset']=file_
            
            if (basic.get_species(kwargs) in kwargs):
                del kwargs[basic.get_species(kwargs)]
                
            logging.debug(' %s Experiment to be handled: ',file_) 
            execute(thread_info_dict,kwargs['thread'],queue)
        return True
    
    elif(domain=="affy"):
        kwargs['affy_long']=True
        #files=[i[0]+'.idf' for i in basic.get_experiment_names(domain+'_data', 'experiment_name'," data_available in('1','2') and SuperSeries!='1' and processed='0'")]
        files=[i[0]+'.idf' for i in basic.get_experiment_names(domain+'_data', 'experiment_name',kwargs['query_string'])]
        basic.Queue_handler(8,files,basic.thread_create,execute,thread_info_dict)  
        return True     
    
    else:
        logging.error(" Domain is misspecified")
        return False


def execute(thread_info_dict,thr,queue):
    
    kwargs=thread_info_dict['General']
    file_=queue.get()
    
    if(not 'raw_data' in kwargs):
        split_file = file_.split('.')
        dataset = split_file[0]
        
    else:
        dataset=file_
        dataset=os.path.basename(dataset)
        
    if 'affy_long' in kwargs:
        thr_temp=copy.deepcopy(thread_info_dict)
        thr_temp['General']['thread']=thr
        thr_temp['General']['dataset']=dataset
        del thread_info_dict
        thread_info_dict=thr_temp
        
    logging.info('Analysis of %s starting.',dataset)
    logging.debug('Processing %s on thread %s', file_ , str(kwargs['thread']) )
    logging.info(thread_info_dict)
    
    affyseq.run(thread_info_dict)    
    queue.task_done()
    return True


def get_idf_files_list():
    idf_files_list = os.listdir('idf')
    return idf_files_list



print(run())
