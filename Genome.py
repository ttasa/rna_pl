import MySQLdb as mdb
import os
import time
import subprocess
import sys
import getopt
from ftplib import FTP, error_perm
from datetime import datetime, date

'''
DATABASE: affyseq_db
USER: affy
PASSWORD: parool!

TABLE: affyseq_reference_data

+----------------------+-------------+------+-----+---------+----------------+
| Field                | Type        | Null | Key | Default | Extra          |
+----------------------+-------------+------+-----+---------+----------------+
| id                   | int(11)     | NO   | PRI | NULL    | auto_increment |
| organism             | varchar(30) | NO   | UNI | NULL    |                |
| local_version_genome | date        | YES  |     | NULL    |                |
| local_version_gtf    | date        | YES  |     | NULL    |                |
+----------------------+-------------+------+-----+---------+----------------
'''


USAGE = 'Usage:\n' + \
            'python Genome.py -g            : Updates of latest genomes for all species \n' + \
            'python Genome.py -t       : -,,- latest transcriptomes \n' + \
            'python Genome.py -b  : -,,- latest transcriptomes and genomes\n' + \
            'python Genome.py -[gtb] listOfOrganisms\n '+ \
            'eg. python Genome.py -g homo_sapiens mus_musculus  : -,,- Transcriptome and/or genome for specific organisms (1-*). \n '


def monthToNum(date):
    return{
            'Jan' : '01',
            'Feb' : '02',
            'Mar' : '03',
            'Apr' : '04',
            'May' : '05',
            'Jun' : '06',
            'Jul' : '07',
            'Aug' : '08',
            'Sep' : '09', 
            'Oct' : '10',
            'Nov' : '11',
            'Dec' : '12'
    }[date]
    
def get_date_from_string(date_string):
    """
    """
    date = datetime.strptime(date_string,'%Y-%m-%d').date()
    return date

    
def parse_ftp_contents(ftp_contents):
    """
    """
    parsed_contents = {}
    if ftp_contents != False:
        for content in ftp_contents:
            split_content = content.split()
            experiment_list = split_content[5:]
            month = monthToNum(experiment_list[0])
            day = experiment_list[1]
            year = experiment_list[2]
            type_ = experiment_list[3]
            if ":" in year:
                year = date.today().year
            parsed_contents[type_] = str(year) + '-' + str(month) + '-' + str(day)

    return parsed_contents

def get_ftp_contents(Object,working_dir):
    """
    """
    FTP_ADDRESS = Object.FTP_Address()
    ftp_contents = []
    ftp = FTP(FTP_ADDRESS)
    try:
        ftp.login()
        ftp.cwd(working_dir)
        ftp.retrlines('LIST', callback=ftp_contents.append)
        ftp.quit()
    except error_perm, e:
        print("Error with " + working_dir + ": " + str(e))
        return False

    return ftp_contents

def get_experiment_type_list(Object):
    """
    """
    working_dir=Object.FTP_Directory()
    ftp_contents = get_ftp_contents(Object,working_dir)
    experiment_type_list = parse_ftp_contents(ftp_contents)   
     
    return experiment_type_list

def get_experiment_list(Object,experiment_type):
    """
    """
    working_dir=Object.FTP_inner_directory(experiment_type)
    ftp_contents = get_ftp_contents(Object,working_dir)
    experiment_list = parse_ftp_contents(ftp_contents)
    
    return experiment_list


def query(query_string):
    con = connect()
    cur = con.cursor()
    cur.execute(query_string)
    rows = cur.fetchall()
    con.close()
    return rows

def connect():
    """
    Returns the connection to the database. Must be closed after use with close().
    """
    try:
        con = mdb.connect(
                            'localhost',    # address
                            'affy',         # user
                            'parool!',      # password
                            'affyseq_db')   # database
        return con

    except mdb.Error, e:
        return "Error %d: %s" % (e.args[0], e.args[1])

def get_experiment(organism):
    """
    """
    DB_TABLE = "affyseq_reference_data"
    query_string =  "SELECT * FROM " + DB_TABLE + " " + \
                    "WHERE organism=" + "'" + organism + "'" + \
                    ";"
    return query(query_string)

def insert_server_experiment(Object,exp_type, exp_date):
    """
    """
    DB_TABLE = "affyseq_reference_data"
    query_string =  "INSERT INTO " + DB_TABLE + " " + \
                    "(organism, local_version_"+ Object.file_type +") VALUE " + \
                    "('" + \
                        exp_type + "','" + \
                        str(exp_date) + \
                    "')" + \
                    ";"
    return query(query_string)

def get_local_time(Object,exp_type):
    """
    """
    DB_TABLE = "affyseq_reference_data"
    query_string =  "SELECT local_version_"+Object.file_type+" FROM " + DB_TABLE + " " + \
                    "WHERE organism=" + "'" + exp_type + "'" + " " + \
                    ";"
    return query(query_string)

def create_directory(dir_path):
    """
    Creates a new directory with the given path.
    """
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    return True


def update_server_experiment(Object,exp_type, exp_date):
    """
    """
    DB_TABLE = "affyseq_reference_data"
    
    query_string =  "UPDATE " + DB_TABLE + " " + \
            "SET local_version_"+Object.file_type +"=" + "'" + str(exp_date) + "'" +  \
            "WHERE organism=" + "'" + exp_type + "'" + ";"
            
    return query(query_string)

def query(query_string):
    con = connect()
    cur = con.cursor()
    cur.execute(query_string)
    rows = cur.fetchall()
    con.close()
    return rows


def generate_fullpath(Object,genome_files,exp_type):
    """
    """
    cur_dir = os.getcwd()
    gen_dir_name='lib'

    value=Object.general_type
    gen_fold=os.path.join(cur_dir,gen_dir_name,value,exp_type)
    create_directory(gen_fold)
    gen_file_name="".join(genome_files.keys())
    gen_fullpath=os.path.join(gen_fold,gen_file_name)
    return gen_fullpath

def download_missing(Object,genome_files,gen_fullpath,exp_type):
    value=Object.general_type
    exp_ser_version="".join(genome_files.values())
    if(not(os.path.exists(gen_fullpath) or os.path.exists(gen_fullpath.replace(".gz","")))):
        if(ftp_genome_download(Object,gen_fullpath,genome_files,exp_type)):
            update_server_experiment(Object,exp_type, exp_ser_version)
            print('Downloaded database %s of %s' %( value ,exp_type))
        else:
            return(False)
    return True
        

def get_local_experiment(Object,exp_type):        
    '''
    '''
    try:
        loc_time=get_local_time(Object,exp_type)
        return loc_time[0][0]
    except OSError, e:
        ERROR_MESSAGE = "Something wrong with "+ exp_type +" - "\
                        "Database dates are wrong."
        print(ERROR_MESSAGE)


def ftp_genome_download(Object,gen_fullpath,genome_files,exp_type):
    """
    """
    FTP_ADDRESS = Object.FTP_Address()
    ftp = FTP(FTP_ADDRESS)
    filename="".join(genome_files.keys())
    try:
        ftp.login()
        try:
            path=Object.FTP_inner_directory(exp_type)
            ftp.cwd(path)
            filex=open(gen_fullpath, 'wb')
            print('Downloading %s %s' % (exp_type , Object.general_type))
            ftp.retrbinary("RETR %s" % filename, filex.write)
        except:
            print 'Download of %s %s failed' % (exp_type , Object.general_type)
            filex.close()    
            ftp.quit()
            return(False)
    except:
        print ('Login of %s %s failed)' % (exp_type , Object.general_type))
        return(False)
    filex.close()
    ftp.quit()
    return True



def process_server_experiment(Object,exp_type, genome_files):
    gen_fullpath=generate_fullpath(Object,genome_files,exp_type)
    
    try:
        download_missing(Object,genome_files,gen_fullpath,exp_type)
    except:
        print 'Error'
    exp_loc_version =  get_local_experiment(Object,exp_type)
    exp_ser_version="".join(genome_files.values())
    exp_ser_version=get_date_from_string(exp_ser_version)
    
    
    
    if exp_ser_version > exp_loc_version:
        print ("Update server experiment "+exp_type)
        dir_name=os.path.dirname(gen_fullpath)
        delete_folder_contents(dir_name)
        if(ftp_genome_download(Object,gen_fullpath,genome_files,exp_type)):
            update_server_experiment(Object,exp_type, exp_ser_version)
            print('Renewed database %s of %s' % (Object.general_type , exp_type))
        return "Updating: "
    elif gen_fullpath==False:
        return "Something went wrong: "
    else:
        return "Up to date %s : " % Object.general_type

def delete_folder_contents(dir_name):
    for file in os.listdir(dir_name):
        file_path=os.path.join(dir_name,file)
        os.unlink(file_path)
    if(len(os.listdir(dir_name))==0):
        return True
    else:
        print 'Some files are still on the folder'
        return False

def process_experiment_name(Object,genome_files,exp_type):
    exp = get_experiment(exp_type)
    exp_ser_version = ''.join(genome_files.values())
    if len(exp_ser_version)==0:
        print ("Could not access server files for: %s" %exp_type)
        return False
    elif len(exp) > 0:
        process_ser_exp_result = process_server_experiment(
                                                    Object,
                                                    exp_type,
                                                    genome_files
                                                    )
        return (process_ser_exp_result)
    else:
        insert_server_experiment(Object,exp_type, exp_ser_version)
        print ("Inserting: ")
        return True
    print "Processing experiment name failed: "
    return True 


def genome(Object,index=False):
    """
    """
    exp_type_list = get_experiment_type_list(Object)
    exp_type_list = filter_experiments(Object,exp_type_list)
    for exp_type in exp_type_list:
        print exp_type
        if(exp_type in ['triticum_aestivum','tursiops_truncatus']):
            continue
        exp_list = get_experiment_list(Object,exp_type)        
        genome_files=Object.filter_file(exp_list)
        exp_log = str(exp_type)
        process_exp_name_result = process_experiment_name(Object,genome_files,exp_type)
        if(index and process_exp_name_result):
            gen_fullpath=generate_fullpath(Object,genome_files,exp_type)
            path=os.path.dirname(gen_fullpath)
            index_names=generate_indexnames(gen_fullpath)
            if( check_elements(index_names,exp_type) and check_indexdb(Object,path,exp_type)):
                print 'Index up to date'
            else:
                clean_index_folder(path)
                if(create_index(gen_fullpath)):
                    genome_time=get_local_experiment(Object,exp_type)
                    update_index_db(genome_time,exp_type)
                    print ('Index created for %s' % exp_type)
                else:
                    return False
                    break
        else:
            pass
    
    return "\nCompleted syncing server experiments.\n"


def transcriptome_index_update(Object):
    exp_type_list = get_experiment_type_list(Object)
    exp_type_list = filter_experiments(Object,exp_type_list)
    cur_dir = os.getcwd()
    for exp_type in exp_type_list:
        print exp_type
        genome_index=genomeindex_path(exp_type,cur_dir)
        transcriptome_file=get_transcriptome_data(exp_type,cur_dir)
        bool_clean=clean_transcriptome_data(transcriptome_file)
        print bool_clean
        if(not(genome_index and transcriptome_file and bool_clean)):
            continue
        abbrev=transcriptome_file.replace('.gtf','')
        subproc = ["tophat2","-G",transcriptome_file,"--transcriptome-index="+abbrev,"-p","10",genome_index]
        try:
            print ('Creating a transcriptome index...')
            subprocess.check_call(subproc)
        except subprocess.CalledProcessError as e:
            print ('Trancriptome index generation failed')
            continue
    return True
     
def clean_transcriptome_data(transcriptome_file):
    trans_directory=os.path.dirname(os.path.dirname(transcriptome_file))
    script_path=os.path.join(trans_directory,'clean_gtf.R')
    with open(transcriptome_file,'r') as file:
        first_line=file.readline().strip()
    if(first_line=="# Cleaned"):
        return True
    else: 
        subproc = ["Rscript",script_path,transcriptome_file]
        try:
            print ('Cleaning gtf file of short names...')
            subprocess.check_call(subproc)
        except subprocess.CalledProcessError as e:
            print ('Cleaning gtf file failed')
            return False
        return True
        
        
def genomeindex_path(exp_type,cur_dir):
    genome_path=os.path.join(cur_dir,'lib','genome',exp_type)
    contents=os.listdir(genome_path)
    contents.sort()
    if(len(contents)==7):
        suffixes=['.1.bt2','.2.bt2','.3.bt2','.4.bt2','.fa','.rev.1.bt2','.rev.2.bt2']
        for nr in range(len(contents)):
            contents[nr]=contents[nr].replace(suffixes[nr],'')
    elif(len(contents)==8):
        suffixes=['.1.bt2','.2.bt2','.3.bt2','.4.bt2','.fa','.fa.fai','.rev.1.bt2','.rev.2.bt2']
        for nr in range(len(contents)):
            contents[nr]=contents[nr].replace(suffixes[nr],'')
    else:
        print('Number of file-contents is erroneous')
        return False
    if(len(set(contents))==1):
        genome_index_path=os.path.join(genome_path,contents[0])
        return genome_index_path
    else:
        print('Something wrong with genome file or its indexes.' + \
              'Try updating genome and its Bowtie index for %s' %exp_type)
        return False

def get_transcriptome_data(exp_type,cur_dir):
    transcriptome_path=os.path.join(cur_dir,'lib','transcriptome',exp_type)
    if(os.path.exists(transcriptome_path)):
        contents=os.listdir(transcriptome_path)
    else:
        return False
    for file in contents:
        if file.endswith('.gtf'):
            return (os.path.join(transcriptome_path,file))
            break
        elif(file.endswith('.gtf.gz')):
            fullpath=os.path.join(transcriptome_path,file)
            subproc = ["gunzip",fullpath]
            try:
                print ('Unzipping...')
                subprocess.check_call(subproc)
                print ('Unzipping completed')
                transcriptome_path=os.path.join(transcriptome_path,file.replace('.gz',''))
                return transcriptome_path
            except subprocess.CalledProcessError as e:
                print ('Gunzip of .gz file failed')
                return False
        else:
            pass
    print 'Transcriptome file missing. Transcriptome for %s needs updating' %exp_type   
    return False

    

def filter_experiments(Object,exp_type_list):
    if(Object.organism!=None):
        wanted_keys = Object.organism
        exp_type_list= (dict([(i, exp_type_list[i]) for i in wanted_keys if i in exp_type_list]))
        ''' 
       if(len(exp_type_list)!=len(sys.argv)-2):
            print ('Error with at least one organisms name. \n' + \
                'Please correct input mistakes. Found organisms: '+ str("".join(exp_type_list.keys()))+'.')
            return(sys.exit(0))
        else:
        '''
        return exp_type_list
    else:
        return exp_type_list



def check_elements(index_names,exp_type):
    for element in index_names:
        print element
        if(os.path.exists(element)):
            continue
        else:
            print 'Index Files missing: Need to create new bowtie index for %s' % exp_type
            return False
            break
    return True

def check_indexdb(Object,path,exp_type):
    if( len(compare_index_genome_data(exp_type))==1):
        return True
    else:
        print ('Discrepancy in database dates. Updating database')
        files_dir=os.listdir(path)
        if (len(files_dir)>1):
            for f in files_dir:
                if f.endswith('.bt2'):
                    break
        else:
            return False    
        mod_time=os.stat(os.path.join(path,f))[8]
        latest_ctime = time.ctime(mod_time)
        latest_date = datetime.strptime(latest_ctime, "%a %b %d %H:%M:%S %Y").date()
        genome_time=get_local_experiment(Object,exp_type)
        if (latest_date>=genome_time):
            update_index_db(genome_time,exp_type)
            return True
        else:
            print ('Existing files are outdated.')
            return False



def compare_index_genome_data(exp_type):
    """
    """
    DB_TABLE = "affyseq_reference_data"
    query_string =  "select local_version_genome, local_version_bowtieindex  \n " + \
    "from "+DB_TABLE +" WHERE (organism= '"+ exp_type+"' AND local_version_genome=local_version_bowtieindex) ;"
    return query(query_string)

def clean_index_folder(path):
    filelist=os.listdir(path)
    for f in filelist:
        if not(f.endswith('.fa') or f.endswith('.gz')):
            if not(os.path.isdir(os.path.join(path,f))):
                ph=os.path.join(path,f)
                os.remove(ph)
    return True
    

def create_index(gen_fullpath):
    Alt_path=gen_fullpath.replace(".gz","")
    if not(os.path.exists(Alt_path)):
        subproc = ["gunzip",gen_fullpath]
        try:
            print ('Unzipping...')
            subprocess.check_call(subproc)
        except subprocess.CalledProcessError as e:
            print ('Gunzip of .gz file failed')
            return False
    Alt_path2=Alt_path.replace(".fa","")
    subproc2 = ["/group/software/aligners/bowtie2/bowtie2-build","-f",Alt_path,Alt_path2]
    try:
        print ('Building an index')
        subprocess.check_call(subproc2)
    except subprocess.CalledProcessError as e:
        print ('Index building failed')
        return False
    return True
    

def update_index_db(exp_date,exp_type):
    """
    """
    DB_TABLE = "affyseq_reference_data"
    
    query_string =  "UPDATE " + DB_TABLE + " " + \
            "SET local_version_bowtieindex =" + "'" + str(exp_date) + "'" +  \
            "WHERE organism=" + "'" + exp_type + "'" + ";"
    
    return query(query_string)

def generate_indexnames(gen_fullpath):
    base_dir,file_name,=os.path.split(gen_fullpath)    
    basename=file_name.split('.fa')[0]
    suffixes=['.1.bt2','.2.bt2','.3.bt2','.4.bt2','.rev.1.bt2','.rev.2.bt2']
    list=[]
    for element in suffixes:
        nec_filename=os.path.join(base_dir,basename+element)
        list.append(nec_filename)
    return list
    
    
class Type:
    
    def __init__(self,type,organism=None):
        self.type=type
        self.organism=organism
        general_type={1:'genome',
                      2: 'transcriptome',
                      3:'genome',
                      4: 'transcriptome',
             }
        
        file_type={1:'genome',2:'gtf',3:'genome',4:'gtf'}
        
        self.file_type=file_type[self.type]
        self.general_type=general_type[self.type]
        
    def FTP_Directory(self):
        FTP_DIRECTORY= {1:'pub/current_fasta',
                        2:'pub/current_gtf',
                        3:'/pub/current/plants/fasta',
                        4:'/pub/current/plants/gtf'
                           }
        return(FTP_DIRECTORY[self.type]) 
    
    def FTP_Address(self):
        FTP_DIRECTORY= {1:'ftp.ensembl.org',
                        2:'ftp.ensembl.org',
                        3:'ftp.ensemblgenomes.org',
                        4:'ftp.ensemblgenomes.org'
                           }
        return(FTP_DIRECTORY[self.type])   
    
    def FTP_inner_directory(self,experiment_type):
        FTP_DIRECTORY= {1:'pub/current_fasta/'+experiment_type+'/dna',
                        2:'pub/current_gtf/'+experiment_type ,
                        3:'/pub/current/plants/fasta/'+experiment_type+'/dna',
                        4:'/pub/current/plants/gtf/'+experiment_type 
                           }
        return(FTP_DIRECTORY[self.type]) 
    
    def filter_file(self,exp_list):
        abbreviation= {1:'dna.toplevel.fa',
                       2:'gtf.gz',
                       3:'dna.toplevel.fa',
                       4:'gtf.gz',
                       }
        dict_out=dict((k,v) for k,v in exp_list.items() if (abbreviation[self.type] in k))
        return dict_out        


def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hgtbix", ["help"])
        
    except getopt.error, msg:
        print msg
        print "for help use --help"
        sys.exit(2)
    for o, a in opts:
        if o in ("-h", "--help"):
            print USAGE
            sys.exit(0)
    if len(sys.argv) == 2:
        if sys.argv[1] == "-g":
            Object=Type(1)
            genome(Object)
            Object=Type(3)
            genome(Object)
        elif sys.argv[1] == "-t":
            Object=Type(2)
            genome(Object)
            Object=Type(3)
            genome(Object)
        elif sys.argv[1] == "-b":
            genome(Type(1))
            genome(Type(2))
            genome(Type(3))
            genome(Type(4))
        elif sys.argv[1] == "-i":
            genome(Type(1),True)
            genome(Type(3),True)
        elif sys.argv[1] == "-x":
            transcriptome_index_update(Type(1))
            transcriptome_index_update(Type(3))
        else:
            print ('Error in input, see HELP (-h)')
    elif len(sys.argv) > 2:
        if sys.argv[1] == "-g":
            Object=Type(1,sys.argv[2:])
            genome(Object)
            Object=Type(3,sys.argv[2:])
            genome(Object)
        elif sys.argv[1] == "-t":
            Object=Type(2,sys.argv[2:])
            genome(Object)
            Object=Type(4,sys.argv[2:])
            genome(Object)
        elif sys.argv[1] == "-b":
            genome(Type(1,sys.argv[2:]))
            genome(Type(2,sys.argv[2:]))
            genome(Type(3,sys.argv[2:]))
            genome(Type(4,sys.argv[2:]))
        elif sys.argv[1] == "-i":
            genome(Type(1,sys.argv[2:]),True)
            genome(Type(3,sys.argv[2:]),True)
        elif sys.argv[1] == "-x":
            transcriptome_index_update(Type(1,sys.argv[2:]))
            transcriptome_index_update(Type(3,sys.argv[2:]))
        else:
            print ('Error in input, see HELP (-h)')
        

if __name__ == "__main__":
    main()

